#pragma once

#include "Common.h"
#include "Collider.h"
#include "IcoSphere.h"
#include "RigidBody.h"

class BulletSphereCollider : public Collider
{

public:

	BulletSphereCollider();
	virtual ~BulletSphereCollider();

	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual boost::shared_ptr<RigidBody> Create( float mass, float elasticity, float radious, bool noBody = false );

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );
	float GetRadious();
	void SetRadious( float radius );

protected:

	IcoSphere _icoSphere;

	float _radius;
};

