#pragma once

#include "Common.h"

class Logger
{

public:

	Logger();
	virtual ~Logger();

	static void WriteToLog( std::string message );
};

