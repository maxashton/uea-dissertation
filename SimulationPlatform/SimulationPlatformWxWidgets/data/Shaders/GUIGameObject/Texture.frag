#version 330

in vec3 vertexNormal;
in vec3 cameraSpacePosition;
in vec2 UV;
in vec3 colorID;

uniform sampler2D textureSampler;

out vec4 outputColor;

layout(std140) uniform;

void main()
{
	outputColor = texture2D( textureSampler, UV ).rgba;
}