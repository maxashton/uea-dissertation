#include "NvidiaPhysxRigidBody.h"
#include "OpenGLCommon.h"
#include <exception>

NvidiaPhysxRigidBody::NvidiaPhysxRigidBody()
{
}

NvidiaPhysxRigidBody::~NvidiaPhysxRigidBody()
{
}

physx::PxActor* NvidiaPhysxRigidBody::GetRigidBody()
{
	if ( _pRigidBody != NULL )
	{
		return _pRigidBody;
	}
	else
	{
		return _pStaticBody;
	}
}

void NvidiaPhysxRigidBody::SetRigidBody( physx::PxRigidBody* body )
{
	_pRigidBody = body;
}

void NvidiaPhysxRigidBody::SetStaticBody( physx::PxRigidStatic* body )
{
	_pStaticBody = body;
}

glm::mat4 NvidiaPhysxRigidBody::GetModelMatrix()
{
	if ( _pRigidBody != NULL )
	{
		// retrieve world space transform of rigid body
		physx::PxTransform t = _pRigidBody->getGlobalPose();

		// convert to matrix form
		physx::PxMat44 m = physx::PxMat44( t );

		glm::mat4 matrix = glm::make_mat4( m.front() );

		return matrix;
	}

	return glm::mat4();
}

void NvidiaPhysxRigidBody::SetModelMatrix( boost::shared_ptr<glm::vec3> position )
{
	if ( _pRigidBody != NULL )
	{
		physx::PxVec3 pxVec;
		pxVec.x = position->x;
		pxVec.y = position->y;
		pxVec.z = position->z;

		// retrieve world space transform of rigid body
		physx::PxTransform positionTrans( pxVec );

		if ( positionTrans.isValid() )
		{
			_pRigidBody->setGlobalPose( positionTrans );
		}
	}
}

void NvidiaPhysxRigidBody::SetMass( float val )
{
	_pRigidBody->setMass( val );
}

void NvidiaPhysxRigidBody::SetElasticity( float val )
{
	//_pRigidBody->restit( val );
}
