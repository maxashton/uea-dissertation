#include "OpenGLTexture.h"
#include <FreeImage.h>

OpenGLTexture::OpenGLTexture()
{
	_bMipMapsGenerated = false;
}

void OpenGLTexture::CreateEmptyTexture( int iWidth, int iHeight, unsigned int format )
{
	glGenTextures( 1, &_uiTexture );
	glBindTexture( GL_TEXTURE_2D, _uiTexture );

	if ( format == GL_RGBA || format == GL_BGRA )
	{
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, NULL );
	}
	else if ( format == GL_RGB || format == GL_BGR )
	{
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, NULL );
	}
	else
	{
		glTexImage2D( GL_TEXTURE_2D, 0, format, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, NULL );
	}

	glGenSamplers( 1, &_uiSampler );
}

void OpenGLTexture::CreateFromData( BYTE* bData, int iWidth, int iHeight, int iBPP, unsigned int format, bool bGenerateMipMaps )
{
	// Generate an OpenGL texture ID for this texture
	glGenTextures( 1, &_uiTexture );
	glBindTexture( GL_TEXTURE_2D, _uiTexture );

	if ( format == GL_RGBA || format == GL_BGRA )
	{
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, bData );

	}
	else if ( format == GL_RGB || format == GL_BGR )
	{
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, bData );

	}
	else
	{
		glTexImage2D( GL_TEXTURE_2D, 0, format, iWidth, iHeight, 0, format, GL_UNSIGNED_BYTE, bData );

	}
	if ( bGenerateMipMaps )
	{
		glGenerateMipmap( GL_TEXTURE_2D );
	}

	glGenSamplers( 1, &_uiSampler );

	_sPath = "";
	_bMipMapsGenerated = bGenerateMipMaps;
}

bool OpenGLTexture::ReloadTexture()
{
	return LoadTexture2D( _sPath );
}

bool OpenGLTexture::LoadTexture2D( std::string sPath )
{
	FREE_IMAGE_FORMAT formato = FreeImage_GetFileType( sPath.c_str(), 0 );
	FIBITMAP* imagen = FreeImage_Load( formato, sPath.c_str() );

	FIBITMAP* temp = imagen;
	imagen = FreeImage_ConvertTo32Bits( imagen );
	FreeImage_Unload( temp );

	int w = FreeImage_GetWidth( imagen );
	int h = FreeImage_GetHeight( imagen );

	GLubyte* textura = new GLubyte[4 * w*h];
	char* pixeles = (char*)FreeImage_GetBits( imagen );

	for (int j = 0; j < w*h; j++){
		textura[j * 4 + 0] = pixeles[j * 4 + 2];
		textura[j * 4 + 1] = pixeles[j * 4 + 1];
		textura[j * 4 + 2] = pixeles[j * 4 + 0];
		textura[j * 4 + 3] = pixeles[j * 4 + 3];
	}

	glEnable( GL_TEXTURE_2D );
	glGenTextures( 1, &_uiTexture );
	glBindTexture( GL_TEXTURE_2D, _uiTexture );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)textura );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

	_iWidth = w;
	_iHeight = h;

	_sPath = sPath;

	return true; // Success
}

void OpenGLTexture::SetSamplerParameter( unsigned int parameter, unsigned int value )
{
	glSamplerParameteri( _uiSampler, parameter, value );
}

void OpenGLTexture::BindTexture()
{
	glActiveTexture( GL_TEXTURE0 + 0 );
	glBindTexture( GL_TEXTURE_2D, _uiTexture );
	glBindSampler( 0, _uiSampler );
}

void OpenGLTexture::DeleteTexture()
{
	glDeleteSamplers( 1, &_uiSampler );
	glDeleteTextures( 1, &_uiTexture );
}

Texture* OpenGLTexture::Clone()
{
	OpenGLTexture* pTexture = new OpenGLTexture();
	pTexture->_bMipMapsGenerated = this->_bMipMapsGenerated;
	pTexture->_iBPP = this->_iBPP;
	pTexture->_iHeight = this->_iHeight;
	pTexture->_iWidth = this->_iWidth;
	pTexture->_sPath = this->_sPath;
	pTexture->_tfMagnification = this->_tfMagnification;
	pTexture->_tfMinification = this->_tfMinification;
	pTexture->_uiSampler = this->_uiSampler;
	pTexture->_uiTexture = this->_uiTexture;

	return pTexture;
}

unsigned int OpenGLTexture::GetTextureID()
{
	return _uiTexture;
}

