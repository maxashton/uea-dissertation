#include "Light.h"

Light::Light()
{
	_v3Pos = boost::shared_ptr<glm::vec3>( new glm::vec3() );
}

Light::~Light()
{
}

void Light::SetLightPosition( glm::vec3 v3Pos )
{
	_v3Pos->x = v3Pos.x;
	_v3Pos->y = v3Pos.y;
	_v3Pos->z = v3Pos.z;
}

void Light::SetLightIntensity( glm::vec4 v4LightIntensity )
{
	_v4LightIntensity = v4LightIntensity;
}

boost::shared_ptr<glm::vec3> Light::GetLightPossition()
{
	return _v3Pos;
}

glm::vec4 Light::GetLightIntensity()
{
	return _v4LightIntensity;
}

void Light::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "Type", "Light" );
	}

	ptree & position = pInTree->add( "Position", "" );
	position.put( "X", _v3Pos->x );
	position.put( "Y", _v3Pos->y );
	position.put( "Z", _v3Pos->z );

	ptree & intensity = pInTree->add( "Intensity", "" );
	intensity.put( "R", _v4LightIntensity.r );
	intensity.put( "G", _v4LightIntensity.g );
	intensity.put( "B", _v4LightIntensity.b );
	intensity.put( "A", _v4LightIntensity.a );
}

void Light::DeSerializeFromXML( ptree::value_type const& v )
{
	ptree positionNode = v.second.get_child( "Position" );

	_v3Pos->x = positionNode.get<float>( "X" );
	_v3Pos->y = positionNode.get<float>( "Y" );
	_v3Pos->z = positionNode.get<float>( "Z" );

	ptree intensity = v.second.get_child( "Intensity" );

	_v4LightIntensity.r = intensity.get<float>( "R" );
	_v4LightIntensity.g = intensity.get<float>( "G" );
	_v4LightIntensity.b = intensity.get<float>( "B" );
	_v4LightIntensity.a = intensity.get<float>( "A" );
}
