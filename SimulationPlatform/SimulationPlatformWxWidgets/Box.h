#pragma once
#include "GameObject.h"

class Box :	public GameObject
{

public:

	Box();
	virtual ~Box();

	void Create();
	int GetNumberOfInicies();

	virtual void ConnectRenderer( Renderer* pRenderer );
	virtual void Draw( glm::mat4 mCameraMatrix );

	std::vector<glm::vec3> _geometry;
	std::vector<short> _indicies;

	GLuint _vertexBufferObject;
	GLuint _indexBufferObject;
	GLuint _vao;
};

