#pragma once

#include "Common.h"
#include "OpenGLCommon.h"

struct ProjectionBlock
{
	glm::mat4 cameraToClipMatrix;
	glm::mat4 orthoMatrix;
};

struct OpenGLShaderPropoties
{
	GLuint ProgramID;
	GLuint ModelToCameraMatrixUnif;
	GLuint NormalModelToCameraMatrixUnif;
	GLuint TextureSamplerUnif;
	GLuint ColorID;
};

class OpenGLShader
{
public:

	OpenGLShader();
	virtual ~OpenGLShader();

	static void SetCameraToClipMatrix( int w, int h );
	static void SetupStaticBuffers();
	void AddShader( GLenum shaderType, std::string strFileName );
	void CreatePrograme();

	const OpenGLShaderPropoties GetShaderPropoties();
	const glm::mat4 GetCameraToClipMatrix();
	
	static int LIGHT_BLOCK_INDEX;
	static int PROJECTION_BLOCK_INDEX;
	static int MATERIAL_BLOCK_INDEX;

	static GLuint LIGHT_UNIFORM_BUFFER;
	static GLuint PROJECTION_UNIFORM_BUFFER;
	static GLuint MATERIAL_UNIFORM_BUFFER;
	
protected:

	static glm::mat4 _cameraToClipMatrix;

	std::vector<GLuint> _shaderList;
	OpenGLShaderPropoties _strctShaderPropoties;
};