#pragma once

#include "Common.h"
#include <glm/glm.hpp>

class Camera
{
public:

	Camera();
	virtual ~Camera();

	virtual glm::mat4x4 GetViewMatrix();
	virtual glm::vec3 GetForwardVector();
	virtual glm::vec3 GetCurrentPosition();

	std::string Name;

protected:

};

