struct PerLight
{
	float4 cameraSpaceLightPos;
	float4 lightIntensity;
};

cbuffer ConstantBuffer : register(b0)
{
	float4 diffuseColor;
	float4 specularColor;	
	float4x4 modelToCameraMatrix;
	float4x4 normalModelToCameraMatrix;
	float specularShininess;
}

cbuffer ConstantLightBuffer : register(b1)
{
    float4 ambientIntensity;
	
	PerLight lights[3];

	float lightAttenuation;
	float maxIntensity;
	float gamma;
}

cbuffer ConstantProjectionBuffer : register(b2)
{
	float4x4 cameraToClipMatrix;
}

Texture2D Texture;
SamplerState ss;

struct VOut
{
    float4 color : COLOR;
    float4 position : SV_POSITION;
};

VOut VShader(float3 position : POSITION )
{
    VOut output;
	float4 nPosition = float4( position.xyz, 1.0 );
	
	//set outputs
	float4 tempCamPosition = mul(modelToCameraMatrix, nPosition);
	output.position = mul(cameraToClipMatrix, tempCamPosition);
	
    return output;
}

float4 PShader(float4 color : COLOR ) : SV_TARGET
{
	return float4( 1.0, 1.0, 1.0, 1.0);
}
