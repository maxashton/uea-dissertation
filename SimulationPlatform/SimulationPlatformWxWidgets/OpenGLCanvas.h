#ifndef _glpane_
#define _glpane_

#include "Common.h"
#include "Engine.h"
#include <wx/wx.h>
#include "wx/glcanvas.h"

class OpenGLCanvas : public wxGLCanvas
{
	wxGLContext* m_context;

public:
	OpenGLCanvas( wxWindow* parent, wxSize size, int* args );
	virtual ~OpenGLCanvas();

	void resized( wxSizeEvent& evt );
	void OnIdle( wxIdleEvent& evt );
	void Close( wxCloseEvent& evt );
	int getWidth();
	int getHeight();

	void render( wxPaintEvent& evt );
	void InitOpenGL3D( int topleft_x, int topleft_y, int bottomrigth_x, int bottomrigth_y );
	void Initialize( int physicsType );

	// events
	void mouseMoved( wxMouseEvent& event );
	void mouseDown( wxMouseEvent& event );
	void mouseWheelMoved( wxMouseEvent& event );
	void mouseReleased( wxMouseEvent& event );
	void rightClick( wxMouseEvent& event );
	void mouseLeftWindow( wxMouseEvent& event );
	void keyPressed( wxKeyEvent& event );
	void keyReleased( wxKeyEvent& event );

	Engine* GetEngine();

	wxFrame* ParentFrame;

	DECLARE_EVENT_TABLE()

protected:

	bool _initialized;
	Engine _engine;
};
#endif