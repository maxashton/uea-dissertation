#pragma once

#include "Common.h"
#include "OpenGLCommon.h"
#include "Texture.h"

enum ETextureFiltering
{
	TEXTURE_FILTER_MAG_NEAREST = 0, // Nearest criterion for magnification
	TEXTURE_FILTER_MAG_BILINEAR, // Bilinear criterion for magnification
	TEXTURE_FILTER_MIN_NEAREST, // Nearest criterion for minification
	TEXTURE_FILTER_MIN_BILINEAR, // Bilinear criterion for minification
	TEXTURE_FILTER_MIN_NEAREST_MIPMAP, // Nearest criterion for minification, but on closest mipmap
	TEXTURE_FILTER_MIN_BILINEAR_MIPMAP, // Bilinear criterion for minification, but on closest mipmap
	TEXTURE_FILTER_MIN_TRILINEAR, // Bilinear criterion for minification on two closest mipmaps, then averaged
};

class OpenGLTexture : public Texture
{

public:

	OpenGLTexture();

	virtual void CreateEmptyTexture( int iWidth, int iHeight, unsigned int format );
	virtual void CreateFromData( BYTE* bData, int iWidth, int iHeight, int iBPP, unsigned int format, bool bGenerateMipMaps = false );

	virtual bool ReloadTexture();

	virtual bool LoadTexture2D( std::string sPath );
	virtual void BindTexture();

	virtual void SetSamplerParameter( unsigned int parameter, unsigned int value );

	virtual void DeleteTexture();

	virtual	Texture* Clone();

	unsigned int GetTextureID();

protected:

	unsigned int  _uiTexture;
	unsigned int  _uiSampler;

};