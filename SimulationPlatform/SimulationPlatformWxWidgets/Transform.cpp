#include "Transform.h"

Transform::Transform()
{
	pPosition = boost::shared_ptr<glm::vec3>( new glm::vec3 );
	pPosition->x = 0.0f;
	pPosition->y = 0.0f;
	pPosition->z = 0.0f;

	pOrientation = new glm::fquat();
	pScale = new glm::vec3();
	pScale->x = 1.0f;
	pScale->y = 1.0f;
	pScale->z = 1.0f;

}

Transform::~Transform()
{
	if ( pOrientation ) delete pOrientation;
	if ( pScale ) delete pScale;
}

void Transform::SetPosition( glm::vec3 position )
{
	pPosition->x = position.x;
	pPosition->y = position.y;
	pPosition->z = position.z;
}
