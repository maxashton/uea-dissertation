#include "GameObject.h"

GameObject::GameObject()
{
	_pTransform = boost::shared_ptr<Transform>( new Transform() );
	_pMaterial = new Material();
	_drawComponents = false;
}

GameObject::GameObject( RenderFactory factory )
{
	_renderfactory = factory;
}

GameObject::~GameObject()
{
	BOOST_FOREACH( Component* comp, _vComponenetList )
	{
		delete comp;
	}

	if( _pMaterial ) delete _pMaterial;
	//if ( _pRenderer ) delete _pRenderer;
}

Material* GameObject::GetMaterial()
{
	return _pMaterial;
}

boost::shared_ptr<Transform> GameObject::GetTransform()
{
	return _pTransform;
}

glm::mat4 GameObject::GetModelMatrix()
{
	return _modelMatrix;
}

bool GameObject::GetModelMatrixOverride()
{
	return _modelMatrixOverride;
}

void GameObject::Draw( glm::mat4 mCameraMatrix )
{
	if ( _drawComponents )
	{
		BOOST_FOREACH( Component* comp, _vComponenetList )
		{
			comp->Draw( mCameraMatrix );
		}
	}
}

void GameObject::DrawPicking( glm::mat4 mCameraMatrix )
{

}

void GameObject::SetModelMatrix( glm::mat4 modelMatrix )
{
	_modelMatrix = modelMatrix;
	_modelMatrixOverride = true;
}

void GameObject::AddComponenet( Component* pComponenet )
{
	_modelMatrixOverride = false;
	_vComponenetList.push_back( pComponenet );
	pComponenet->AttachGameObject( this );
}

void GameObject::RemoveComponenet( Component* pComponenet )
{
	std::vector<Component*>::iterator i = std::find( _vComponenetList.begin(), _vComponenetList.end(), pComponenet );

	if ( i != _vComponenetList.end() )
	{
		_vComponenetList.erase( i );
	}
}

void GameObject::Update()
{
	BOOST_FOREACH( Component* comp, _vComponenetList )
	{
		comp->Update();
	}
}

void GameObject::ConnectRenderer( Renderer* pRenderer )
{
	_pRenderer = pRenderer;
}

void GameObject::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "Type", "GameObject" );
	}

	ptree & nameNode = pInTree->add( "Name", _name );
	ptree & componenetNode = pInTree->add( "Components", "" );

	BOOST_FOREACH( Component* comp, _vComponenetList )
	{
		ptree & compNode = componenetNode.add( "Component", "" );
		comp->SerializeToXLM( &compNode );
	}

	boost::shared_ptr<Transform> pTransform = GetTransform();
	ptree & transformNode = pInTree->add( "Transform", "" );

	ptree & position = transformNode.add( "Position", "" );
	position.put( "X", pTransform->pPosition->x );
	position.put( "Y", pTransform->pPosition->y );
	position.put( "Z", pTransform->pPosition->z );

	ptree & scale = transformNode.add( "Scale", "" );
	scale.put( "X", pTransform->pScale->x );
	scale.put( "Y", pTransform->pScale->y );
	scale.put( "Z", pTransform->pScale->z );

	ptree & orientation = transformNode.add( "Orientation", "" );
	orientation.put( "X", pTransform->pOrientation->x );
	orientation.put( "Y", pTransform->pOrientation->y );
	orientation.put( "Z", pTransform->pOrientation->z );
	orientation.put( "W", pTransform->pOrientation->w );

	ptree & materialmNode = pInTree->add( "Material", "" );

	ptree & diffuse = materialmNode.add( "Diffuse", "" );
	diffuse.put( "R", _pMaterial->GetDiffuseColor().r );
	diffuse.put( "G", _pMaterial->GetDiffuseColor().g );
	diffuse.put( "B", _pMaterial->GetDiffuseColor().b );
	diffuse.put( "A", _pMaterial->GetDiffuseColor().a );

	ptree & specular = materialmNode.add( "Specular", "" );
	specular.put( "R", _pMaterial->GetSpecularColor().r );
	specular.put( "G", _pMaterial->GetSpecularColor().g );
	specular.put( "B", _pMaterial->GetSpecularColor().b );
	specular.put( "A", _pMaterial->GetSpecularColor().a );

	materialmNode.put( "SpecularShininess", _pMaterial->GetSpecularShininess() );
}

void GameObject::DeSerializeFromXML( ptree::value_type const& v )
{
	_name = v.second.get<std::string>( "Name" );

	ptree transform = v.second.get_child( "Transform" );

	ptree positionNode = transform.get_child( "Position" );
	
	_pTransform->pPosition->x = positionNode.get<float>( "X" );
	_pTransform->pPosition->y = positionNode.get<float>( "Y" );
	_pTransform->pPosition->z = positionNode.get<float>( "Z" );

	ptree scaleNode = transform.get_child( "Scale" );

	_pTransform->pScale->x = scaleNode.get<float>( "X" );
	_pTransform->pScale->y = scaleNode.get<float>( "Y" );
	_pTransform->pScale->z = scaleNode.get<float>( "Z" );

	ptree oriNode = transform.get_child( "Orientation" );

	_pTransform->pOrientation->x = oriNode.get<float>( "X" );
	_pTransform->pOrientation->y = oriNode.get<float>( "Y" );
	_pTransform->pOrientation->z = oriNode.get<float>( "Z" );
	_pTransform->pOrientation->w = oriNode.get<float>( "W" );

	ptree materialNode = v.second.get_child( "Material" );

	ptree diffuseNode = materialNode.get_child( "Diffuse" );
	_pMaterial->SetDiffuseColor( diffuseNode.get<float>( "R" ), diffuseNode.get<float>( "G" ), diffuseNode.get<float>( "B" ), diffuseNode.get<float>( "A" ) );

	ptree specularNode = materialNode.get_child( "Specular" );
	_pMaterial->SetSpecularColor( specularNode.get<float>( "R" ), specularNode.get<float>( "G" ), specularNode.get<float>( "B" ), specularNode.get<float>( "A" ) );

	_pMaterial->SetSpecularShininess( materialNode.get<float>( "SpecularShininess" ) );
}

int GameObject::GetNumberOfVertesies()
{
	return 0;
}

void GameObject::SetPickingColor( glm::vec4 color )
{
	_pickingColor = color;
}

glm::vec4 GameObject::GetPickingColor()
{
	return _pickingColor;
}

void GameObject::SetDrawComponenets( bool toDraw )
{
	_drawComponents = toDraw;
}

Component* GameObject::FindCollider()
{
	for ( std::vector<Component*>::iterator i = _vComponenetList.begin(); i != _vComponenetList.end(); i++ )
	{
		if ( ( *i )->Identify() == IDCollider )
		{
			return ( *i );
		}
	}

	return NULL;
}

void GameObject::SetName( std::string name )
{
	_name = name;
}

std::string GameObject::GetName()
{
	return _name;
}

std::vector<Component*>* GameObject::GetComponenets()
{
	return &_vComponenetList;
}

void GameObject::SetModelViewMatrixOverride( bool val )
{
	_modelMatrixOverride = val;
}
