#include "LightGizmo.h"

LightGizmo::LightGizmo( boost::shared_ptr<Light> pLight )
{
	_pLight = pLight;
}

LightGizmo::~LightGizmo()
{
}

boost::shared_ptr<Light> LightGizmo::GetLight()
{
	return _pLight;
}
