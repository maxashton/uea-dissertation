#pragma once

#include "Common.h"
#include "PhysicsEngine.h"
#include <bullet/btBulletDynamicsCommon.h>

class BulletPhysicsEngine :	public PhysicsEngine
{

public:

	BulletPhysicsEngine();
	virtual ~BulletPhysicsEngine();

	virtual void Setup();
	virtual void Destroy();
	virtual void StartAsync();
	virtual void AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody );

protected:

	btDynamicsWorld* _pWorld;
	btDispatcher* _pDispatcher;
	btCollisionConfiguration* _pCollisionConfig;
	btBroadphaseInterface* _pBroadphase;
	btConstraintSolver* _pSolver;

	std::vector<btRigidBody*> _rigidBodies;

	virtual void UpdatePhysics();

};

