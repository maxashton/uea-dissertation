#pragma once

#include "Common.h"
#include "Singleton.h"
#include "GUIObject.h"
#include <vector>

class HUDController : public Singleton<HUDController>
{

public:

	HUDController();
	virtual ~HUDController();

	void Render();

	void AddHUDObject( GUIObject* pGameObject );
	void ProcessClick( int x, int y );

protected:

	friend class Singleton <HUDController>;

	std::vector<GUIObject*> _vgoHUDObjects;

	glutil::MatrixStack _msHUDTransform;

	void HUDController::GetPixel( int x, int y, GLubyte *color );
	glm::vec3 PickClicked( int x, int y );
};

