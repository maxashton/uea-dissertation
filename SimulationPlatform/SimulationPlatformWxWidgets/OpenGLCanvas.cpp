#include "OpenGLCanvas.h"

BEGIN_EVENT_TABLE( OpenGLCanvas, wxGLCanvas )
EVT_MOTION( OpenGLCanvas::mouseMoved )
EVT_LEFT_DOWN( OpenGLCanvas::mouseDown )
EVT_RIGHT_DOWN( OpenGLCanvas::mouseDown )
EVT_MIDDLE_DOWN( OpenGLCanvas::mouseDown )
EVT_LEFT_UP( OpenGLCanvas::mouseReleased )
EVT_MIDDLE_UP( OpenGLCanvas::mouseReleased )
EVT_RIGHT_UP( OpenGLCanvas::mouseReleased )
EVT_RIGHT_DOWN( OpenGLCanvas::rightClick )
EVT_LEAVE_WINDOW( OpenGLCanvas::mouseLeftWindow )
EVT_SIZE( OpenGLCanvas::resized )
EVT_KEY_DOWN( OpenGLCanvas::keyPressed )
EVT_KEY_UP( OpenGLCanvas::keyReleased )
EVT_MOUSEWHEEL( OpenGLCanvas::mouseWheelMoved )
EVT_PAINT( OpenGLCanvas::render )
EVT_IDLE( OpenGLCanvas::OnIdle )
EVT_CLOSE( OpenGLCanvas::Close )
END_EVENT_TABLE()

OpenGLCanvas::OpenGLCanvas( wxWindow* parent, wxSize size, int* args ) : wxGLCanvas( parent, wxID_ANY, args, wxDefaultPosition, size, wxFULL_REPAINT_ON_RESIZE )
{
	_initialized = false;

	m_context = new wxGLContext( this );

	// To avoid flashing on MSW
	SetBackgroundStyle( wxBG_STYLE_CUSTOM );
}

//some useful events to use
void OpenGLCanvas::mouseMoved( wxMouseEvent& event )
{
	if ( _initialized )
	{
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseMotion( pt.x, pt.y - 50 );

		Refresh();
	}
}

void OpenGLCanvas::mouseDown( wxMouseEvent& event )
{
	if ( _initialized )
	{
		SetFocus();
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseButton( event.GetButton() - 1, 1, pt.x, pt.y - 50 );

		Refresh();
	}
}

void OpenGLCanvas::mouseWheelMoved( wxMouseEvent& event )
{

}

void OpenGLCanvas::mouseReleased( wxMouseEvent& event )
{
	if ( _initialized )
	{
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseButton( event.GetButton() - 1, 0, pt.x, pt.y - 50 );

		Refresh();
	}
}

void OpenGLCanvas::rightClick( wxMouseEvent& event )
{

}

void OpenGLCanvas::mouseLeftWindow( wxMouseEvent& event )
{

}

void OpenGLCanvas::keyPressed( wxKeyEvent& event )
{
	POINT pt;
	GetCursorPos( &pt );

	_engine.Keyboard( event.m_uniChar, pt.x, pt.y -50 );
}

void OpenGLCanvas::keyReleased( wxKeyEvent& event )
{

}

Engine* OpenGLCanvas::GetEngine()
{
	return &_engine;
}

OpenGLCanvas::~OpenGLCanvas()
{
	delete m_context;
}

void OpenGLCanvas::resized( wxSizeEvent& evt )
{
	if ( _initialized )
	{
		_engine.Reshape( getWidth(), getHeight() );
	}

	Refresh();
}

void OpenGLCanvas::InitOpenGL3D( int topleft_x, int topleft_y, int bottomrigth_x, int bottomrigth_y )
{
	int argc = 1;
	char* argv [1] = { wxString( ( wxTheApp->argv ) [0] ).char_str() };

	//set context to current
	wxGLCanvas::SetCurrent( *m_context );
}

void OpenGLCanvas::Initialize( int physicsType )
{
	InitOpenGL3D( 0, 0, getWidth() / 2, getHeight() );
	_engine.Init( OpenGL, physicsType );
	_engine.Reshape( getWidth(), getHeight() );

	_initialized = true;
}

int OpenGLCanvas::getWidth()
{
	return GetSize().x;
}

int OpenGLCanvas::getHeight()
{
	return GetSize().y;
}

void OpenGLCanvas::OnIdle( wxIdleEvent& evt )
{
	Refresh( false );
}

void OpenGLCanvas::Close( wxCloseEvent& evt )
{

}

void OpenGLCanvas::render( wxPaintEvent& evt )
{
	if ( !IsShown() ) return;

	wxGLCanvas::SetCurrent( *m_context );
	wxPaintDC( this );

	_engine.Display();

	SwapBuffers();
}