#pragma once

#include "Common.h"
#include "GameObject.h"
//#include "Shader.h"
#include <assimp/scene.h> 

typedef void( *click_event ) ();

class GUIObject : public GameObject
{

public:

	GUIObject( std::string stdTextureLocation, std::string stdSelectedTextureLocation, glm::vec3 v3ColorID, float fWidth, float fHeight );
	virtual ~GUIObject();

/*	virtual void Draw( glutil::MatrixStack mCameraMatrix );
	virtual void DrawPicking();
	void BuildPickingShader();
	void BuildGUIShader();

	void SetSelected( bool bSelected );
	void AttatchClickEvent( click_event fpEvent );
	glm::vec3 GetColorID()*/;

protected:

	//OpenGLShader _pickingShader;
	//std::vector<aiVector3D> _vVertexList;
	//std::vector<aiVector2D> _vUVList;
	//GLuint _uiVAO;

	//GLuint _uiIdleTexture;
	//GLuint _iSelectedTexture;

	//bool _bSelected;

	//void CreateVertexList();
	//void LoadTextures( std::string stdTextureLocation, std::string stdSelectedTextureLocation );

	//GLuint positionBufferObject;

	//aiVector3D _aiColorID;
	//glm::vec3 _v3ColorID;

	//int _iSelecedCounter;

	//click_event _fpClick;

	//float _fWidth;
	//float _fHeight;
};