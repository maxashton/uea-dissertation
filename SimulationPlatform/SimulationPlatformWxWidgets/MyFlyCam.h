#pragma once
#include "Common.h"
#include "Camera.h"

class MyFlyCam : public Camera
{

public:

	MyFlyCam();
	virtual ~MyFlyCam();

	virtual glm::mat4x4 GetViewMatrix();

protected:

	glm::vec3 _rotation;
	glm::vec3 _position;

};
