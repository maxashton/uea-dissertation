#include "D3D11Canvas.h"

BEGIN_EVENT_TABLE( D3D11Canvas, wxWindow )
EVT_SIZE( D3D11Canvas::OnSize )
EVT_PAINT( D3D11Canvas::OnPaint )
EVT_ERASE_BACKGROUND( D3D11Canvas::OnEraseBackground )
EVT_IDLE( D3D11Canvas::OnIdle )
EVT_KEY_DOWN( D3D11Canvas::keyPressed )
EVT_KEY_UP( D3D11Canvas::keyReleased )
EVT_MOUSEWHEEL( D3D11Canvas::mouseWheelMoved )
EVT_MOTION( D3D11Canvas::mouseMoved )
EVT_LEFT_DOWN( D3D11Canvas::mouseDown )
EVT_RIGHT_DOWN( D3D11Canvas::mouseDown )
EVT_MIDDLE_DOWN( D3D11Canvas::mouseDown )
EVT_LEFT_UP( D3D11Canvas::mouseReleased )
EVT_MIDDLE_UP( D3D11Canvas::mouseReleased )
EVT_RIGHT_UP( D3D11Canvas::mouseReleased )
EVT_RIGHT_DOWN( D3D11Canvas::rightClick )
EVT_LEAVE_WINDOW( D3D11Canvas::mouseLeftWindow )
END_EVENT_TABLE()

D3D11Canvas::D3D11Canvas( wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name )
				: wxWindow( parent, id, pos, size, style | wxFULL_REPAINT_ON_RESIZE, name )
{
	m_init = false;
	_hWnd = ( HWND )GetHWND();
	_initialized = false;
}

D3D11Canvas::~D3D11Canvas()
{
	Direct3DShader::Release();
}

int D3D11Canvas::GetWidth()
{
	return GetSize().x;
}

int D3D11Canvas::GetHeight()
{
	return GetSize().y;
}

Engine* D3D11Canvas::GetEngine()
{
	return &_engine;
}

void D3D11Canvas::OnIdle( wxIdleEvent &event )
{
	event.RequestMore( true );
	Render();
}

void D3D11Canvas::OnPaint( wxPaintEvent& WXUNUSED( event ) )
{
	wxPaintDC dc( this );

	// Initialize D3D
	if ( !m_init )
	{
		InitD3D();
		m_init = true;
	}

}

void D3D11Canvas::OnSize( wxSizeEvent& event )
{
	if ( _initialized )
	{
		_engine.Reshape( GetWidth(), GetHeight() );
		Render();
	}

}

void D3D11Canvas::OnEraseBackground( wxEraseEvent& WXUNUSED( event ) )
{
}

void D3D11Canvas::InitD3D()
{
	// create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC scd;

	// clear out the struct for use
	ZeroMemory( &scd, sizeof( DXGI_SWAP_CHAIN_DESC ) );

	// fill the swap chain description struct
	scd.BufferCount = 1;                                   // one back buffer
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;    // use 32-bit color
	scd.BufferDesc.Width = GetWidth();                   // set the back buffer width
	scd.BufferDesc.Height = GetHeight();                 // set the back buffer height
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;     // how swap chain is to be used
	scd.OutputWindow = _hWnd;                               // the window to be used
	scd.SampleDesc.Count = 4;                              // how many multisamples
	scd.Windowed = TRUE;                                   // windowed/full-screen mode
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;    // allow full-screen switching

	// create a device, device context and swap chain using the information in the scd struct
	D3D11CreateDeviceAndSwapChain( NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		NULL,
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&scd,
		&Direct3DShader::SwapChain,
		&Direct3DShader::Device,
		NULL,
		&Direct3DShader::DeviceContext );


	// create the depth buffer texture
	D3D11_TEXTURE2D_DESC texd;
	ZeroMemory( &texd, sizeof( texd ) );

	texd.Width = GetWidth();
	texd.Height = GetHeight();
	texd.ArraySize = 1;
	texd.MipLevels = 1;
	texd.SampleDesc.Count = 4;
	texd.Format = DXGI_FORMAT_D32_FLOAT;
	texd.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	ID3D11Texture2D *pDepthBuffer;
	Direct3DShader::Device->CreateTexture2D( &texd, NULL, &pDepthBuffer );

	// create the depth buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
	ZeroMemory( &dsvd, sizeof( dsvd ) );

	dsvd.Format = DXGI_FORMAT_D32_FLOAT;
	dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	Direct3DShader::Device->CreateDepthStencilView( pDepthBuffer, &dsvd, &Direct3DShader::ZBuffer );
	pDepthBuffer->Release();

	// get the address of the back buffer
	ID3D11Texture2D *pBackBuffer;
	Direct3DShader::SwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );

	// use the back buffer address to create the render target
	Direct3DShader::Device->CreateRenderTargetView( pBackBuffer, NULL, &Direct3DShader::BackBuffer );
	pBackBuffer->Release();

	// set the render target as the back buffer
	Direct3DShader::DeviceContext->OMSetRenderTargets( 1, &Direct3DShader::BackBuffer, Direct3DShader::ZBuffer );

	// Set the viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory( &viewport, sizeof( D3D11_VIEWPORT ) );

	viewport.TopLeftX = 0;    // set the left to 0
	viewport.TopLeftY = 0;    // set the top to 0
	viewport.Width = GetWidth();    // set the width to the window's width
	viewport.Height = GetHeight();    // set the height to the window's height
	viewport.MinDepth = 0;    // the closest an object can be on the depth buffer is 0.0
	viewport.MaxDepth = 1;    // the farthest an object can be on the depth buffer is 1.0

	Direct3DShader::DeviceContext->RSSetViewports( 1, &viewport );

	InitPipeline();
}

void D3D11Canvas::Render()
{
	_engine.Display();

	Direct3DShader::SwapChain->Present( 0, 0 );
}

void D3D11Canvas::Initialize( int physicsType )
{
	_initialized = true;
	m_init = true;
	InitD3D();
	_engine.Init( DirectX, physicsType );
	_engine.Reshape( GetWidth(), GetHeight() );
}

void D3D11Canvas::InitPipeline( void )
{
	Direct3DShader::SetupConstantBuffers();
	Direct3DShader::SetRasterState();
	_renderer.BuildAssimpGameObjectDiffuseShader();
}

void D3D11Canvas::mouseMoved( wxMouseEvent& event )
{
	if ( _initialized )
	{
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseMotion( pt.x, pt.y );

		Refresh();
	}
}

void D3D11Canvas::mouseDown( wxMouseEvent& event )
{
	if ( _initialized )
	{
		SetFocus();
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseButton( event.GetButton() - 1, 1, pt.x, pt.y );

		Refresh();
	}
}

void D3D11Canvas::mouseWheelMoved( wxMouseEvent& event )
{

}

void D3D11Canvas::mouseReleased( wxMouseEvent& event )
{
	if ( _initialized )
	{
		POINT pt;
		GetCursorPos( &pt );

		_engine.MouseButton( event.GetButton() - 1, 0, pt.x, pt.y );

		Refresh();
	}
}

void D3D11Canvas::rightClick( wxMouseEvent& event )
{

}

void D3D11Canvas::mouseLeftWindow( wxMouseEvent& event )
{

}

void D3D11Canvas::keyPressed( wxKeyEvent& event )
{
	POINT pt;
	GetCursorPos( &pt );

	_engine.Keyboard( event.m_uniChar, pt.x, pt.y );
}

void D3D11Canvas::keyReleased( wxKeyEvent& event )
{

}
