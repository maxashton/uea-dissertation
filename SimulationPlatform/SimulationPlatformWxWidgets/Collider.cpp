#include "Collider.h"
#include "GameObject.h"

Collider::Collider()
{
	_colliderType = None;
}

Collider::~Collider()
{
}

void Collider::Draw()
{

}

boost::shared_ptr<RigidBody> Collider::Create()
{
	return NULL;
}

void Collider::Update()
{
	if ( _pRigidBody != NULL && _pGameObject !=  NULL)
	{
		glm::mat4 matrix = _pRigidBody->GetModelMatrix();

		_pGameObject->SetModelMatrix( matrix );
	}
}

void Collider::AttachGameObject( GameObject* pGameObject )
{
	Component::AttachGameObject( pGameObject );

	if ( _pRigidBody != NULL )
	{
		_pRigidBody->SetModelMatrix( pGameObject->GetTransform()->pPosition );
	}
}

boost::shared_ptr<RigidBody> Collider::GetRigidBody()
{
	return _pRigidBody;
}

void Collider::RemoveRigidBody()
{
	this->_pRigidBody = NULL;
}

ColliderType Collider::GetColliderType()
{
	return _colliderType;
}

IdentifyType Collider::Identify()
{
	return IDCollider;
}

float Collider::GetElasticity()
{
	return _elasticity;
}

void Collider::SetElasticity( float val )
{
	_elasticity = val;
}

float Collider::GetMass()
{
	return _mass;
}

void Collider::SetMass( float val )
{
	_mass = val;
}
