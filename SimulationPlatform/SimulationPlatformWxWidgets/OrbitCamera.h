#pragma once

#include "Common.h"
#include "Component.h"
#include "Camera.h"
#include "GameObject.h"

class OrbitCamera :	public Camera
{

public:

	OrbitCamera( GameObject* pOrbitObject );
	~OrbitCamera();

	void ChangeOrientation( float x, float y, float z );
	void SetOrbitDistance( float fDistance );
	void ChangeOrbitDistance( float fDistance );

	virtual glm::mat4x4 GetViewMatrix();


protected:

	glm::vec3 _v3Rotation;
	GameObject* _pObjectToOrbit;

	glm::quat _qOrbitOrientation;

	float _fOrbitDistance;

	glm::vec3 GetDirectionVector();

};

