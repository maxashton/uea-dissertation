#pragma once

#include "RuntimeFrame.h"
#include "Engine.h"

class Engine;

class RuntimeFrameEx :	public RuntimeFrame
{

public:

	RuntimeFrameEx( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500, 300 ), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );
	virtual ~RuntimeFrameEx();
protected:

	Engine* _pEngine;

	wxWindow* _renderPanel;

	void FPSCallback( double fps );
	void PhysicsCallback( double fps );

	std::string _sceneName;

};

