#pragma once
#include "Collider.h"
#include "Box.h"
#include "RigidBody.h"

class BulletBoxCollider : public Collider
{

public:

	BulletBoxCollider();
	virtual ~BulletBoxCollider();

	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual boost::shared_ptr<RigidBody> Create( float mass, float elasticity, float width, float height, float depth, bool noBody = false );

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );
	glm::vec3 GetDimentions();
	void SetDimentions( glm::vec3 dims );

protected:

	float _width;
	float _height;
	float _depth;

	Box _box;

};

