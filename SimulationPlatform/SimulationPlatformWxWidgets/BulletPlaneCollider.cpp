#include "BulletPlaneCollider.h"
#include "Scene.h"
#include "RenderFactory.h"
#include "RigidBodyFactory.h"

BulletPlaneCollider::BulletPlaneCollider()
{
	_colliderType = PlaneType;
}


BulletPlaneCollider::~BulletPlaneCollider()
{
}

void BulletPlaneCollider::Draw( glm::mat4 mCameraMatrix )
{
	glm::mat4 matrix = _pRigidBody->GetModelMatrix();
	_geoPlane.SetModelMatrix( matrix );

	_geoPlane.Draw( mCameraMatrix );
}

boost::shared_ptr<RigidBody> BulletPlaneCollider::Create()
{
	//create drawable plane
	_geoPlane.Create();
	_geoPlane.ConnectRenderer( RenderFactory::CreateRenderer() );
	_geoPlane.GetTransform()->pScale = new glm::vec3( 100, 100, 100 );

	_pRigidBody = RigidBodyFactory::CreatePlaneBody();

	return _pRigidBody;
}

void BulletPlaneCollider::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType /*= true */ )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "CType", "BulletPlaneCollider" );
	}

	Component::SerializeToXLM( pInTree, false );
}

void BulletPlaneCollider::DeSerializeFromXML( ptree::value_type const& v )
{
	Create();
}