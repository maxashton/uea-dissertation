#include "Engine.h"
#include "LightManager.h"
#include "Framework.h"
#include "BulletPlaneCollider.h"
#include "BulletSphereCollider.h"
#include "BulletBoxCollider.h"
#include "HUDController.h"
#include "Renderer.h"
#include "RenderFactory.h"
#include "SceneIO.h"
#include "MyFlyCam.h"
#include "LightGizmo.h"
#include <glm/gtc/quaternion.hpp>
#include "BulletPhysicsEngine.h"
#include "RigidBodyFactory.h"
#include "PhysxPhysicsEngine.h"

glutil::ViewData g_initialViewData =
{
	glm::vec3( 0.0f, 10.0f, 20.0f ),
	glm::fquat( glm::vec3( Framework::DegToRad( 0 ), 0, 0 ) ),
	0.0f,
	0.0f
};

glutil::ViewScale g_viewScale =
{
	3.0f, 80.0f,
	4.0f, 1.0f,
	5.0f, 1.0f,
	90.0f / 250.0f
};

Engine::Engine()
{
	_statsWritain = false;
	_recordStats = false;
}

Engine::~Engine()
{
	_pPhysicsEngine->Destroy();
	if ( _pScene ) { delete _pScene; }
}

void Engine::Init( int renderType, int physicsType )
{
	RenderFactory::SetRenderType( renderType );

	if ( physicsType == BulletPx )
	{
		_pPhysicsEngine = boost::shared_ptr<PhysicsEngine>( new BulletPhysicsEngine() );
		RigidBodyFactory::SetType( BulletPx );
	}
	else if ( physicsType == NvPhysX )
	{
		_pPhysicsEngine = boost::shared_ptr<PhysicsEngine>( new PhysxPhysicsEngine() );
		RigidBodyFactory::SetType( NvPhysX );
	}

	_pPhysicsEngine->Setup();

	_pScene = new Scene();
	_pScene->SetRenderMode( renderType );

	//setup LightManager
	LightManager* pLightManager = _pScene->GetLigtManager();
	pLightManager->SetGamma( 2.2f );
	pLightManager->SetAmbiantIntensity( glm::vec4( 0.005f, 0.005f, 0.005f, 1.0f ) );
	pLightManager->SetLightAttenuation( g_fLightAttenuation );
	pLightManager->SetMaxIntensity( 1.0f );

	//orbit cam
	g_OrbitCam = new OrbitCamera( go_pGround );
	g_OrbitCam->SetOrbitDistance( 10.0f );
	_pScene->AddCamera( "orbit", g_OrbitCam );

	//flycam
	g_FlyCam = new FlyCam( g_initialViewData, g_viewScale );
	g_FlyCam->Name = "fly";
	_pScene->AddCamera( "fly", g_FlyCam );
	_pScene->SetActiveCamera( "fly" );

	//Add directional light
	g_pDlight = new DirectionalLight();
	g_pDlight->SetLightDirection( Framework::DegToRad( g_lightAngle ) );
	g_pDlight->SetLightIntensity( glm::vec4( 0.4f, 0.4f, 0.4f, 1.0f ) );
	pLightManager->AddDirectionalLight( g_pDlight );

	//ground plane
	go_pGround = new AssimpGameObject();
	go_pGround->ConnectRenderer( RenderFactory::CreateRenderer() );
	go_pGround->LoadModelFromFile( "data\\Models\\Plane\\plane.obj" );
	go_pGround->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
	go_pGround->GetMaterial()->SetSpecularColor( 1.0f, 1.0f, 1.0f, 1.0 );
	go_pGround->GetMaterial()->SetSpecularShininess( 0.0f );
	go_pGround->GetTransform()->pScale = new glm::vec3( 10.0f, 1.0f, 10.0f );
	go_pGround->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 1.0f );

	BulletPlaneCollider* pPlaneCollider = new BulletPlaneCollider();
	_pPhysicsEngine->AddRigidBody( pPlaneCollider->Create() );
	go_pGround->AddComponenet( pPlaneCollider );

	_pScene->AddGameObject( go_pGround );

	go_box = new AssimpGameObject();
	go_box->ConnectRenderer( RenderFactory::CreateRenderer() );
	go_box->LoadModelFromFile( "data\\Models\\Box\\box.obj" );
	go_box->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
	go_box->GetMaterial()->SetSpecularColor( 1.0f, 1.0f, 1.0f, 1.0 );
	go_box->GetMaterial()->SetSpecularShininess( 0.0f );
	go_box->GetTransform()->pOrientation = new glm::quat( glm::vec3( -0.0, 0.0f, 0.0f ) );
	go_box->GetTransform()->SetPosition( glm::vec3( 0.0f, 5.0f, 0.0f ) );
	go_box->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );

	go_Sphere1 = new AssimpGameObject();
	go_Sphere1->ConnectRenderer( RenderFactory::CreateRenderer() );
	go_Sphere1->LoadModelFromFile( "data\\Models\\Sphere\\sphere.obj" );
	go_Sphere1->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
	go_Sphere1->GetMaterial()->SetSpecularColor( 0.5f, 0.5f, 0.5f, 1.0 );
	go_Sphere1->GetMaterial()->SetSpecularShininess( 0.0f );
	go_Sphere1->GetTransform()->pOrientation = new glm::quat( glm::vec3( 0.0, 0.0f, 0.0f ) );
	go_Sphere1->GetTransform()->SetPosition( glm::vec3( 5.0, 5.0f, 0.0f ) );

	float startX = -10.0f;
	float startY = 40.0f;
	float startZ = -10.0f;

	float xPos = startX;
	float yPos = startY;
	float zPos = startZ;

	float incVal = 5.0f;

	for ( int x = 0; x < 5; x++ )
	{
		for ( int y = 0; y < 8; y++ )
		{
			for ( int z = 0; z < 8; z++ )
			{
				AssimpGameObject* pSphere = new AssimpGameObject( go_Sphere1 );
				pSphere->GetTransform()->SetPosition( glm::vec3( xPos, yPos, zPos ) );
				pSphere->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );
				pSphere->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 0.0f );

				BulletSphereCollider* pCollider = new BulletSphereCollider();
				_pPhysicsEngine->AddRigidBody( pCollider->Create( 1.0f, 1.0f, 1.0f ) );
				pSphere->AddComponenet( pCollider );

				_pScene->AddGameObject( pSphere );

				zPos += incVal;
			}

			yPos += incVal;
			zPos = startZ;
		}

		zPos = startZ;
		yPos = startY;
		xPos += incVal;
	}

	startX = -10.0f;
	startY = 100.0f;
	startZ = -10.0f;

	xPos = startX;
	yPos = startY;
	zPos = startZ;

	for ( int x = 0; x < 5; x++ )
	{
		for ( int y = 0; y < 8; y++ )
		{
			for ( int z = 0; z < 8; z++ )
			{
				AssimpGameObject* pBox = new AssimpGameObject( go_box );
				pBox->GetTransform()->SetPosition( glm::vec3( xPos, yPos, zPos ) );
				pBox->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );
				pBox->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 0.0f );

				BulletBoxCollider* boxCollider = new BulletBoxCollider();
				_pPhysicsEngine->AddRigidBody( boxCollider->Create( 1.0f, 1.0f, 1.0f, 1.0f, 1.0f ) );
				pBox->AddComponenet( boxCollider );

				_pScene->AddGameObject( pBox );

				zPos += incVal;
			}

			yPos += incVal;
			zPos = startZ;
		}

		zPos = startZ;
		yPos = startY;
		xPos += incVal;
	}

	//std::string strrender;
	//std::string phyType;

	//if ( physicsType == BulletPx )
	//{
	//	phyType = "Bullet";
	//}
	//else if ( physicsType == NvPhysX )
	//{
	//	phyType = "PhysX";
	//}

	//if ( renderType == DirectX )
	//{
	//	strrender = "Direct3D";
	//}
	//else if ( renderType == OpenGL )
	//{
	//	strrender = "OpenGL";
	//}

	//std::stringstream ssname;
	//ssname << "Objs_" << _pScene->GetNumberOfObjects() << "_" << strrender << "_" << phyType << ".txt";
	//_sceneName = ssname.str();

	//std::stringstream sstream;
	//sstream << "data\\Scenes\\";
	//sstream << "Objs_" << _pScene->GetNumberOfObjects() << "_" << strrender << "_" << phyType << ".xml";


	//SceneIO::Write( sstream.str(), _pScene->GetGameObjects(), _pScene->GetPointLights(), _pScene->GetDirectionLights(), glm::vec4( 0.2f, 0.2f, 0.02f, 1.0f ), strrender, phyType );

	_pPhysicsEngine->StartAsync();
}

void Engine::LoadObjectFromFile( std::string filePath, std::string name )
{
	glm::vec3 forward = _pScene->GetActiveCamera()->GetForwardVector();
	glm::vec3 camPos = _pScene->GetActiveCamera()->GetCurrentPosition();
	glm::vec3 infrontPos = camPos + ( forward * 20.0f );

	AssimpGameObject* pNewObject = new AssimpGameObject();
	pNewObject->SetName( name );
	pNewObject->ConnectRenderer( RenderFactory::CreateRenderer() );
	pNewObject->LoadModelFromFile( filePath );
	pNewObject->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
	pNewObject->GetMaterial()->SetSpecularColor( 1.0f, 1.0f, 1.0f, 1.0 );
	pNewObject->GetMaterial()->SetSpecularShininess( 0.0f );
	pNewObject->GetTransform()->pOrientation = new glm::quat( glm::vec3( 0.0, 0.0f, 0.0f ) );
	pNewObject->GetTransform()->SetPosition( glm::vec3( infrontPos.x, infrontPos.y, infrontPos.z ) );
	pNewObject->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );
	pNewObject->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 1.0f );

	BulletBoxCollider* boxCollider = new BulletBoxCollider();
	boxCollider->Create( 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, true );
	pNewObject->AddComponenet( boxCollider );

	_pScene->AddGameObject( pNewObject );
}

void Engine::AddPointLight( std::string name )
{
	//add point light
	glm::vec3 forward = _pScene->GetActiveCamera()->GetForwardVector();
	glm::vec3 camPos = _pScene->GetActiveCamera()->GetCurrentPosition();
	glm::vec3 infrontPos = camPos + ( forward * 20.0f );

	boost::shared_ptr<PointLight> pPointLight = boost::shared_ptr<PointLight>( new PointLight() );
	pPointLight->SetLightIntensity( glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f ) );
	pPointLight->SetLightPosition( infrontPos );

	_pScene->GetLigtManager()->AddPointLight( pPointLight );

	//add Gizmo
	LightGizmo* pNewObject = new LightGizmo( pPointLight );
	pNewObject->SetName( name );
	pNewObject->ConnectRenderer( RenderFactory::CreateRenderer() );
	pNewObject->LoadModelFromFile( "data\\Models\\Bulb\\bulb.obj" );
	pNewObject->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
	pNewObject->GetMaterial()->SetSpecularColor( 1.0f, 1.0f, 1.0f, 1.0 );
	pNewObject->GetMaterial()->SetSpecularShininess( 0.0f );
	pNewObject->GetTransform()->SetPosition( infrontPos );
	pNewObject->GetTransform()->pPosition = pPointLight->GetLightPossition();
	pNewObject->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );
	pNewObject->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 1.0f );

	BulletBoxCollider* boxCollider = new BulletBoxCollider();
	boxCollider->Create( 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, true );
	pNewObject->AddComponenet( boxCollider );

	_pScene->AddGizmo( pNewObject );
}

void Engine::SetListener( BuilderFrameEx* pFrame, SelectionListenerCallback cbFunc )
{
	SelectionListener = cbFunc;
	pListenFrame = pFrame;
}

void Engine::SetListener( RuntimeFrameEx* pFrame, FrameListenerCallback cbFunc, FrameListenerCallback phyFun )
{
	pRunListenFrame = pFrame;
	FrameListener = cbFunc;
	PhysicsListener = phyFun;
}

void Engine::SaveSceneToFile( std::string fileLocation, std::string renderer, std::string physicsEngine )
{
	glm::vec4 ambLight = _pScene->GetLigtManager()->GetAmbiantIntensity();
	SceneIO::Write( fileLocation, _pScene->GetGameObjects(), _pScene->GetPointLights(), _pScene->GetDirectionLights(), ambLight, renderer, physicsEngine );
}

void Engine::SelectObjectWithName( std::string name )
{
	_pScene->SelectObjectWithName( name );

	if ( pListenFrame != NULL )
	{
		( pListenFrame->*SelectionListener )( _pScene->GetSelectedObject() );
	}
}

Scene* Engine::GetScene()
{
	return _pScene;
}

void Engine::CalculateFPS()
{
	boost::posix_time::ptime timeNow = boost::posix_time::microsec_clock::local_time();
	boost::posix_time::time_duration diff = timeNow - _lastFPSTime;

	if ( diff.seconds() >= 1.0 )
	{
		WriteStats();

		_fps = (  double( _frameCounter ) );
		_lastFPSTime = timeNow;
		_frameCounter = 0;

		if ( pRunListenFrame != NULL )
		{
			( pRunListenFrame->*FrameListener )( _fps );
		}

		if ( pRunListenFrame != NULL )
		{
			( pRunListenFrame->*PhysicsListener )( _pPhysicsEngine->GetLastTimeStep() );
		}
	}

}

void Engine::RemoveSelectedObjectFromScene()
{
	_pScene->RemoveSelectedObjectFromScene();
	_pScene->SelectFirstObject();

	if ( pListenFrame != NULL )
	{
		( pListenFrame->*SelectionListener )( _pScene->GetSelectedObject() );
	}
}

LoadingOutputData Engine::LoadFromFileBuilderMode( std::string filePath )
{
	_pScene->LoadFromFIle( filePath, _pPhysicsEngine );
	_pScene->RemoveRigidBodies();
	_pScene->CreateGizmoesForLights();

	LoadingOutputData data;
	data.objectNameList = _pScene->GetSceneObjectNameList();

	return data;
}

void Engine::LoadFromFileRuntimeMode( std::string filePath )
{
	_pScene->LoadFromFIle( filePath, _pPhysicsEngine );
	_pPhysicsEngine->SetInitialStep( true );
}

void Engine::WriteStats()
{
	if ( _recordStats )
	{
		boost::posix_time::ptime timeNow = boost::posix_time::microsec_clock::local_time();
		boost::posix_time::time_duration diff = timeNow - _startTime;

		//record for  mins then write stats
		if ( diff.seconds() > 300.0f && _statsWritain == false )
		{
			int fpsAverage = 0;

			BOOST_FOREACH( int val, _fpsList )
			{
				fpsAverage += val;
			}

			fpsAverage = fpsAverage / _fpsList.size();

			float latencyAverage = 0;

			BOOST_FOREACH( float val, _latencyList )
			{
				latencyAverage += val;
			}

			latencyAverage = latencyAverage / _latencyList.size();

			_statsWritain = true;
			std::ofstream myfile;
			myfile.open( "data\\Results\\" + _sceneName );
			myfile << "SceneName: " << _sceneName << "\n";
			myfile << "Runtime: " << "5:00" << "\n";
			myfile << "Number of Objects: " << GetScene()->GetNumberOfObjects() << "\n";
			myfile << "Number of Vertices: " << _pScene->GetTotalNumberOfVertciesInScene() << "\n";
			myfile << "Average FPS:  " << fpsAverage << "\n";
			myfile << "Average Physics Latency:  " << latencyAverage << "\n";

			myfile.close();
		}
		else
		{
			_fpsList.push_back( _fps );
			_latencyList.push_back( _pPhysicsEngine->GetLastTimeStep() );
		}
	}
}

void Engine::Display()
{
	_frameCounter++;
	CalculateFPS();
	_pScene->Render();
}

void Engine::Reshape( int w, int h )
{
	_pScene->ResetSceneCameraToClipMatrix( w, h );
	_pScene->SetScreenDimentions( w, h );
}

void Engine::Keyboard( unsigned char key, int x, int y )
{
	switch ( key )
	{
	case 27:
		return;
	case '=':
	{
	}; break;
	case '-':
	{

	}; break;
	case 't':
	{
		_orientation += 0.01f;
		go_box->GetTransform()->pOrientation = new glm::quat( glm::vec3( _orientation, 0.0f, 0.0f ) );
	}; break;
	case 'P':
	{
		_startTime = boost::posix_time::microsec_clock::local_time();
		_recordStats = true;
		_pPhysicsEngine->Run();

	}; break;
	default:
	{
		if ( _pScene->GetActiveCamera()->Name == "fly" )
		{
			g_FlyCam->HandleKeyPress( key );
		}
	};

	}
}

void Engine::MouseMotion( int x, int y )
{
	if ( _pScene->GetActiveCamera()->Name == "fly" )
	{
		g_FlyCam->HandleMouseMotion( x, y );
	}
	else
	{
		float iXChange = x - v2Last.x;
		float iYChange = y - v2Last.y;

		float fXIncrease = 0.0f;
		float fYIncrease = 0.0f;
		float fZIncrease = 0.0f;

		if ( iXChange > 0 )
		{
			fYIncrease = 1.0f;
		}
		else if ( iXChange < 0 )
		{
			fYIncrease = -1.0f;
		}

		if ( iYChange > 0 )
		{
			fXIncrease = 1.0f;
		}
		else if ( iYChange < 0 )
		{
			fXIncrease = -1.0f;
		}

		g_OrbitCam->ChangeOrientation( fXIncrease, fYIncrease, fZIncrease );

		v2Last.x = ( float )x;
		v2Last.y = ( float )y;
	}
}

void Engine::MouseButton( int button, int state, int x, int y )
{
	if ( _pScene->GetActiveCamera()->Name == "fly" )
	{
		g_FlyCam->HandleMouseButton( button, state, x, y );
	}

	if ( button == 2 && state == 0 )
	{
		_pScene->SelectObject( x, y );
		
		if ( pListenFrame != NULL )
		{
			( pListenFrame->*SelectionListener )( _pScene->GetSelectedObject() );
		}
	}
}

void Engine::MouseWheel( int wheel, int direction, int x, int y )
{
	if ( direction == 1 )
	{
		g_OrbitCam->ChangeOrbitDistance( 0.5f );
	}

	else if ( direction == -1 )
	{
		g_OrbitCam->ChangeOrbitDistance( -0.5f );
	}
}

