struct PerLight
{
	float4 cameraSpaceLightPos;
	float4 lightIntensity;
};

cbuffer ConstantBuffer : register(b0)
{
	float4 diffuseColor;
	float4 specularColor;	
	float4x4 modelToCameraMatrix;
	float4x4 normalModelToCameraMatrix;
	float specularShininess;
}

cbuffer ConstantLightBuffer : register(b1)
{
    float4 ambientIntensity;
	
	PerLight lights[3];

	float lightAttenuation;
	float maxIntensity;
	float gamma;
}

cbuffer ConstantProjectionBuffer : register(b2)
{
	float4x4 cameraToClipMatrix;
}

Texture2D Texture;
SamplerState ss;

struct VOut
{
    float4 color : COLOR;
    float2 texcoord : TEXCOORD; 
	float4 cameraSpacePosition : TEXCOORD1;
	float4 vertexNormal : NORMAL;
    float4 position : SV_POSITION;
};

VOut VShader(float3 position : POSITION, float3 normal : NORMAL, float2 texcoord : TEXCOORD)
{
    VOut output;
	float4 nPosition = float4( position.xyz, 1.0 );
	
	//set outputs
	float4 tempCamPosition = mul(modelToCameraMatrix, nPosition);
	output.position = mul(cameraToClipMatrix, tempCamPosition);
	output.texcoord = texcoord;
	output.cameraSpacePosition = tempCamPosition;
    output.vertexNormal = normalize( mul( normalModelToCameraMatrix, float4( normal.xyz, 1.0) ) );
	
    return output;
}

float CalcAttenuation(in float3 cameraSpacePosition, in float3 cameraSpaceLightPos,	out float3 lightDirection)
{
	float3 lightDifference =  cameraSpaceLightPos - cameraSpacePosition;
	float lightDistanceSqr = dot(lightDifference, lightDifference);
	lightDirection = lightDifference * rsqrt(lightDistanceSqr);
	
	return (1 / ( 1.0 + lightAttenuation * lightDistanceSqr));
}

float4 ComputeLighting( PerLight lightData, float3 cameraSpacePosition, float2 texcoord, float3 vertexNormal )
{
	float3 lightDir = float3(0,0,0);
	float4 lightIntensity = float4(0,0,0,0);
	
	if(lightData.cameraSpaceLightPos.w == 0.0)
	{
		lightDir = lightData.cameraSpaceLightPos.xyz;
		lightIntensity = lightData.lightIntensity;
	}
	else
	{
		float atten = CalcAttenuation(cameraSpacePosition, lightData.cameraSpaceLightPos, lightDir);
		lightIntensity = mul( atten, lightData.lightIntensity );
	}
	
	float3 surfaceNormal = normalize( vertexNormal );
	float cosAngIncidence = dot(surfaceNormal, lightDir);
	cosAngIncidence = cosAngIncidence < 0.0001 ? 0.0 : cosAngIncidence;
	
	float4 lighting = Texture.Sample(ss, texcoord) * lightIntensity * cosAngIncidence;
	
	return lighting;
}

float4 PShader(float4 color : COLOR, float2 texcoord : TEXCOORD, float4 cameraSpacePosition : TEXCOORD1, float4 vertexNormal : NORMAL ) : SV_TARGET
{
	float4 accumLighting = diffuseColor + Texture.Sample(ss, texcoord) * ambientIntensity;
	
	for(int light = 0; light < 3; light++)
	{
		accumLighting += ComputeLighting(lights[light], cameraSpacePosition.xyz, texcoord, vertexNormal.xyz );
	}
	
	accumLighting = accumLighting / maxIntensity;
	float div = 1.0 / gamma;
	float4 fgamma = float4( div, div, div, 1.0 );
	
	return pow( accumLighting, fgamma );
}
