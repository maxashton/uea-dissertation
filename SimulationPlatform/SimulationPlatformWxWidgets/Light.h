#pragma once

#include "Common.h"

class Light
{

public:

	Light();
	virtual ~Light();

	void SetLightPosition( glm::vec3 v3Pos );
	void SetLightIntensity( glm::vec4 v4LightIntensity );
	boost::shared_ptr<glm::vec3> GetLightPossition();
	glm::vec4 GetLightIntensity();

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );

protected:

	boost::shared_ptr<glm::vec3> _v3Pos;
	glm::vec4 _v4LightIntensity;
};

