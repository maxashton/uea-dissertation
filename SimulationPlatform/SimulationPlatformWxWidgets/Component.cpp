#include "Component.h"

Component::Component()
{
	_draw = false;
}

Component::~Component()
{
	_pGameObject = NULL;
}

void Component::Update()
{

}

void Component::Draw( glm::mat4 mCameraMatrix )
{

}

void Component::AttachGameObject( GameObject* pGameObject )
{
	this->_pGameObject = pGameObject;
}

void Component::SetToDraw( bool draw )
{
	_draw = draw;
}

void Component::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType )
{

}

void Component::DeSerializeFromXML( ptree::value_type const& v )
{

}

IdentifyType Component::Identify()
{
	return IDComponent;
}
