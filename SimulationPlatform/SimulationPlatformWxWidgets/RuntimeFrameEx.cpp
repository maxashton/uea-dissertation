#include "RuntimeFrameEx.h"
#include "OpenGLCanvas.h"
#include "D3D11Canvas.h"
#include "SceneIO.h"
#include <iostream>
#include <fstream>

RuntimeFrameEx::RuntimeFrameEx( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : RuntimeFrame( parent, id, title, pos, size, style )
{
	wxFileDialog openFileDialog( this, _( "Scene file" ), "", "", "Scene file (*.xml)|*.xml", wxFD_OPEN | wxFD_FILE_MUST_EXIST );

	// if quit
	if ( openFileDialog.ShowModal() == wxID_CANCEL )
	{
		//exit( 0 );
	}

	LoadingOutputData settingsData = SceneIO::ReadSettings( openFileDialog.GetPath().ToStdString() );
	int renderType = ( settingsData.renderer == "Direct3D 11" ) ? DirectX : OpenGL;
	int physxType = ( settingsData.physicsEngine == "Bullet" || settingsData.physicsEngine == "Bullet Physics" ) ? 1 : 2;

	if ( renderType == OpenGL )
	{
		int args [] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };
		OpenGLCanvas* canvas = new OpenGLCanvas( m_panel1, wxSize( 1000, 1000 ), args );
		canvas->Initialize( physxType );
		_pEngine = canvas->GetEngine();
		_pEngine->GetScene()->GetLigtManager()->SetAmbiantIntensity( settingsData.ambientLight );
		_pEngine->LoadFromFileRuntimeMode( openFileDialog.GetPath().ToStdString() );
		_pEngine->SetListener( this, &RuntimeFrameEx::FPSCallback, &RuntimeFrameEx::PhysicsCallback );
		_renderPanel = canvas;
	}
	else if ( renderType == DirectX )
	{
		D3D11Canvas* canvas = new D3D11Canvas( m_panel1, wxID_ANY, wxDefaultPosition, wxSize( 1000, 1000 ), wxSUNKEN_BORDER );
		canvas->Initialize( physxType );
		_pEngine = canvas->GetEngine();
		_pEngine->GetScene()->GetLigtManager()->SetAmbiantIntensity( settingsData.ambientLight );
		_pEngine->SetListener( this, &RuntimeFrameEx::FPSCallback, &RuntimeFrameEx::PhysicsCallback );
		_pEngine->LoadFromFileRuntimeMode( openFileDialog.GetPath().ToStdString() );
		_renderPanel = canvas;
	}

	bSizer2->Add( _renderPanel, 1, wxEXPAND | wxALL, 5 );
}

RuntimeFrameEx::~RuntimeFrameEx()
{
}

void RuntimeFrameEx::FPSCallback( double fps )
{
	m_staticText2->SetLabelText( wxString::Format( wxT( "%g" ), fps ) );
}

void RuntimeFrameEx::PhysicsCallback( double fps )
{
	m_staticText4->SetLabelText( wxString::Format( wxT( "%g" ), fps ) );
}
