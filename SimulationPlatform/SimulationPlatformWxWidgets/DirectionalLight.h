#pragma once

#include "Common.h"
#include "Light.h"
#include <glm/glm.hpp>

class DirectionalLight : public Light
{

public:

	DirectionalLight();
	virtual ~DirectionalLight();

	void SetLightDirection( float fDir );
	glm::vec4 GetLightDirection();
	float GetLightDirectionFloat();

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );

protected:

	float _fLightDirection;

};

