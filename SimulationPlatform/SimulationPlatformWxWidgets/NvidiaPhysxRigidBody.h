#pragma once
#include "RigidBody.h"
#include <PxPhysicsAPI.h>

class NvidiaPhysxRigidBody : public RigidBody
{

public:

	NvidiaPhysxRigidBody();
	virtual ~NvidiaPhysxRigidBody();

	physx::PxActor* GetRigidBody();
	void SetRigidBody( physx::PxRigidBody* body );
	void SetStaticBody( physx::PxRigidStatic* body );


	virtual glm::mat4 GetModelMatrix();
	virtual void SetModelMatrix( boost::shared_ptr<glm::vec3> position );

	virtual void SetMass( float val );
	virtual void SetElasticity( float val );

protected:

	physx::PxRigidBody* _pRigidBody;
	physx::PxRigidStatic* _pStaticBody;


};

