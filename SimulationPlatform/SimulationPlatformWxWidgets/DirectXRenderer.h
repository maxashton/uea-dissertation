#ifdef _WIN32
#pragma once

#include "Common.h"
#include "Renderer.h"
#include "Direct3DShader.h"

struct VERTEX { FLOAT X, Y, Z; };
struct ASSIMP_VERTEX { FLOAT X, Y, Z; D3DXVECTOR3 Normal; FLOAT U, V; };
struct GEOMETRIC_VERTEX { FLOAT X, Y, Z; };

class DirectXRenderer :	public Renderer
{

public:

	DirectXRenderer();
	virtual ~DirectXRenderer();

	static void UpdateLightingBuffers( LightBlock* lightData );
	static void Clear();

	virtual void DrawIcoSphere( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies );
	virtual void DrawGeoPlane( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies );
	virtual void DrawAssimpGameObject( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform,
										Material* pMatrial, int iNumMeshes, std::vector<int> materialIndices, std::vector<Texture*>* textures,
										std::vector<int> meshStartIndices, std::vector<int> meshSizes );

	virtual void DrawAssimpGameObjectPicking( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform,
												int iNumMeshes, std::vector<int> materialIndices, std::vector<int> meshStartIndices,
												std::vector<int> meshSizes, glm::vec4 color );

	virtual void BuildAssimpGameObjectDiffuseShader();
	virtual void BuildAssimGameObjectSpecularShader();
	virtual void InitializeAssimpModelVertexBuffer( std::vector<BYTE> modelData );

	virtual void BuildGeometricSpecularShader();
	virtual void BuildGeometricDiffuseShader();
	virtual void InitializeGeometricVertexBuffer( std::vector<short> indicies, std::vector<glm::vec3> geomtery );

	virtual Texture* LoadTexture( std::string path );

protected:

	Direct3DShader* _pShader;
	Direct3DShader* _pPickingShader;

	ID3D11Buffer* _pVBuffer;
	ID3D11Buffer* _pIBuffer;
};
#endif