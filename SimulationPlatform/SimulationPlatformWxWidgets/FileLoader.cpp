#include "FileLoader.h"
#include <iostream>
#include <fstream>

FileLoader::FileLoader()
{
}


FileLoader::~FileLoader()
{
}

std::vector<std::string> FileLoader::LoadInit()
{
	std::string line;
	std::ifstream myfile( "data//init.txt" );
	std::vector<std::string> vStrings;

	int iLine=0;
	if ( myfile.is_open() )
	{
		while ( getline( myfile, line ) )
		{
			if ( iLine == 1 )
			{
				vStrings.push_back( line.substr( 7 ) );
			}
			else if ( iLine == 2 )
			{
				vStrings.push_back( line.substr( 7 ) );
			}

			iLine++;
		}
		myfile.close();
	}

	return vStrings;
}
