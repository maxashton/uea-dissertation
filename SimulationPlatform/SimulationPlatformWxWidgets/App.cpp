#include "App.h"

bool MyApp::OnInit()
{
	wxBoxSizer* sizer = new wxBoxSizer( wxHORIZONTAL );
	frame = new wxFrame( ( wxFrame * )NULL, -1, wxT( "Hello GL World" ), wxPoint( 50, 50 ), wxSize( 400, 200 ) );

	int args [] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };

	glPane = new BasicGLPane( ( wxFrame* )frame, args );
	sizer->Add( glPane, 1, wxEXPAND );

	frame->SetSizer( sizer );
	frame->SetAutoLayout( true );

	frame->Show();
	return true;
}