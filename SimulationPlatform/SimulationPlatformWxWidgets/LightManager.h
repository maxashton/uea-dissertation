#pragma once

#include "Common.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Structs.h"

class LightManager
{

public:

	LightManager();
	virtual ~LightManager();

	void AddPointLight( boost::shared_ptr<PointLight> pLight );
	void RemoveLight( boost::shared_ptr<Light> pLight );
	void AddDirectionalLight( DirectionalLight* pLight );
	void SetGamma( float fGamma );
	void SetAmbiantIntensity( glm::vec4 v4AmbiantIntensity );
	void SetLightAttenuation( float fLightAttenuation );
	void SetMaxIntensity( float fMaxIntensity );
	float GetGamma();
	std::vector<boost::shared_ptr<PointLight> >* GetPointLights();
	std::vector<DirectionalLight*>* GetDirectionLights();
	glm::vec4 GetAmbiantIntensity();
	
	LightBlock GetLightInfo( const glm::mat4 &worldToCameraMat ) const;

protected:

	std::vector<boost::shared_ptr<PointLight> >* _pvPointLights;
	std::vector<DirectionalLight*>* _pvDirectionalLights;

	float _fGamma;
	float _fLightAttenuation;
	float _fMaxItensity;
	glm::vec4 _v4AmbientIntensity;
};

