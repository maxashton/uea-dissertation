#pragma once

#include "Common.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Transform
{

public:
	Transform();
	virtual ~Transform();

	glm::vec3* pScale;
	boost::shared_ptr<glm::vec3> pPosition;
	glm::fquat* pOrientation;

	void SetPosition( glm::vec3 position );

};
 