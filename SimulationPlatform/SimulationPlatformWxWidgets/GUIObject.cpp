#include "GUIObject.h"
#include <FreeImage.h>

//GUIObject::GUIObject( std::string stdTextureLocation, std::string stdSelectedTextureLocation, glm::vec3 v3ColorID, float fWidth, float fHeight )
//{
//	//_fWidth = fWidth;
//	//_fHeight = fHeight;
//
//	//_aiColorID = aiVector3D( v3ColorID.x, v3ColorID.y, v3ColorID.z );
//	//_v3ColorID = v3ColorID;
//	//_iSelecedCounter = 0;
//
//	//BuildGUIShader();
//	//BuildPickingShader();
//	//CreateVertexList();
//	//LoadTextures( stdTextureLocation, stdSelectedTextureLocation );
//}
//
//GUIObject::~GUIObject()
//{
//}
//
//void GUIObject::Draw( glutil::MatrixStack mCameraMatrix )
//{
	//_msModelSpaceMatrix = glutil::MatrixStack(  );
	//ShaderPropoties strctShaderProps = _pShader->GetShaderPropoties();

	//glUseProgram( strctShaderProps.ProgramID );
	//{
	//	//set uniform material data
	//	glBindBuffer( GL_UNIFORM_BUFFER, Shader::MATERIAL_UNIFORM_BUFFER );
	//	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( Material ), &_pMaterial->GetData() );
	//	glBindBuffer( GL_UNIFORM_BUFFER, 0 );

	//	{
	//		_msModelSpaceMatrix.Translate( *this->pTransform->pPosition );
	//		_msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *this->pTransform->pOrientation ) );
	//		_msModelSpaceMatrix.Scale( this->pTransform->pScale->x, this->pTransform->pScale->y, this->pTransform->pScale->z );

	//		glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( _msModelSpaceMatrix.Top() ) );

	//		glm::mat3 normMatrix( _msModelSpaceMatrix.Top() );
	//		normMatrix = glm::transpose( glm::inverse( normMatrix ) );
	//		glUniformMatrix3fv( _pShader->GetShaderPropoties().NormalModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( normMatrix ) );

	//		_vboData.BindVBO();
	//		glActiveTexture( GL_TEXTURE0 );
	//		glBindTexture( GL_TEXTURE_2D, ( !_bSelected ) ? _uiIdleTexture : _iSelectedTexture );
	//		glUniform1i( strctShaderProps.TextureSamplerUnif, 0 );

	//		glBindVertexArray( _uiVAO );
	//		// Vertex positions
	//		glEnableVertexAttribArray( 0 );
	//		glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), 0 );
	//		// Texture coordinates
	//		glEnableVertexAttribArray( 1 );
	//		glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), (void*)sizeof( aiVector3D ) );
	//		// Normal vectors
	//		glEnableVertexAttribArray( 2 );
	//		glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), (void*)(sizeof( aiVector3D ) + sizeof( aiVector2D )) );

	//		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

	//		glDisableVertexAttribArray( 0 );
	//		glDisableVertexAttribArray( 1 );
	//		glDisableVertexAttribArray( 2 );
	//	}
	//}
	//glUseProgram( 0 );

	//if ( _bSelected )
	//{
	//	if ( _iSelecedCounter > 5 )
	//	{
	//		_iSelecedCounter = 0;
	//		_bSelected = false;
	//	}
	//	else
	//	{
	//		_iSelecedCounter++;
	//	}
	//}

//}

//void GUIObject::DrawPicking()
//{
	//_msModelSpaceMatrix = glutil::MatrixStack();
	//ShaderPropoties strctShaderProps = _pickingShader.GetShaderPropoties();

	//glUseProgram( strctShaderProps.ProgramID );
	//{
	//	//set uniform material data
	//	glBindBuffer( GL_UNIFORM_BUFFER, Shader::MATERIAL_UNIFORM_BUFFER );
	//	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( Material ), &_pMaterial->GetData() );
	//	glBindBuffer( GL_UNIFORM_BUFFER, 0 );

	//	{
	//		_msModelSpaceMatrix.Translate( *this->pTransform->pPosition );
	//		_msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *this->pTransform->pOrientation ) );
	//		_msModelSpaceMatrix.Scale( this->pTransform->pScale->x, this->pTransform->pScale->y, this->pTransform->pScale->z );

	//		glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( _msModelSpaceMatrix.Top() ) );

	//		glm::mat3 normMatrix( _msModelSpaceMatrix.Top() );
	//		normMatrix = glm::transpose( glm::inverse( normMatrix ) );
	//		glUniformMatrix3fv( _pShader->GetShaderPropoties().NormalModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( normMatrix ) );

	//		_vboData.BindVBO();
	//		glActiveTexture( GL_TEXTURE0 );
	//		glBindTexture( GL_TEXTURE_2D, _uiIdleTexture );
	//		glUniform1i( strctShaderProps.TextureSamplerUnif, 0 );

	//		glBindVertexArray( _uiVAO );
	//		// Vertex positions
	//		glEnableVertexAttribArray( 0 );
	//		glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), 0 );
	//		// Texture coordinates
	//		glEnableVertexAttribArray( 1 );
	//		glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )sizeof( aiVector3D ) );
	//		// colorID vectors
	//		glEnableVertexAttribArray( 2 );
	//		glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )( sizeof( aiVector3D ) + sizeof( aiVector2D ) ) );

	//		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

	//		glDisableVertexAttribArray( 0 );
	//		glDisableVertexAttribArray( 1 );
	//		glDisableVertexAttribArray( 2 );
	//	}
	//}
	//glUseProgram( 0 );
//}

//void GUIObject::BuildPickingShader()
//{
/*	_pickingShader.AddShader( GL_VERTEX_SHADER, "Shaders\\GUIGameObject\\default.vert" );
	_pickingShader.AddShader( GL_FRAGMENT_SHADER, "Shaders\\GUIGameObject\\Picking.frag" );

	_pickingShader.CreatePrograme()*/;
//}
//
//void GUIObject::BuildGUIShader()
//{
/*	_pShader->AddShader( GL_VERTEX_SHADER, "Shaders\\GUIGameObject\\default.vert" );
	_pShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\GUIGameObject\\Texture.frag" );

	_pShader->CreatePrograme()*/;
//}

//void GUIObject::CreateVertexList()
//{
//	float fHalfWidth = _fWidth / 2.0f;
//	float fHalfHeight = _fHeight / 2.0f;
//	_vVertexList.push_back( aiVector3D( -fHalfWidth, -fHalfHeight, 0.0 ) );
//	_vVertexList.push_back( aiVector3D( -fHalfWidth, fHalfHeight, 0.0f ) );
//	_vVertexList.push_back( aiVector3D( fHalfWidth, -fHalfHeight, 0.0f ) );
//	_vVertexList.push_back( aiVector3D( fHalfWidth, fHalfHeight, 0.0f ) );
//
//	_vUVList.push_back( aiVector2D( 0, 1 ) );
//	_vUVList.push_back( aiVector2D( 0, 0 ) );
//	_vUVList.push_back( aiVector2D( 1, 1 ) );
//	_vUVList.push_back( aiVector2D( 1, 0 ) );
//
//	glGenVertexArrays( 1, &_uiVAO );
//	glBindVertexArray( _uiVAO );

	//_vboData.CreateVBO();
	//_vboData.BindVBO();

	//for ( unsigned int i = 0; i < _vVertexList.size(); i++ )
	//{
	//	_vboData.AddData( &_vVertexList[i], sizeof(aiVector3D));
	//	_vboData.AddData( &_vUVList[i], sizeof( aiVector2D ) );
	//	_vboData.AddData( &_v3ColorID, sizeof( aiVector3D ) );
	//}

	//_vboData.UploadDataToGPU( GL_STATIC_DRAW );

	//glBindBuffer( GL_ARRAY_BUFFER, 0 );
//}
//
//void LoadTexture( std::string stdLocation, GLuint &ID )
//{
//	///Now the interesting code:
//	FREE_IMAGE_FORMAT formato = FreeImage_GetFileType( stdLocation.c_str(), 0 );
//	FIBITMAP* imagen = FreeImage_Load( formato, stdLocation.c_str() );
//
//	FIBITMAP* temp = imagen;
//	imagen = FreeImage_ConvertTo32Bits( imagen );
//	FreeImage_Unload( temp );
//
//	int w = FreeImage_GetWidth( imagen );
//	int h = FreeImage_GetHeight( imagen );
//
//	GLubyte* textura = new GLubyte [4 * w*h];
//	char* pixeles = ( char* )FreeImage_GetBits( imagen );
//
//	//for ( int j = 0; j < w*h; j++ )
//	//{
//	//	textura [j * 4 + 0] = pixeles [j * 4 + 2];
//	//	textura [j * 4 + 1] = pixeles [j * 4 + 1];
//	//	textura [j * 4 + 2] = pixeles [j * 4 + 0];
//	//	textura [j * 4 + 3] = pixeles [j * 4 + 3];
//	//}
//
//	glEnable( GL_TEXTURE_2D );
//	glGenTextures( 1, &ID );
//	glBindTexture( GL_TEXTURE_2D, ID );
//	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, ( GLvoid* )pixeles );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//}
//
//void GUIObject::LoadTextures( std::string stdTextureLocation, std::string stdSelectedTextureLocation )
//{
//	LoadTexture( stdTextureLocation, _uiIdleTexture );
//	LoadTexture( stdSelectedTextureLocation, _iSelectedTexture );
//}
//
//void GUIObject::SetSelected( bool bSelected )
//{
//	_bSelected = bSelected;
//
//	if ( bSelected && _fpClick != NULL )
//	{
//		( _fpClick )();
//	}
//}
//
//glm::vec3 GUIObject::GetColorID()
//{
//	return _v3ColorID;
//}
//
//void GUIObject::AttatchClickEvent( click_event fpEvent )
//{
//	_fpClick = fpEvent;
//}
