#pragma once

#include "Common.h"
#include "GameObject.h"
#include "Texture.h"
#include "RenderFactory.h"

class AssimpGameObject : public GameObject
{

public:

	AssimpGameObject();
	AssimpGameObject( AssimpGameObject* otherObject );
	AssimpGameObject( RenderFactory factory );

	virtual ~AssimpGameObject();

	bool LoadModelFromFile( std::string filePath );

	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual void DrawPicking( glm::mat4 mCameraMatrix );

	virtual void ConnectRenderer( Renderer* pRenderer );

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );

	virtual void DeSerializeFromXML( ptree::value_type const& v );

	virtual int GetNumberOfVertesies();

protected:

	bool _bLoaded;
	std::vector<Texture*>* _pTextures;
	std::vector<int> _iMeshStartIndices;
	std::vector<int> _iMeshSizes;
	std::vector<int> _iMaterialIndices;
	int _iNumMaterials;

	int _dataSize;
	std::vector<BYTE> _modelData;
	std::string _filePath;

	bool LoadModelFromFileLogic( const char* sFilePath );
	void AddModelData( void* ptrData, UINT uiDataSize );
};

