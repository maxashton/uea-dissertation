#include "Direct3DTexture.h"
#include "Direct3DShader.h"

Direct3DTexture::Direct3DTexture()
{
}


Direct3DTexture::~Direct3DTexture()
{
}

void Direct3DTexture::BindTexture()
{
	Direct3DShader::DeviceContext->PSSetShaderResources( 0, 1, &_pTexture );
}

bool Direct3DTexture::LoadTexture2D( std::string sPath )
{
	std::wstring stemp = std::wstring( sPath.begin(), sPath.end() );

	D3DX11CreateShaderResourceViewFromFile( Direct3DShader::Device,        // the Direct3D device
		stemp.c_str(),    // load Wood.png in the local folder
		NULL,           // no additional information
		NULL,           // no multithreading
		&_pTexture,      // address of the shader-resource-view
		NULL );          // no multithreading

	return true;
}

Texture* Direct3DTexture::Clone()
{
	Direct3DTexture* pTexture = new Direct3DTexture();
	pTexture->_bMipMapsGenerated = this->_bMipMapsGenerated;
	pTexture->_iBPP = this->_iBPP;
	pTexture->_iHeight = this->_iHeight;
	pTexture->_iWidth = this->_iWidth;
	pTexture->_sPath = this->_sPath;
	pTexture->_tfMagnification = this->_tfMagnification;
	pTexture->_tfMinification = this->_tfMinification;
	pTexture->_pTexture = this->_pTexture;

	return pTexture;
}
