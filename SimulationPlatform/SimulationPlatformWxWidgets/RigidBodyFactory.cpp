#include "RigidBodyFactory.h"
#include "PhysxPhysicsEngine.h"
#include "BulletRigidBody.h"
#include <bullet/btBulletDynamicsCommon.h>
#include <PxPhysicsAPI.h>
#include "NvidiaPhysxRigidBody.h"

int RigidBodyFactory::_type = 1;

RigidBodyFactory::RigidBodyFactory()
{
}


RigidBodyFactory::~RigidBodyFactory()
{
}

void RigidBodyFactory::SetType( PhysicsType etype )
{
	_type = etype;
}

boost::shared_ptr<RigidBody> RigidBodyFactory::CreatePlaneBody()
{
	if ( _type == BulletPx )
	{
		boost::shared_ptr<BulletRigidBody> pRigidBody = boost::shared_ptr<BulletRigidBody>( new BulletRigidBody() );

		btTransform t;
		t.setIdentity();
		t.setOrigin( btVector3( 0, 0, 0 ) );
		btStaticPlaneShape* plane = new btStaticPlaneShape( btVector3( 0, 1, 0 ), 0 );

		btMotionState* motion = new btDefaultMotionState( t );
		btRigidBody::btRigidBodyConstructionInfo info( 0.0, motion, plane );
		boost::shared_ptr<btRigidBody> pbtBody = boost::shared_ptr<btRigidBody>( new btRigidBody( info ) );
		pRigidBody->SetRigidBody( pbtBody );

		return pRigidBody;
	}
	else if ( _type == NvPhysX )
	{
		boost::shared_ptr<NvidiaPhysxRigidBody> pRigidBody = boost::shared_ptr<NvidiaPhysxRigidBody>( new NvidiaPhysxRigidBody() );

		physx::PxMaterial* material = PhysxPhysicsEngine::gPhysicsSDK->createMaterial( 0.5, 0.5, 0.5 ); //Creating a PhysX material

		physx::PxTransform planePos = physx::PxTransform( physx::PxVec3( 0.0f ), physx::PxQuat( physx::PxHalfPi, physx::PxVec3( 0.0f, 0.0f, 1.0f ) ) );	//Position and orientation(transform) for plane actor  
		physx::PxRigidStatic* plane = PhysxPhysicsEngine::gPhysicsSDK->createRigidStatic( planePos ); //Creating rigid static actor
		plane->createShape( physx::PxPlaneGeometry(), *material ); //Defining geometry for plane actor

		pRigidBody->SetStaticBody( plane );

		return pRigidBody;
	}

	return NULL;
}

boost::shared_ptr<RigidBody> RigidBodyFactory::CreateSphereBody( float mass, float elasticity, float radious )
{
	if ( _type == BulletPx )
	{
		boost::shared_ptr<BulletRigidBody> pRigidBody = boost::shared_ptr<BulletRigidBody>( new BulletRigidBody() );

		btTransform t;
		t.setIdentity();
		t.setOrigin( btVector3( 0, 0, 0 ) );
		btSphereShape* sphere = new btSphereShape( radious );
		btVector3 inertia( 0, 0, 0 );

		if ( mass != 0.0 )
		{
			sphere->calculateLocalInertia( mass, inertia );
		}

		btMotionState* motion = new btDefaultMotionState( t );
		btRigidBody::btRigidBodyConstructionInfo info( mass, motion, sphere, inertia );;

		boost::shared_ptr<btRigidBody> pbtBody = boost::shared_ptr<btRigidBody>( new btRigidBody( info ) );
		pRigidBody->SetRigidBody( pbtBody );

		if ( elasticity != 0.0 )
		{
			pbtBody->setRestitution( elasticity );
		}
		else if ( mass == 0 )
		{
			pbtBody->setRestitution( 1.0 );
		}

		return pRigidBody;
	}
	else if ( _type == NvPhysX )
	{
		boost::shared_ptr<NvidiaPhysxRigidBody> pRigidBody = boost::shared_ptr<NvidiaPhysxRigidBody>( new NvidiaPhysxRigidBody() );

		physx::PxMaterial* material = PhysxPhysicsEngine::gPhysicsSDK->createMaterial( 0.0, 0.0, elasticity ); //Creating a PhysX material

		PxTransform		boxPos( PxVec3( 0.0f, 00.0f, 0.0f ) );												//Position and orientation(transform) for box actor 
		PxSphereGeometry boxGeometry( radious );


		physx::PxRigidDynamic* sphere = physx::PxCreateDynamic( *PhysxPhysicsEngine::gPhysicsSDK, boxPos, boxGeometry, *material, 1.0f ); //Creating rigid static actor	

		sphere->setMass( mass );

		pRigidBody->SetRigidBody( sphere );

		return pRigidBody;
	}

	return NULL;
}

boost::shared_ptr<RigidBody> RigidBodyFactory::CreateBoxBody( float mass, float elasticity, float width, float height, float depth )
{
	if ( _type == BulletPx )
	{
		boost::shared_ptr<BulletRigidBody> pRigidBody = boost::shared_ptr<BulletRigidBody>( new BulletRigidBody() );

		btTransform t;
		t.setIdentity();
		t.setOrigin( btVector3( 0, 0, 0 ) );
		btVector3 inertia( 0, 0, 0 );

		btBoxShape* sphere = new btBoxShape( btVector3( width / 2.0f, height / 2.0f, depth / 2.0f ) );;

		if ( mass != 0.0 )
		{
			sphere->calculateLocalInertia( mass, inertia );
		}

		btMotionState* motion = new btDefaultMotionState( t );
		btRigidBody::btRigidBodyConstructionInfo info( mass, motion, sphere, inertia );

		boost::shared_ptr<btRigidBody> pbtBody = boost::shared_ptr<btRigidBody>( new btRigidBody( info ) );
		pRigidBody->SetRigidBody( pbtBody );

		if ( elasticity != 0.0 )
		{
			pbtBody->setRestitution( elasticity );
		}
		else if ( mass == 0 )
		{
			pbtBody->setRestitution( 1.0 );
		}

		return pRigidBody;
	}
	else if ( _type == NvPhysX )
	{
		boost::shared_ptr<NvidiaPhysxRigidBody> pRigidBody = boost::shared_ptr<NvidiaPhysxRigidBody>( new NvidiaPhysxRigidBody() );

		physx::PxMaterial* material = PhysxPhysicsEngine::gPhysicsSDK->createMaterial( 0.0, 0.0, elasticity ); //Creating a PhysX material


		PxTransform		boxPos( PxVec3( 0.0f, 0.0f, 0.0f ) );												//Position and orientation(transform) for box actor 
		PxBoxGeometry	boxGeometry( PxVec3( width, height, depth ) );


		physx::PxRigidDynamic* box = physx::PxCreateDynamic( *PhysxPhysicsEngine::gPhysicsSDK, boxPos, boxGeometry, *material, 1.0f ); //Creating rigid static actor	
		box->setMass( mass );

		pRigidBody->SetRigidBody( box );

		return pRigidBody;
	}

	return NULL;
}
