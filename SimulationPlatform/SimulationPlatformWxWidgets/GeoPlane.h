#pragma once

#include "Common.h"
#include "GameObject.h"
#include <assimp/scene.h> 

class GeoPlane : public GameObject
{

public:

	GeoPlane();
	virtual ~GeoPlane();

	void Create();
	int GetNumberOfInicies();

	virtual void ConnectRenderer( Renderer* pRenderer );
	virtual void Draw( glm::mat4 mCameraMatrix );

protected: 

	std::vector<glm::vec3> _geometry;
	std::vector<short> _indicies;

	GLuint _vertexBufferObject;
	GLuint _indexBufferObject;
	GLuint _vao;
};

