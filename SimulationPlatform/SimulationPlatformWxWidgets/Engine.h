#pragma once
#include "Common.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "OrbitCamera.h"
#include "AssimpGameObject.h"
#include "FlyCam.h"
#include "Scene.h"
#include "MyFlyCam.h"
#include "BuilderFrameEx.h"
#include "RuntimeFrameEx.h"
#include "PhysicsEngine.h"

#include <boost/thread.hpp>

class BuilderFrameEx;
class RuntimeFrameEx;

typedef void( BuilderFrameEx::*SelectionListenerCallback )( GameObject* pSelectedObject );
typedef void( RuntimeFrameEx::*FrameListenerCallback )( double fps );

class Engine
{

public:

	Engine();
	virtual ~Engine();

	void Init( int renderType, int physicsType );
	void Display();
	void Reshape( int w, int h );
	void Keyboard( unsigned char key, int x, int y );
	void MouseMotion( int x, int y );
	void MouseButton( int button, int state, int x, int y );
	void MouseWheel( int wheel, int direction, int x, int y );
	void UpdatePhysics();
	void LoadObjectFromFile( std::string filePath, std::string name );
	void AddPointLight( std::string name );
	void SetListener( BuilderFrameEx* pFrame, SelectionListenerCallback cbFunc );
	void SetListener( RuntimeFrameEx* pFrame, FrameListenerCallback cbFunc, FrameListenerCallback physicsListener );

	void SaveSceneToFile( std::string fileLocation, std::string renderer, std::string physicsEngine );
	void SelectObjectWithName( std::string name );
	void RemoveSelectedObjectFromScene();

	LoadingOutputData LoadFromFileBuilderMode( std::string filePath );
	void LoadFromFileRuntimeMode( std::string filePath );
	void WriteStats();

	std::string _sceneName;

	Scene* GetScene();

protected:

	SelectionListenerCallback SelectionListener;
	FrameListenerCallback FrameListener;
	FrameListenerCallback PhysicsListener;

	BuilderFrameEx* pListenFrame;
	RuntimeFrameEx* pRunListenFrame;

	float _orientation = 0.0f;

	OrbitCamera* g_OrbitCam;
	FlyCam* g_FlyCam;
	MyFlyCam* _pMyFlyCam;

	AssimpGameObject* go_pGround = NULL;
	AssimpGameObject* go_box = NULL;
	AssimpGameObject* go_Sphere1 = NULL;

	DirectionalLight* g_pDlight;
	PointLight* g_pPlight;
	float g_lightAngle = 0;

	const float g_fHalfLightDistance = 50.0f;
	const float g_fLightAttenuation = 1.0f / ( g_fHalfLightDistance * g_fHalfLightDistance );

	bool g_bRemotePick;
	glm::vec2 v2Last;

	Scene* _pScene;

	double _fps = 0;
	int _frameCounter;

	boost::posix_time::ptime _lastFPSTime;

	void CalculateFPS();

	boost::shared_ptr<PhysicsEngine> _pPhysicsEngine;

	boost::posix_time::ptime _startTime;

	bool _statsWritain;
	bool _recordStats;

	std::vector<int> _fpsList;
	std::vector<float> _latencyList;

};