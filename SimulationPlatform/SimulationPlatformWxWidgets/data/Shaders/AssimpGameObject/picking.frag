#version 330

in vec3 vertexNormal;
in vec3 cameraSpacePosition;
in vec2 UV;

uniform sampler2D textureSampler;

out vec4 outputColor;

layout(std140) uniform;

uniform Material
{
	vec4 diffuseColor;
	vec4 specularColor;
	float specularShininess;
} Mtl;

struct PerLight
{
	vec4 cameraSpaceLightPos;
	vec4 lightIntensity;
};

const int numberOfLights = 4;

uniform Light
{
	vec4 ambientIntensity;
	float lightAttenuation;
	float maxIntensity;
	float gamma;
	PerLight lights[numberOfLights];
} Lgt;


void main()
{	
	outputColor = Mtl.diffuseColor;
}