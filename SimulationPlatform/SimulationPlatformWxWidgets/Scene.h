#pragma once

#include "Common.h"
#include "GameObject.h"
#include "Camera.h"
#include "LightManager.h"
#include <map>
#include <bullet/btBulletDynamicsCommon.h>
#include "PhysicsEngine.h"

class Scene
{

public:

	Scene();
	virtual ~Scene();

	void AddCamera( std::string strID, Camera* pCamera );
	void AddGameObject( GameObject* pGameObject );
	void SetActiveCamera( std::string strID );
	void DrawGameObject( GameObject* pGameObject );

	void LoadFromFIle( std::string filePath, boost::shared_ptr<PhysicsEngine> pPhysicsEngine );

	Camera* GetActiveCamera();

	LightManager* GetLigtManager();

	void Render();
	void RenderPicking();

	void SetModelToView( int iNum );
	void SetRenderMode( int renderMode );
	void ResetSceneCameraToClipMatrix( int width, int height );
	void SelectObject( int x, int y );
	void SetScreenDimentions( int x, int y );
	void AddGizmo( GameObject* pGameObject );
	void SelectObjectWithName( std::string name );
	void RemoveSelectedObjectFromScene();
	void SelectFirstObject();
	void Reset();
	void RemoveRigidBodies();
	void CreateGizmoesForLights();
	int GetNumberOfObjects();
	std::vector<std::string> GetSceneObjectNameList();
	int GetTotalNumberOfVertciesInScene();

	GameObject* GetSelectedObject();
	std::vector<GameObject*>* GetGameObjects() const;
	std::vector<boost::shared_ptr<PointLight> >* GetPointLights() const;
	std::vector<DirectionalLight*>* GetDirectionLights() const;

protected:

	Camera* _pActiveCamera;

	LightManager* _pLightManager;

	std::map<std::string, Camera*> _mCameraList;
	std::vector<GameObject*>* _pScene;
	std::vector<GameObject*>* _pGizmoes;

	float _fDeltaTime;
	int _iModelToView;
	int _renderingMode;

	GameObject* _pSelectedObject;

	int _screenWidth;
	int _screenHeight;
};

