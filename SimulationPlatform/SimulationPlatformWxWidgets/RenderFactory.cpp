#include "Common.h"
#include "RenderFactory.h"
#include "OpenGLRenderer.h"
#include "DirectXRenderer.h"

int RenderFactory::_renderType = 0;

RenderFactory::RenderFactory()
{
}

RenderFactory::~RenderFactory()
{
}

void RenderFactory::SetRenderType( int renderType )
{
	_renderType = renderType;
}

Renderer* RenderFactory::CreateRenderer()
{
	if ( _renderType == OpenGL )
	{
		return new OpenGLRenderer();
	}
#ifdef _WIN32
	else if ( _renderType == DirectX )
	{
		return new DirectXRenderer();
	}
#endif

	return NULL;
}
