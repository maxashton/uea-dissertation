#ifdef _WIN32
#include "DirectXRenderer.h"
#include "Structs.h"
#include "Direct3DTexture.h"

DirectXRenderer::DirectXRenderer()
{
	_pShader = new Direct3DShader();
}

DirectXRenderer::~DirectXRenderer()
{
	if ( _pShader ) delete _pShader;
	if ( _pVBuffer ) _pVBuffer->Release();
	if ( _pIBuffer ) _pIBuffer->Release();
}

void DirectXRenderer::UpdateLightingBuffers( LightBlock* lightData )
{
	Direct3DShader::UpdateLightingBuffers( lightData );
}

void DirectXRenderer::Clear()
{
	Direct3DShader::DeviceContext->ClearRenderTargetView( Direct3DShader::BackBuffer, D3DXCOLOR( 0.5f, 0.74f, 1.0f, 0.9f ) );
	// clear the depth buffer
	Direct3DShader::DeviceContext->ClearDepthStencilView( Direct3DShader::ZBuffer, D3D11_CLEAR_DEPTH, 1.0f, 0 );
}

void DirectXRenderer::DrawIcoSphere( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
	Direct3DShader::SetRasterState( D3D11_FILL_WIREFRAME );
	_pShader->SetDeviceContextWithShader();

	ObjectBuffer cBuffer;

	cBuffer.diffuseColor = glm::vec4( .0f, 0.0f, 0.0f, 1.0f );
	cBuffer.specularColor = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );

	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );

	if ( modelMatrixOverride )
	{
		msModelSpaceMatrix.ApplyMatrix( modelMatrix );
		msModelSpaceMatrix.Scale( pTransform->pScale->x + 0.01f );
	}
	else
	{
		msModelSpaceMatrix.Translate( *pTransform->pPosition );
		msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}

	glm::mat4 normMatrix( msModelSpaceMatrix.Top() );
	normMatrix = glm::transpose( glm::inverse( normMatrix ) );

	// load the matrices into the constant buffer
	cBuffer.normalModelToCameraMatrix = normMatrix;
	cBuffer.modelToCameraMatrix = msModelSpaceMatrix.Top();

	// select which vertex buffer to display
	UINT stride = sizeof( VERTEX );
	UINT offset = 0;
	Direct3DShader::DeviceContext->IASetVertexBuffers( 0, 1, &_pVBuffer, &stride, &offset );
	Direct3DShader::DeviceContext->IASetIndexBuffer( _pIBuffer, DXGI_FORMAT_R32_UINT, 0 );

	// select which primtive type we are using
	Direct3DShader::DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	// draw the Hypercraft
	Direct3DShader::DeviceContext->UpdateSubresource( Direct3DShader::ConstBuffers [0], 0, 0, &cBuffer, 0, 0 );
	Direct3DShader::DeviceContext->DrawIndexed( numOfIndicies, 0, 0 );

	Direct3DShader::SetRasterState( D3D11_FILL_SOLID );
}

void DirectXRenderer::DrawGeoPlane( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
	Direct3DShader::SetRasterState( D3D11_FILL_WIREFRAME );

	_pShader->SetDeviceContextWithShader();

	ObjectBuffer cBuffer;

	cBuffer.diffuseColor = glm::vec4( .0f, 0.0f, 0.0f, 1.0f );
	cBuffer.specularColor = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );

	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );

	if ( modelMatrixOverride )
	{
		msModelSpaceMatrix.ApplyMatrix( modelMatrix );
		msModelSpaceMatrix.RotateX( -90.0f );
		msModelSpaceMatrix.Scale( pTransform->pScale->x + 0.01f, pTransform->pScale->y + 0.01f, pTransform->pScale->z + 0.01f );

	}
	else
	{
		msModelSpaceMatrix.Translate( *pTransform->pPosition );
		msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}

	glm::mat4 normMatrix( msModelSpaceMatrix.Top() );
	normMatrix = glm::transpose( glm::inverse( normMatrix ) );

	// load the matrices into the constant buffer
	cBuffer.normalModelToCameraMatrix = normMatrix;
	cBuffer.modelToCameraMatrix = msModelSpaceMatrix.Top();

	// select which vertex buffer to display
	UINT stride = sizeof( VERTEX );
	UINT offset = 0;
	Direct3DShader::DeviceContext->IASetVertexBuffers( 0, 1, &_pVBuffer, &stride, &offset );
	Direct3DShader::DeviceContext->IASetIndexBuffer( _pIBuffer, DXGI_FORMAT_R32_UINT, 0 );

	// select which primtive type we are using
	Direct3DShader::DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	// draw the Hypercraft
	Direct3DShader::DeviceContext->UpdateSubresource( Direct3DShader::ConstBuffers[0], 0, 0, &cBuffer, 0, 0 );
	Direct3DShader::DeviceContext->DrawIndexed( numOfIndicies, 0, 0 );

	Direct3DShader::SetRasterState( D3D11_FILL_SOLID );
}

void DirectXRenderer::DrawAssimpGameObject( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, Material* pMatrial, 
											int iNumMeshes, std::vector<int> materialIndices, std::vector<Texture*>* textures, std::vector<int> meshStartIndices, 
											std::vector<int> meshSizes )
{
	_pShader->SetDeviceContextWithShader();

	ObjectBuffer cBuffer;

	cBuffer.diffuseColor = glm::vec4( .0f, 0.0f, 0.0f, 1.0f );
	cBuffer.specularColor = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );

	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );

	if ( modelMatrixOverride )
	{
		msModelSpaceMatrix.ApplyMatrix( modelMatrix );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}
	else
	{
		msModelSpaceMatrix.Translate( *pTransform->pPosition );
		msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}

	glm::mat4 normMatrix( msModelSpaceMatrix.Top() );
	normMatrix = glm::transpose( glm::inverse( normMatrix ) );

	// load the matrices into the constant buffer
	cBuffer.normalModelToCameraMatrix = normMatrix;
	cBuffer.modelToCameraMatrix = msModelSpaceMatrix.Top();

	// select which vertex buffer to display
	UINT stride = sizeof( ASSIMP_VERTEX );
	UINT offset = 0;
	Direct3DShader::DeviceContext->IASetVertexBuffers( 0, 1, &_pVBuffer, &stride, &offset );

	// select which primtive type we are using
	Direct3DShader::DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	// draw the Hypercraft
	Direct3DShader::DeviceContext->UpdateSubresource( Direct3DShader::ConstBuffers [0], 0, 0, &cBuffer, 0, 0 );

	for ( int i = 0; i < iNumMeshes; i++ )
	{
		int iMatIndex = materialIndices [i];
		( *textures ) [iMatIndex]->BindTexture();
		Direct3DShader::DeviceContext->Draw( meshSizes [i], meshStartIndices [i] );
	}
}

void DirectXRenderer::DrawAssimpGameObjectPicking( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int iNumMeshes, 
												   std::vector<int> materialIndices, std::vector<int> meshStartIndices, std::vector<int> meshSizes, glm::vec4 color )
{
	_pShader->SetDeviceContextWithShader();

	ObjectBuffer cBuffer;

	cBuffer.diffuseColor = color;
	cBuffer.specularColor = glm::vec4( 1.0f, 1.0f, 1.0f, 1.0f );

	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );

	if ( modelMatrixOverride )
	{
		msModelSpaceMatrix.ApplyMatrix( modelMatrix );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}
	else
	{
		msModelSpaceMatrix.Translate( *pTransform->pPosition );
		msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
		msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
	}

	glm::mat4 normMatrix( msModelSpaceMatrix.Top() );
	normMatrix = glm::transpose( glm::inverse( normMatrix ) );

	// load the matrices into the constant buffer
	cBuffer.normalModelToCameraMatrix = normMatrix;
	cBuffer.modelToCameraMatrix = msModelSpaceMatrix.Top();

	// select which vertex buffer to display
	UINT stride = sizeof( ASSIMP_VERTEX );
	UINT offset = 0;
	Direct3DShader::DeviceContext->IASetVertexBuffers( 0, 1, &_pVBuffer, &stride, &offset );

	// select which primtive type we are using
	Direct3DShader::DeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

	// draw the Hypercraft
	Direct3DShader::DeviceContext->UpdateSubresource( Direct3DShader::ConstBuffers [0], 0, 0, &cBuffer, 0, 0 );

	for ( int i = 0; i < iNumMeshes; i++ )
	{
		int iMatIndex = materialIndices [i];
		Direct3DShader::DeviceContext->Draw( meshSizes [i], meshStartIndices [i] );
	}
}

void DirectXRenderer::BuildAssimpGameObjectDiffuseShader()
{
	_pShader->CreateVertexAndPixelShader( "data//Shaders//Direct3D//AssimpGameObject.shader" );
}

void DirectXRenderer::BuildAssimGameObjectSpecularShader()
{
	_pShader->CreateVertexAndPixelShader( "data//Shaders//Direct3D//AssimpGameObject.shader" );
}

void DirectXRenderer::InitializeAssimpModelVertexBuffer( std::vector<BYTE> modelData )
{
	// create the vertex buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory( &bd, sizeof( bd ) );

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof( BYTE ) * modelData.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	Direct3DShader::Device->CreateBuffer( &bd, NULL, &_pVBuffer );

	// copy the vertices into the buffer
	D3D11_MAPPED_SUBRESOURCE ms;
	Direct3DShader::DeviceContext->Map( _pVBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms );    // map the buffer
	memcpy( ms.pData, &modelData [0], sizeof( BYTE ) * modelData.size() );                 // copy the data
	Direct3DShader::DeviceContext->Unmap( _pVBuffer, NULL );
}

void DirectXRenderer::BuildGeometricSpecularShader()
{
	_pShader->CreateVertexAndPixelShaderGemetryOnly( "data//Shaders//Direct3D//Geometric.shader" );
}

void DirectXRenderer::BuildGeometricDiffuseShader()
{
	_pShader->CreateVertexAndPixelShaderGemetryOnly( "data//Shaders//Direct3D//Geometric.shader" );
}

void DirectXRenderer::InitializeGeometricVertexBuffer( std::vector<short> indicies, std::vector<glm::vec3> geomtery )
{
	std::vector<DWORD> invertedIndicies;

	for ( int i = 0; i < indicies.size(); i++ )
	{
		invertedIndicies.push_back( indicies[i] );
	}

	// create the vertex buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory( &bd, sizeof( bd ) );

	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof( glm::vec3 ) * geomtery.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	Direct3DShader::Device->CreateBuffer( &bd, NULL, &_pVBuffer );

	// copy the vertices into the buffer
	D3D11_MAPPED_SUBRESOURCE ms;
	Direct3DShader::DeviceContext->Map( _pVBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms );    // map the buffer
	memcpy( ms.pData, &geomtery [0], sizeof( glm::vec3 ) * geomtery.size() );                 // copy the data
	Direct3DShader::DeviceContext->Unmap( _pVBuffer, NULL );

	// create the index buffer
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof( DWORD ) * invertedIndicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.MiscFlags = 0;

	Direct3DShader::Device->CreateBuffer( &bd, NULL, &_pIBuffer );

	Direct3DShader::DeviceContext->Map( _pIBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms );    // map the buffer
	memcpy( ms.pData, &invertedIndicies [0], sizeof( DWORD ) * invertedIndicies.size() );                   // copy the data
	Direct3DShader::DeviceContext->Unmap( _pIBuffer, NULL );
}

Texture* DirectXRenderer::LoadTexture( std::string path )
{
	Direct3DTexture* pTexture = new Direct3DTexture();
	pTexture->LoadTexture2D( path );

	return pTexture;
}
#endif