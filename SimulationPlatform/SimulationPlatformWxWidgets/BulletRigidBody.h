#pragma once

#include "Common.h"
#include "RigidBody.h"
#include <bullet/btBulletDynamicsCommon.h>

class BulletRigidBody :	public RigidBody
{

public:

	BulletRigidBody();
	virtual ~BulletRigidBody();

	virtual glm::mat4 GetModelMatrix();

	virtual void SetModelMatrix( boost::shared_ptr<glm::vec3> position );

	void SetRigidBody( boost::shared_ptr<btRigidBody> pRigidBody );

	virtual boost::shared_ptr<btRigidBody> GetRigidBody();

	virtual void SetMass( float val );
	virtual void SetElasticity( float val );

protected:

	boost::shared_ptr<btRigidBody> _pRigidBody;
};

