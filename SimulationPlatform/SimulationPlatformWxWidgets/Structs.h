#pragma once

#include "Common.h"

struct LightInfo
{
	glm::vec4 cameraSpaceLightPos;
	glm::vec4 lightIntensity;
};

struct LightBlock
{
	glm::vec4 ambientIntensity;
	float lightAttenuation;
	float maxIntensity;
	float gamma;
	float padding;
	LightInfo lights [20];
};

enum IdentifyType
{
	IDComponent,
	IDCollider
};

struct LoadingOutputData
{
	std::vector<std::string> objectNameList;
	glm::vec4 ambientLight;
	std::string renderer;
	std::string physicsEngine;
};