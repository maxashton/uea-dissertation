#pragma once

#include "Common.h"
#include "Light.h"

class PointLight : 	public Light
{

public:

	PointLight();
	virtual ~PointLight();

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
};

