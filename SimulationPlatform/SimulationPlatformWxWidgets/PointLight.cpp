#include "PointLight.h"

PointLight::PointLight()
{
}


PointLight::~PointLight()
{
}

void PointLight::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType /*= true */ )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "Type", "PointLight" );
	}

	Light::SerializeToXLM( pInTree, false );
}
