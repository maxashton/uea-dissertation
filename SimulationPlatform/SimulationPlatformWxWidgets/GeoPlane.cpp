#include "GeoPlane.h"

GeoPlane::GeoPlane()
{
}

GeoPlane::~GeoPlane()
{
}

void GeoPlane::Create()
{
	_geometry.push_back( glm::vec3( -1.0f, -1.0f, 0.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, 0.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, 0.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, 0.0f ) );

	_indicies.push_back( 2 );
	_indicies.push_back( 1 );
	_indicies.push_back( 0 );

	_indicies.push_back( 1 );
	_indicies.push_back( 2 );
	_indicies.push_back( 3 );
}

int GeoPlane::GetNumberOfInicies()
{
	return ( int ) _indicies.size();
}

void GeoPlane::ConnectRenderer( Renderer* pRenderer )
{
	GameObject::ConnectRenderer( pRenderer );

	pRenderer->BuildGeometricDiffuseShader();
	pRenderer->InitializeGeometricVertexBuffer( _indicies, _geometry );
}

void GeoPlane::Draw( glm::mat4 mCameraMatrix )
{
	_pRenderer->DrawGeoPlane( mCameraMatrix, _modelMatrixOverride, _modelMatrix, _pTransform, GetNumberOfInicies() );
}
