#include "Renderer.h"
#include <assimp/postprocess.h>

Renderer::Renderer()
{
}

Renderer::~Renderer()
{
}

void Renderer::DrawIcoSphere( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
}

void Renderer::DrawGeoPlane( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
}

void Renderer::DrawAssimpGameObject( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, Material* pMatrial,
										int iNumMeshes, std::vector<int> materialIndices, std::vector<Texture*>* textures, std::vector<int> meshStartIndices, 
										std::vector<int> meshSizes )
{

}

void Renderer::DrawAssimpGameObjectPicking( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int iNumMeshes, std::vector<int> materialIndices, std::vector<int> meshStartIndices, std::vector<int> meshSizes, glm::vec4 color )
{

}

void Renderer::BuildAssimpGameObjectDiffuseShader()
{
}

void Renderer::BuildAssimGameObjectSpecularShader()
{
}

void Renderer::InitializeAssimpModelVertexBuffer( std::vector<BYTE> modelData )
{

}

void Renderer::BuildGeometricSpecularShader()
{
}

void Renderer::BuildGeometricDiffuseShader()
{
}

void Renderer::InitializeGeometricVertexBuffer( std::vector<short> indicies, std::vector<glm::vec3> geomtery )
{
}

Texture* Renderer::LoadTexture( std::string path )
{
	return NULL;
}
