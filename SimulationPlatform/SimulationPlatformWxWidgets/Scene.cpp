﻿#include "Scene.h"
#include "OpenGLRenderer.h"
#include "DirectXRenderer.h"
#include "OpenGLShader.h"
#include "Direct3DShader.h"
#include "SceneIO.h"
#include "Framework.h"
#include "LightGizmo.h"
#include "Collider.h"
#include "BulletBoxCollider.h"
#include "RenderFactory.h"

Scene::Scene()
{
	_pLightManager = new LightManager();
	_iModelToView = 0;
	_pScene = new std::vector< GameObject* >();
	_pGizmoes = new std::vector<GameObject*>();
}

Scene::~Scene()
{
	for ( std::map<std::string, Camera*>::iterator i = _mCameraList.begin(); i != _mCameraList.end(); i++ )
	{
		if ( i->second ) delete i->second;
	}

	_mCameraList.clear();

	if ( _pScene )
	{
		for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
		{
			if ( *i ) delete ( *i );
		}

		_pScene->clear();
		delete _pScene;
	}

	if ( _pGizmoes )
	{
		for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
		{
			if ( *i ) delete ( *i );
		}

		_pGizmoes->clear();
		delete _pGizmoes;
	}

	if ( _pLightManager ) delete _pLightManager;
}

void Scene::AddCamera( std::string strID, Camera* pCamera )
{
	_mCameraList.insert( std::pair<std::string, Camera*>( strID, pCamera ) );

	if ( _mCameraList.size() == 1 )
	{ 
		_pActiveCamera = pCamera;
	}
}

void Scene::SetActiveCamera( std::string strID )
{
	Camera* pCam = _mCameraList.find( strID )->second;

	if ( pCam )
	{
		_pActiveCamera = pCam;
	}
}

LightManager* Scene::GetLigtManager()
{
	return _pLightManager;
}

void Scene::AddGameObject( GameObject* pGameObject )
{
	glm::vec4 randomColor;

	randomColor = glm::vec4( 0.01 * ( _pScene->size() + _pGizmoes->size() ), 1.0f, 1.0f, 1.0 );

	pGameObject->SetPickingColor( randomColor );
	
	_pScene->push_back( pGameObject );
}

void Scene::DrawGameObject( GameObject* pGameObject )
{
	LightBlock lightData = _pLightManager->GetLightInfo( _pActiveCamera->GetViewMatrix() );

	if ( _renderingMode == OpenGL )
	{
		OpenGLRenderer::UpdateLightingBuffers( &lightData );
	}
	else if ( _renderingMode == DirectX )
	{

	}

	pGameObject->Draw( _pActiveCamera->GetViewMatrix() );

}

void Scene::LoadFromFIle( std::string filePath, boost::shared_ptr<PhysicsEngine> pPhysicsEngine )
{
	SceneIO::Read( filePath, _pLightManager, _pScene, pPhysicsEngine );
}

void Scene::Render()
{
	LightBlock lightData = _pLightManager->GetLightInfo( _pActiveCamera->GetViewMatrix() );

	if ( _renderingMode == OpenGL )
	{
		OpenGLRenderer::Clear();
		OpenGLRenderer::UpdateLightingBuffers( &lightData );
	}
	else if ( _renderingMode == DirectX )
	{
		DirectXRenderer::Clear();
		DirectXRenderer::UpdateLightingBuffers( &lightData );
	}

	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		( *i )->Update();
		( *i )->Draw( _pActiveCamera->GetViewMatrix() );
	}

	for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
	{
		( *i )->Draw( _pActiveCamera->GetViewMatrix() );
	}
}

void Scene::RenderPicking()
{
	LightBlock lightData = _pLightManager->GetLightInfo( _pActiveCamera->GetViewMatrix() );

	if ( _renderingMode == OpenGL )
	{
		OpenGLRenderer::Clear();
		OpenGLRenderer::UpdateLightingBuffers( &lightData );
	}
	else if ( _renderingMode == DirectX )
	{
		DirectXRenderer::Clear();
		DirectXRenderer::UpdateLightingBuffers( &lightData );
	}

	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		( *i )->DrawPicking( _pActiveCamera->GetViewMatrix() );
	}

	for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
	{
		( *i )->DrawPicking( _pActiveCamera->GetViewMatrix() );
	}

}

Camera* Scene::GetActiveCamera()
{
	return _pActiveCamera;
}

void Scene::SetModelToView( int iNum )
{
	_iModelToView = iNum;
}

void Scene::SetRenderMode( int renderMode )
{
	_renderingMode = renderMode;

	if ( _renderingMode == OpenGL )
	{
		//initialize glew
		GLenum err = glewInit();

		glEnable( GL_DEPTH_TEST );
		glDepthMask( GL_TRUE );
		glDepthFunc( GL_LEQUAL );
		glDepthRange( 0.0f, 1.0f );

		glEnable( GL_CULL_FACE );
		glCullFace( GL_BACK );

		OpenGLShader::SetupStaticBuffers();
	}
	else
	{
#ifndef _WIN32
		Direct3DShader::SetupConstantBuffers();
#endif
	}
}

void Scene::ResetSceneCameraToClipMatrix( int width, int height )
{
	if ( _renderingMode == OpenGL )
	{
		OpenGLShader::SetCameraToClipMatrix( width, height );
	}
	else if ( _renderingMode == DirectX )
	{
#ifndef _WIN32
		Direct3DShader::SetCameraToClipMatrix( width, height );
#endif
	}
}

void Scene::SelectObject( int x, int y )
{
	glm::vec4 color;

	if ( _renderingMode == OpenGL )
	{
		RenderPicking();

		GLubyte rgb [4];
		rgb [0] = 0;
		rgb [1] = 0;
		rgb [2] = 0;
		rgb [3] = 0;

		glReadPixels( x, _screenHeight - y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, rgb );

		color = glm::vec4( std::roundf( ( rgb [0] / 255.0f ) * 100 ) / 100, std::roundf( ( rgb [1] / 255.0f ) * 100 ) / 100, std::roundf( ( rgb [2] / 255.0f ) * 100 ) / 100, 1.0f );
	}
	else if ( _renderingMode == DirectX )
	{
		//Get backfuller texture
		//ID3D11Texture2D* pBackBufferTexture = NULL;
		//Direct3DShader::SwapChain->GetBuffer( 0, __uuidof( pBackBufferTexture ), reinterpret_cast<void**>( &pBackBufferTexture ) );

		////create modifiable texture from back buffers description
		//D3D11_TEXTURE2D_DESC* pBufferDesc = NULL;
		//pBackBufferTexture->GetDesc( pBufferDesc );
		//pBufferDesc->BindFlags = 0;

		////copy back buffer texture to our modifiable texture
		//ID3D11Texture2D* pRWTexture = NULL;
		//Direct3DShader::Device->CreateTexture2D( pBufferDesc, NULL, &pRWTexture );
		//Direct3DShader::DeviceContext->CopyResource( pBackBufferTexture, pRWTexture );

		////map texture
		//D3D11_MAPPED_SUBRESOURCE  mapResource;
		//Direct3DShader::DeviceContext->Map( pRWTexture, 0, D3D11_MAP_READ, NULL, &mapResource );

		//struct Color { float r, g, b, a; };

		//Color* obj;
		//obj = new Color [( mapResource.RowPitch / sizeof( Color ) )*_screenHeight];
		//memcpy( obj, mapResource.pData, mapResource.RowPitch*_screenHeight );

		//Color pix = obj [( y * _screenWidth ) + ( 4 * y ) + x];

		//Direct3DShader::DeviceContext->Unmap( pRWTexture, 0 );
		//pBackBufferTexture->Release();
		//pRWTexture->Release();

		//color = glm::vec4( pix.a, pix.b, pix.g, 1.0f );

		////free created
		//delete pBufferDesc;
		//delete [] obj;
	}

	//Game objects
	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		glm::vec4 oColor = ( *i )->GetPickingColor();

		if (  oColor == color )
		{
			if ( _pSelectedObject != NULL )
			{
				_pSelectedObject->SetDrawComponenets( false );
			}

			_pSelectedObject = ( *i );
			_pSelectedObject->SetDrawComponenets( true );
		}
	}

	//gizmo's
	for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
	{
		glm::vec4 oColor = ( *i )->GetPickingColor();

		if ( oColor == color )
		{
			if ( _pSelectedObject != NULL )
			{
				_pSelectedObject->SetDrawComponenets( false );
			}

			_pSelectedObject = ( *i );
			_pSelectedObject->SetDrawComponenets( true );
		}
	}
}

void Scene::SetScreenDimentions( int x, int y )
{
	_screenWidth = x;
	_screenHeight = y;
}

void Scene::AddGizmo( GameObject* pGameObject )
{
	glm::vec4 randomColor;

	randomColor = glm::vec4( 0.01 * ( _pScene->size() + _pGizmoes->size() ), 1.0f, 1.0f, 1.0 );
	pGameObject->SetPickingColor( randomColor );

	_pGizmoes->push_back( pGameObject );
}

void Scene::SelectObjectWithName( std::string name )
{
	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		std::string objName = ( *i )->GetName();

		if ( objName == name )
		{
			if ( _pSelectedObject != NULL )
			{
				_pSelectedObject->SetDrawComponenets( false );
			}

			_pSelectedObject = ( *i );
			_pSelectedObject->SetDrawComponenets( true );
		}
	}

	//gizmo's
	for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
	{
		std::string objName = ( *i )->GetName();

		if ( objName == name )
		{
			if ( _pSelectedObject != NULL )
			{
				_pSelectedObject->SetDrawComponenets( false );
			}

			_pSelectedObject = ( *i );
			_pSelectedObject->SetDrawComponenets( true );
		}
	}
}

void Scene::RemoveSelectedObjectFromScene()
{
	std::vector<GameObject*>::iterator i = std::find( _pScene->begin(), _pScene->end(), _pSelectedObject );

	if ( i != _pScene->end() )
	{
		_pScene->erase( i );
	}
	else
	{
		//must be a gizmo
		std::vector<GameObject*>::iterator ii = std::find( _pGizmoes->begin(), _pGizmoes->end(), _pSelectedObject );

		if ( ii != _pGizmoes->end() )
		{
			LightGizmo* gizmo = dynamic_cast< LightGizmo* >( *ii );
			_pLightManager->RemoveLight( gizmo->GetLight() );

			_pGizmoes->erase( ii );
		}
	}
}

void Scene::SelectFirstObject()
{
	if ( _pScene->size() > 0 )
	{
		_pSelectedObject = _pScene->at( 0 );
	}
}

void Scene::Reset()
{
	while ( _pScene->size() > 0 )
	{
		_pSelectedObject = ( _pScene->at( 0 ) );
		RemoveSelectedObjectFromScene();
	}

	//gizmo's
	while ( _pGizmoes->size() > 0 )
	{
		_pSelectedObject = ( _pGizmoes->at( 0 ) );
		RemoveSelectedObjectFromScene();
	}
}

void Scene::RemoveRigidBodies()
{
	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		std::vector<Component*>* componenets = ( *i )->GetComponenets();

		BOOST_FOREACH( Component* com, *componenets )
		{
			Collider* collider = dynamic_cast< Collider* >( com );

			if ( collider != NULL && collider->GetColliderType() != PlaneType )
			{
				collider->RemoveRigidBody();
				( *i )->SetModelViewMatrixOverride( false );
			}
		}
	}
}

void Scene::CreateGizmoesForLights()
{
	std::vector<boost::shared_ptr<PointLight> >* pPointLights =  _pLightManager->GetPointLights();

	//TODO add name to light
	int lightIter = 0;
	BOOST_FOREACH( boost::shared_ptr<PointLight> pLight, *pPointLights )
	{
		std::stringstream nameStream;
		nameStream << "Point Light ";
		nameStream << lightIter;
		//add Gizmo
		LightGizmo* pNewObject = new LightGizmo( pLight );
		pNewObject->SetName( nameStream.str() );
		pNewObject->ConnectRenderer( RenderFactory::CreateRenderer() );
		pNewObject->LoadModelFromFile( "data\\Models\\Bulb\\bulb.obj" );
		pNewObject->GetMaterial()->SetDiffuseColor( 1.0f, 1.0f, 1.0f, 1.0f );
		pNewObject->GetMaterial()->SetSpecularColor( 1.0f, 1.0f, 1.0f, 1.0 );
		pNewObject->GetMaterial()->SetSpecularShininess( 0.0f );
		pNewObject->GetTransform()->pPosition = pLight->GetLightPossition();
		pNewObject->GetTransform()->pScale = new glm::vec3( 1.0f, 1.0f, 1.0f );
		pNewObject->GetMaterial()->SetDiffuseColor( 0.0f, 0.0f, 0.0f, 1.0f );

		BulletBoxCollider* boxCollider = new BulletBoxCollider();
		boxCollider->Create( 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, true );
		pNewObject->AddComponenet( boxCollider );

		AddGizmo( pNewObject );
		lightIter++;
	}
}

int Scene::GetNumberOfObjects()
{
	return _pScene->size();
}

std::vector<std::string> Scene::GetSceneObjectNameList()
{
	std::vector<std::string> nameList;

	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		nameList.push_back( ( *i )->GetName() );
	}

	for ( std::vector<GameObject*>::iterator i = _pGizmoes->begin(); i != _pGizmoes->end(); i++ )
	{
		nameList.push_back( ( *i )->GetName() );
	}

	return nameList;
}

int Scene::GetTotalNumberOfVertciesInScene()
{
	int total = 0;

	for ( std::vector<GameObject*>::iterator i = _pScene->begin(); i != _pScene->end(); i++ )
	{
		total += ( *i )->GetNumberOfVertesies();
	}

	return total;
}

GameObject* Scene::GetSelectedObject()
{
	return _pSelectedObject;
}

std::vector<GameObject*>* Scene::GetGameObjects() const
{
	return _pScene;
}

std::vector<boost::shared_ptr<PointLight> >* Scene::GetPointLights() const
{
	return _pLightManager->GetPointLights();
}

std::vector<DirectionalLight*>* Scene::GetDirectionLights() const
{
	return _pLightManager->GetDirectionLights();
}
