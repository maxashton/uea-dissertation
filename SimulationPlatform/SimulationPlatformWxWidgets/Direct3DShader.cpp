#ifdef _WIN32
#include "Direct3DShader.h"

ID3D11RasterizerState* Direct3DShader::RasterState = NULL;
IDXGISwapChain* Direct3DShader::SwapChain = NULL;
ID3D11Device* Direct3DShader::Device = NULL;
ID3D11DeviceContext* Direct3DShader::DeviceContext = NULL;
ID3D11RenderTargetView* Direct3DShader::BackBuffer = NULL;
ID3D11DepthStencilView* Direct3DShader::ZBuffer = NULL;

std::vector<ID3D11Buffer*> Direct3DShader::ConstBuffers = std::vector<ID3D11Buffer*>();

Direct3DShader::Direct3DShader()
{
	_cameraToClipMatrix = glm::mat4( 0.0f );
}

Direct3DShader::~Direct3DShader()
{
	if ( _pLayout )
	{
		_pLayout->Release();
		//delete _pLayout;
	}

	if ( _pVShader )
	{
		_pVShader->Release();
		//delete _pVShader;
	}

	if ( _pPShader )
	{
		_pPShader->Release();
		//delete _pPShader;
	}
}

void Direct3DShader::SetCameraToClipMatrix( int w, int h )
{
	D3DXMATRIX matProjection;

	// create a projection matrix
	D3DXMatrixPerspectiveFovRH( &matProjection,
		( FLOAT )D3DXToRadian( 45 ),                    // field of view
		( FLOAT )w / ( FLOAT )h, // aspect ratio
		1.0f,                                       // near view-plane
		300.0f );

	ProjectionBuffer projectionBuffer;
	projectionBuffer.cameraToClipMatrix = matProjection;

	Direct3DShader::DeviceContext->UpdateSubresource( Direct3DShader::ConstBuffers [2], 0, 0, &projectionBuffer, 0, 0 );
}

void Direct3DShader::SetupConstantBuffers()
{
	// create the constant buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory( &bd, sizeof( bd ) );

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = 160;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	ID3D11Buffer* goeBuffer;
	Device->CreateBuffer( &bd, NULL, &goeBuffer );
	ConstBuffers.push_back( goeBuffer );

	D3D11_BUFFER_DESC lightbd;
	ZeroMemory( &lightbd, sizeof( lightbd ) );

	lightbd.Usage = D3D11_USAGE_DEFAULT;
	lightbd.ByteWidth = 160;
	lightbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	ID3D11Buffer* lightBuffer;
	Device->CreateBuffer( &lightbd, NULL, &lightBuffer );
	ConstBuffers.push_back( lightBuffer );

	D3D11_BUFFER_DESC projectionBD;
	ZeroMemory( &projectionBD, sizeof( projectionBD ) );

	projectionBD.Usage = D3D11_USAGE_DEFAULT;
	projectionBD.ByteWidth = 160;
	projectionBD.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	ID3D11Buffer* projectionBuffer;
	Device->CreateBuffer( &projectionBD, NULL, &projectionBuffer );
	ConstBuffers.push_back( projectionBuffer );

	DeviceContext->VSSetConstantBuffers( 0, 3, &ConstBuffers [0] );
	DeviceContext->PSSetConstantBuffers( 0, 2, &ConstBuffers [0] );
}

void Direct3DShader::SetRasterState( D3D11_FILL_MODE fillmode )
{
	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = fillmode;
	rasterizerState.CullMode = D3D11_CULL_BACK;
	rasterizerState.FrontCounterClockwise = true;
	rasterizerState.DepthBias = false;
	rasterizerState.DepthBiasClamp = 0;
	rasterizerState.SlopeScaledDepthBias = 0;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = true;
	rasterizerState.MultisampleEnable = false;
	rasterizerState.AntialiasedLineEnable = false;

	Device->CreateRasterizerState( &rasterizerState, &RasterState );

	DeviceContext->RSSetState( RasterState );
}

void Direct3DShader::Release()
{
	SwapChain->SetFullscreenState( FALSE, NULL );    // switch to windowed mode

	// close and release all existing COM objects
	ZBuffer->Release();

	BOOST_FOREACH( ID3D11Buffer* buffer, ConstBuffers )
	{
		buffer->Release();
	}

	SwapChain->Release();
	BackBuffer->Release();
	Device->Release();
	DeviceContext->Release();
}

void Direct3DShader::UpdateLightingBuffers( LightBlock* pLightBlock )
{
	LightBuffer lightBuffer;

	lightBuffer.ambientIntensity = pLightBlock->ambientIntensity;
	lightBuffer.gamma = pLightBlock->gamma;
	lightBuffer.maxIntensity = pLightBlock->maxIntensity;
	lightBuffer.lightAttenuation = pLightBlock->lightAttenuation;

	for ( int i = 0; i < 3; i++ )
	{
		lightBuffer.lights [i] = pLightBlock->lights [i];
	}

	DeviceContext->UpdateSubresource( ConstBuffers[ 1 ], 0, 0, &lightBuffer, 0, 0 );
}

void Direct3DShader::CreateVertexAndPixelShader( std::string fileLocation )
{
	// compile the shaders
	std::wstring stemp = std::wstring( fileLocation.begin(), fileLocation.end() );

	ID3D10Blob *VS, *PS;
	D3DX11CompileFromFile( stemp.c_str(), 0, 0, "VShader", "vs_4_0", 0, 0, 0, &VS, 0, 0 );
	D3DX11CompileFromFile( stemp.c_str(), 0, 0, "PShader", "ps_4_0", 0, 0, 0, &PS, 0, 0 );

	// create the shader objects
	Direct3DShader::Device->CreateVertexShader( VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &_pVShader );
	Direct3DShader::Device->CreatePixelShader( PS->GetBufferPointer(), PS->GetBufferSize(), NULL, &_pPShader );

	// create the input element object
	D3D11_INPUT_ELEMENT_DESC ied [] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof( glm::vec3 ), D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof( glm::vec3 ) + sizeof( glm::vec2 ), D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	// use the input element descriptions to create the input layout
	Direct3DShader::Device->CreateInputLayout( ied, 3, VS->GetBufferPointer(), VS->GetBufferSize(), &_pLayout );
}

void Direct3DShader::CreateVertexAndPixelShaderGemetryOnly( std::string fileLocation )
{
	// compile the shaders
	std::wstring stemp = std::wstring( fileLocation.begin(), fileLocation.end() );

	ID3D10Blob *VS, *PS;
	D3DX11CompileFromFile( stemp.c_str(), 0, 0, "VShader", "vs_4_0", 0, 0, 0, &VS, 0, 0 );
	D3DX11CompileFromFile( stemp.c_str(), 0, 0, "PShader", "ps_4_0", 0, 0, 0, &PS, 0, 0 );

	// create the shader objects
	Direct3DShader::Device->CreateVertexShader( VS->GetBufferPointer(), VS->GetBufferSize(), NULL, &_pVShader );
	Direct3DShader::Device->CreatePixelShader( PS->GetBufferPointer(), PS->GetBufferSize(), NULL, &_pPShader );

	// create the input element object
	D3D11_INPUT_ELEMENT_DESC ied [] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	// use the input element descriptions to create the input layout
	Direct3DShader::Device->CreateInputLayout( ied, 1, VS->GetBufferPointer(), VS->GetBufferSize(), &_pLayout );
}

//const Direct3DShaderPropoties Direct3DShader::GetShaderPropoties()
//{
//	return Direct3DShaderPropoties();
//}

const glm::mat4 Direct3DShader::GetCameraToClipMatrix()
{
	return glm::mat4();
}

ID3D11InputLayout* Direct3DShader::GetLayout() const
{
	return _pLayout;
}

ID3D11VertexShader* Direct3DShader::GetVertexShader() const
{
	return _pVShader;
}

ID3D11PixelShader* Direct3DShader::GetPixelShader() const
{
	return _pPShader;
}

void Direct3DShader::SetDeviceContextWithShader()
{
	Direct3DShader::DeviceContext->VSSetShader( _pVShader, 0, 0 );
	Direct3DShader::DeviceContext->PSSetShader( _pPShader, 0, 0 );

	Direct3DShader::DeviceContext->IASetInputLayout( _pLayout );
}
#endif