#pragma once

#include "Common.h"

class RigidBody
{

public:

	RigidBody();
	~RigidBody();

	virtual glm::mat4 GetModelMatrix();
	virtual void SetModelMatrix( boost::shared_ptr<glm::vec3> position );

	virtual void SetMass( float val );
	virtual void SetElasticity( float val );

protected: 


};

