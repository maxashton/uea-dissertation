#include "BuilderFrameEx.h"
#include "OpenGLCanvas.h"
#include "D3D11Canvas.h"
#include "Framework.h"
#include "Collider.h"
#include "BulletPlaneCollider.h"
#include "BulletSphereCollider.h"
#include "BulletBoxCollider.h"
#include "LightGizmo.h"
#include <boost/foreach.hpp>

BuilderFrameEx::BuilderFrameEx( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style )
								: BuilderFrame( parent, id, title, pos, size, style )
{
	_listBoxCounter = 0;
	_canvasCreated = false;

	if ( !_canvasCreated )
	{
		_canvasCreated = true;

		int renderType = OpenGL;
		int physxType = 1;

		if ( renderType == OpenGL )
		{
			int args [] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };
			OpenGLCanvas* canvas = new OpenGLCanvas( m_panel1, wxSize( 700, 800), args );
			canvas->Initialize( physxType );
			_pEngine = canvas->GetEngine();
			_pEngine->SetListener( this, &BuilderFrameEx::SelectionChanged );
			_renderPanel = canvas;
		}
		else if ( renderType == DirectX )
		{
			D3D11Canvas* canvas = new D3D11Canvas( m_panel1, wxID_ANY, wxDefaultPosition, wxSize( 700, 800 ), wxSUNKEN_BORDER );
			canvas->Initialize( physxType );
			_pEngine = canvas->GetEngine();
			_pEngine->SetListener( this, &BuilderFrameEx::SelectionChanged );
			_renderPanel = canvas;
		}

		bSizer19->Add( _renderPanel, 1, wxEXPAND | wxALL, 5 );
	}
}


BuilderFrameEx::~BuilderFrameEx()
{
}

void BuilderFrameEx::FormPaint( wxPaintEvent& event )
{
	if ( !_canvasCreated )
	{
		_canvasCreated = true;

		int renderType = OpenGL;
		int physxType = 1;

		if ( renderType == OpenGL )
		{
			int args [] = { WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0 };
			OpenGLCanvas* canvas = new OpenGLCanvas( m_panel1, wxDefaultSize, args );
			canvas->Initialize( physxType );
			_pEngine = canvas->GetEngine();
			_pEngine->SetListener( this, &BuilderFrameEx::SelectionChanged );
			_renderPanel = canvas;
		}
		else if ( renderType == DirectX )
		{
			D3D11Canvas* canvas = new D3D11Canvas( m_panel1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSUNKEN_BORDER );
			canvas->Initialize( physxType );
			_pEngine = canvas->GetEngine();
			_pEngine->SetListener( this, &BuilderFrameEx::SelectionChanged );
			_renderPanel = canvas;
		}

		bSizer19->Add( _renderPanel, 1, wxEXPAND | wxALL, 5 );
	}
}

void BuilderFrameEx::Load_Click( wxCommandEvent& event )
{
	wxFileDialog openFileDialog( this, _( "Scene file" ), "", "", "Scene file (*.xml)|*.xml", wxFD_OPEN | wxFD_FILE_MUST_EXIST );

	// if quit
	if ( openFileDialog.ShowModal() == wxID_CANCEL )
	{
		return;
	}

	//clear current scene
	while ( m_listBox2->GetCount() > 0 )
	{
		m_listBox2->Delete( 0 );
	}

	_pEngine->GetScene()->Reset();

	//load new scene;
	LoadingOutputData data = _pEngine->LoadFromFileBuilderMode( openFileDialog.GetPath().ToStdString() );

	BOOST_FOREACH( std::string name, data.objectNameList )
	{
		wxArrayString stringArray;
		std::stringstream stringStream;
		stringStream << name;

		_listBoxCounter++;

		stringArray.Add( wxString( stringStream.str() ) );
		m_listBox2->Insert( stringArray, 0 );
	}
}

void BuilderFrameEx::Save( wxCommandEvent& event )
{
	wxFileDialog saveFileDialog( this, _( "Save XYZ file" ), "", "", "Scene file (*.xml)|*.xml", wxFD_SAVE | wxFD_OVERWRITE_PROMPT );
	
	if ( saveFileDialog.ShowModal() == wxID_CANCEL )
	{
		return; 
	}

	std::string stlstring = std::string( saveFileDialog.GetPath().mbc_str() );
	std::string renderer = std::string( m_choice2->GetStringSelection().mbc_str() );
	std::string physicsEngine = std::string( m_choice3->GetStringSelection().mbc_str() );

	_pEngine->SaveSceneToFile( stlstring, renderer, physicsEngine );
}

void BuilderFrameEx::Exit( wxCommandEvent& event )
{

}

void BuilderFrameEx::Create_Create_Obj( wxCommandEvent& event )
{
	unsigned int selection = m_listBox1->GetSelection();

	if ( selection != -1 )
	{
		wxString selectionStr = m_listBox1->GetString( selection );

		std::map<wxString, wxString>::iterator it = _filePaths.find( selectionStr );

		if ( it != _filePaths.end() )
		{
			wxArrayString stringArray;
			std::stringstream stringStream;
			stringStream << "Game Object ";
			stringStream << _listBoxCounter;

			_listBoxCounter++;

			stringArray.Add( wxString( stringStream.str() ) );
			m_listBox2->Insert( stringArray, 0 );

			_pEngine->LoadObjectFromFile( std::string( it->second.mb_str() ), stringStream.str() );
		}
	}
	else
	{
		wxMessageBox( wxT( "Please select from list box" ) );
	}
}

void BuilderFrameEx::Create_Add_Click( wxCommandEvent& event )
{
	wxFileName file = m_filePickBox->GetValue();

	if ( file.GetPath() != "" )
	{
		std::map<wxString, wxString>::iterator it = _filePaths.find( file.GetName() );

		if ( it == _filePaths.end() )
		{
			_filePaths.insert( std::pair<wxString, wxString>( file.GetName(), file.GetFullPath() ) );
			m_listBox1->Append( file.GetName() );
		}
		else
		{
			wxMessageBox( wxT( "Object already in list" ) );
		}
	}
	else
	{
		wxMessageBox( wxT( "Please select and object file to add" ) );
	}
}

void BuilderFrameEx::Create_Add_PointLight( wxCommandEvent& event )
{
	wxArrayString stringArray;
	std::stringstream stringStream;
	stringStream << "Point Light ";
	stringStream << _listBoxCounter;

	stringArray.Add( wxString( stringStream.str() ) );
	m_listBox2->Insert( stringArray, 0 );
	_listBoxCounter++;;

	_pEngine->AddPointLight( stringStream.str() );
}

float wxStringToFloat( wxString str )
{
	double value;
	if ( str.ToDouble( &value ) )
	{
		return ( float )value;
	}

	return 0;
}

void BuilderFrameEx::Prop_Pos_X( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->SetPosition( glm::vec3( wxStringToFloat( m_textCtrl1->GetValue() ), wxStringToFloat( m_textCtrl2->GetValue() ),
													wxStringToFloat( m_textCtrl3->GetValue() ) ) );
}

void BuilderFrameEx::Prop_Pos_Y( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->SetPosition( glm::vec3( wxStringToFloat( m_textCtrl1->GetValue() ), wxStringToFloat( m_textCtrl2->GetValue() ),
													wxStringToFloat( m_textCtrl3->GetValue() ) ) );

}

void BuilderFrameEx::Prop_Pos_Z( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->SetPosition( glm::vec3( wxStringToFloat( m_textCtrl1->GetValue() ), wxStringToFloat( m_textCtrl2->GetValue() ),
		wxStringToFloat( m_textCtrl3->GetValue() ) ) );
}

void BuilderFrameEx::Prop_Ori_X( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->pOrientation = new glm::quat( glm::vec3( Framework::DegToRad( wxStringToFloat( m_textCtrl4->GetValue() ) ),
																	Framework::DegToRad( wxStringToFloat( m_textCtrl5->GetValue() ) ),
																	Framework::DegToRad( wxStringToFloat( m_textCtrl6->GetValue() ) ) ) );
}

void BuilderFrameEx::Prop_Ori_Y( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->pOrientation = new glm::quat( glm::vec3( Framework::DegToRad( wxStringToFloat( m_textCtrl4->GetValue() ) ),
		Framework::DegToRad( wxStringToFloat( m_textCtrl5->GetValue() ) ),
		Framework::DegToRad( wxStringToFloat( m_textCtrl6->GetValue() ) ) ) );;
}

void BuilderFrameEx::Prop_Ori_Z( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	_pSelectedObject->GetTransform()->pOrientation = new glm::quat( glm::vec3( Framework::DegToRad( wxStringToFloat( m_textCtrl4->GetValue() ) ),
		Framework::DegToRad( wxStringToFloat( m_textCtrl5->GetValue() ) ),
		Framework::DegToRad( wxStringToFloat( m_textCtrl6->GetValue() ) ) ) );
}

void BuilderFrameEx::Prop_Tint_Color( wxColourPickerEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	wxColor color = m_colourPicker1->GetColour();
	float r = ( ( float ) color.Red() / 255 );
	float g = ( ( float )color.Green() / 255 );
	float b = ( ( float )color.Blue() / 255 );
	float a = ( ( float )color.Alpha() / 255 );

	_pSelectedObject->GetMaterial()->SetDiffuseColor( r, g, b, a );
}

void BuilderFrameEx::Prop_Collider_Type( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	//remove old collider component
	Component* pObjectColider = _pSelectedObject->FindCollider();
	_pSelectedObject->RemoveComponenet( pObjectColider );
	delete pObjectColider;


	int choice = m_choice1->GetSelection();

	//add new collider component
	if ( choice == 0 )
	{
		BulletBoxCollider* boxCollider = new BulletBoxCollider();
		boxCollider->Create( 1.0f, 1.0f, 2.0f, 2.0f, 2.0f, true );
		_pSelectedObject->AddComponenet( boxCollider );
	}
	else if ( choice == 1 )
	{
		BulletSphereCollider* boxCollider = new BulletSphereCollider();
		boxCollider->Create( 1.0f, 1.0f, 1.0f, true );
		_pSelectedObject->AddComponenet( boxCollider );
	}
	else if ( choice == 2 )
	{
		BulletPlaneCollider* boxCollider = new BulletPlaneCollider();
		boxCollider->Create();
		_pSelectedObject->AddComponenet( boxCollider );
	}

	UpdateUIColliderInfo();
}

void BuilderFrameEx::Prop_Col_Width( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	UpdateGameObjectCollider();
}

void BuilderFrameEx::Prop_Col_Height( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	UpdateGameObjectCollider();
}

void BuilderFrameEx::Prop_Col_Depth( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	UpdateGameObjectCollider();
}

void BuilderFrameEx::Prop_Col_Radius( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	UpdateGameObjectCollider();
}

void BuilderFrameEx::Ambient_Color_Change( wxColourPickerEvent& event )
{
	wxColor color = m_colourPicker3->GetColour();
	float r = ( ( float )color.Red() / 255 );
	float g = ( ( float )color.Green() / 255 );
	float b = ( ( float )color.Blue() / 255 );
	float a = ( ( float )color.Alpha() / 255 );

	_pEngine->GetScene()->GetLigtManager()->SetAmbiantIntensity( glm::vec4( r, g, b, a ) );
}

void BuilderFrameEx::Ambient_Color_Change( wxCommandEvent& event )
{
	LightGizmo* pGizmo = dynamic_cast< LightGizmo* >( _pSelectedObject );

	if ( pGizmo != NULL )
	{
		boost::shared_ptr<Light> pLight = pGizmo->GetLight();

		glm::vec4 goColor = pLight->GetLightIntensity();
		wxColor color( goColor.r * 255, goColor.g * 255, goColor.b * 255, goColor.a * 255 );
		m_colourPicker11->SetColour( color );
	}
}

void BuilderFrameEx::Prop_Light_Color( wxColourPickerEvent& event )
{
	LightGizmo* pGizmo = dynamic_cast< LightGizmo* >( _pSelectedObject );

	if ( pGizmo != NULL )
	{
		boost::shared_ptr<Light> pLight = pGizmo->GetLight();

		wxColor color = m_colourPicker11->GetColour();
		float r = ( ( float )color.Red() / 255 );
		float g = ( ( float )color.Green() / 255 );
		float b = ( ( float )color.Blue() / 255 );
		float a = ( ( float )color.Alpha() / 255 );

		pLight->SetLightIntensity( glm::vec4( r, g, b, a ) );
		pGizmo->GetMaterial()->SetDiffuseColor( r, g, b, a );
	}
}

void BuilderFrameEx::Scene_List_Change( wxCommandEvent& event )
{
	unsigned int selection = m_listBox2->GetSelection();

	if ( selection != -1 )
	{
		wxString selectionStr = m_listBox2->GetString( selection );
		_pEngine->SelectObjectWithName( std::string( selectionStr.mb_str() ) );
	}
	else
	{
		wxMessageBox( wxT( "Please select from list box" ) );
	}
}

void BuilderFrameEx::Scene_Delete_Click( wxCommandEvent& event )
{
	int selectionIndex = m_listBox2->GetSelection();
	m_listBox2->Delete( selectionIndex );
	_pEngine->RemoveSelectedObjectFromScene();
}

void BuilderFrameEx::Mass_Changed( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	float mass = wxStringToFloat( m_textCtrl12->GetValue() );

	Component* collider = _pSelectedObject->FindCollider();

	Collider* pCasted = dynamic_cast< Collider* >( collider );

	pCasted->SetMass( mass );
}

void BuilderFrameEx::Elasticity_Changed( wxCommandEvent& event )
{
	if ( _settingUI )
	{
		return;
	}

	float elas = wxStringToFloat( m_textCtrl13->GetValue() );

	Component* collider = _pSelectedObject->FindCollider();

	Collider* pCasted = dynamic_cast< Collider* >( collider );

	pCasted->SetElasticity( elas );
}

void BuilderFrameEx::MyFilePicker1( wxCommandEvent& event )
{
	wxFileDialog openFileDialog( this, _( "3D Objects" ), "", "", "Scene file (*.obj)|*.obj", wxFD_OPEN | wxFD_FILE_MUST_EXIST );

	if ( openFileDialog.ShowModal() == wxID_CANCEL )
	{
		return;
	}

	m_filePickBox->SetValue( openFileDialog.GetPath() );
}

void BuilderFrameEx::SelectionChanged( GameObject* pSelectedObject )
{
	if ( pSelectedObject != NULL )
	{
		_pSelectedObject = pSelectedObject;
		UpdateUIBasedOnSelection();

		//set scene list box
		int indexOfSelection = m_listBox2->FindString( _pSelectedObject->GetName() );

		m_listBox2->SetSelection( indexOfSelection );
	}
}

void BuilderFrameEx::UpdateUIBasedOnSelection()
{
	_settingUI = true;

	if ( _pSelectedObject != NULL )
	{
		UpdateUITransformInfo();
		UpdateUIColliderInfo();
		UpdateLightInfo();
	}

	_settingUI = false;
}

void BuilderFrameEx::UpdateUITransformInfo()
{
	//position
	std::stringstream xss;
	xss.precision( 5 );

	xss << _pSelectedObject->GetTransform()->pPosition->x;
	m_textCtrl1->SetValue( wxString( xss.str() ) );

	std::stringstream yss;
	yss.precision( 5 );

	yss << _pSelectedObject->GetTransform()->pPosition->y;
	m_textCtrl2->SetValue( wxString( yss.str() ) );

	std::stringstream zss;
	zss.precision( 5 );

	zss << _pSelectedObject->GetTransform()->pPosition->z;
	m_textCtrl3->SetValue( wxString( zss.str() ) );

	//orientation
	std::stringstream oxss;
	oxss.precision( 5 );

	oxss << Framework::DegToRad( _pSelectedObject->GetTransform()->pOrientation->x );
	m_textCtrl4->SetValue( wxString( oxss.str() ) );

	std::stringstream oyss;
	oyss.precision( 5 );

	oyss << Framework::DegToRad( _pSelectedObject->GetTransform()->pOrientation->y );
	m_textCtrl5->SetValue( wxString( oyss.str() ) );

	std::stringstream ozss;
	ozss.precision( 5 );

	ozss << Framework::DegToRad( _pSelectedObject->GetTransform()->pOrientation->z );
	m_textCtrl6->SetValue( wxString( ozss.str() ) );

	//color picker
	glm::vec4 goColor = _pSelectedObject->GetMaterial()->GetDiffuseColor();

	wxColor color( goColor.r * 255, goColor.g * 255, goColor.b * 255, goColor.a * 255 );
	m_colourPicker1->SetColour( color );
}

void BuilderFrameEx::UpdateUIColliderInfo()
{
	Component* pObjectColider = _pSelectedObject->FindCollider();

	if ( pObjectColider != NULL )
	{
		Collider* pCasted = dynamic_cast< Collider* >( ( pObjectColider ) );
		ColliderType colliderType = pCasted->GetColliderType();

		switch ( colliderType )
		{
		case BoxType: SetBoxColliderInfo( pCasted ); break;
		case SphereType: SetSphereColliderInfo( pCasted ); break;
		case PlaneType: SetPlaneColliderInfo( pCasted ); break;
		}

		std::stringstream xss;
		xss.precision( 5 );
		xss << pCasted->GetMass();
		m_textCtrl12->SetValue( wxString( xss.str() ) );

		std::stringstream elasss;
		elasss.precision( 5 );
		elasss << pCasted->GetElasticity();
		m_textCtrl13->SetValue( wxString( elasss.str() ) );
	}
}

void BuilderFrameEx::UpdateLightInfo()
{
	LightGizmo* pGizmo = dynamic_cast< LightGizmo* >( _pSelectedObject );

	if ( pGizmo != NULL )
	{
		boost::shared_ptr<Light> pLight = pGizmo->GetLight();

		glm::vec4 goColor = pLight->GetLightIntensity();
		wxColor color( goColor.r * 255, goColor.g * 255, goColor.b * 255, goColor.a * 255 );
		m_colourPicker11->SetColour( color );
	}
}

void BuilderFrameEx::SetPlaneColliderInfo( Component* pObjectColider )
{
	BulletPlaneCollider* casted = dynamic_cast< BulletPlaneCollider* >( ( pObjectColider ) );

	std::stringstream zss;
	zss.precision( 5 );

	m_choice1->SetSelection( 2 );

	m_textCtrl7->SetValue( wxString( wxT( "Infinite" ) ) );
	m_textCtrl8->SetValue( wxString( wxT( "Infinite" ) ) );
	m_textCtrl9->SetValue( wxString( wxT( "Infinite" ) ) );
	m_textCtrl10->SetValue( wxString( wxT( "None" ) ) );
}

void BuilderFrameEx::SetBoxColliderInfo( Component* pObjectColider )
{
	BulletBoxCollider* casted = dynamic_cast< BulletBoxCollider* >( ( pObjectColider ) );

	m_choice1->SetSelection( 0 );

	glm::vec3 dim = casted->GetDimentions();

	//dimensions 
	std::stringstream xss;
	xss.precision( 5 );

	xss << dim.x;
	m_textCtrl7->SetValue( wxString( xss.str() ) );

	std::stringstream yss;
	yss.precision( 5 );

	yss << dim.y;
	m_textCtrl8->SetValue( wxString( yss.str() ) );

	std::stringstream zss;
	zss.precision( 5 );

	zss << dim.z;
	m_textCtrl9->SetValue( wxString( zss.str() ) );

	m_textCtrl10->SetValue( wxString( wxT( "None" ) ) );
}
void BuilderFrameEx::SetSphereColliderInfo( Component* pObjectColider )
{
	BulletSphereCollider* casted = dynamic_cast< BulletSphereCollider* >( ( pObjectColider ) );

	m_choice1->SetSelection( 1 );

	m_textCtrl7->SetValue( wxString( wxT( "None" ) ) );
	m_textCtrl8->SetValue( wxString( wxT( "None" ) ) );
	m_textCtrl9->SetValue( wxString( wxT( "None" ) ) );

	std::stringstream xss;
	xss.precision( 5 );

	//radius
	xss << casted->GetRadious();
	m_textCtrl10->SetValue( wxString( xss.str() ) );
}

void BuilderFrameEx::UpdateGameObjectCollider()
{
	int choice = m_choice1->GetSelection();
	Collider* pCasted = dynamic_cast< Collider* >( ( _pSelectedObject->FindCollider() ) );

	if ( choice == 0 )
	{

		BulletBoxCollider* pBox = dynamic_cast< BulletBoxCollider* >( ( pCasted ) );

		pBox->SetDimentions( glm::vec3( wxStringToFloat( m_textCtrl7->GetValue() ), wxStringToFloat( m_textCtrl8->GetValue() ),
			wxStringToFloat( m_textCtrl9->GetValue() ) ) );
	}
	else if ( choice == 1 )
	{
		BulletSphereCollider* pSphere = dynamic_cast< BulletSphereCollider* >( ( pCasted ) );

		pSphere->SetRadious( wxStringToFloat( m_textCtrl10->GetValue() ) );
	}
}
