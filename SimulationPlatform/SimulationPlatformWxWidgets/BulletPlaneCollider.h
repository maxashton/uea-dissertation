#pragma once
#include "Common.h"
#include "Collider.h"
#include "GeoPlane.h"

class BulletPlaneCollider :	public Collider
{

public:

	BulletPlaneCollider();
	virtual ~BulletPlaneCollider();

	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual boost::shared_ptr<RigidBody> Create();

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );

protected:

	GeoPlane _geoPlane;
};

