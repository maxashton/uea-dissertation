#pragma once

#include "PhysicsEngine.h"
#include <PxPhysicsAPI.h>

using namespace physx;

class PhysxPhysicsEngine :	public PhysicsEngine
{
		
public:

	PhysxPhysicsEngine();
	virtual ~PhysxPhysicsEngine();

	virtual void Setup();
	virtual void Destroy();
	virtual void StartAsync();
	virtual void AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody );

	static PxPhysics*				gPhysicsSDK;			//Instance of PhysX SDK
	static PxFoundation*			gFoundation;			//Instance of singleton foundation SDK class
	static PxDefaultAllocator		gDefaultAllocatorCallback;	//Instance of default implementation of the allocator interface required by the SDK

protected:

	virtual void UpdatePhysics();

	PxScene*						gScene = NULL;				//Instance of PhysX Scene				
	PxReal							gTimeStep = 1.0f / 60.0f;		//Time-step value for PhysX simulation 
	PxRigidDynamic					*gBox = NULL;				//Instance of box actor 
};

