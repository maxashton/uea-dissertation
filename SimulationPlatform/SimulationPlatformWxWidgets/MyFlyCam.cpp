#include "MyFlyCam.h"
#include <glutil/MatrixStack.h>
#include <glm/gtc/matrix_transform.hpp>

MyFlyCam::MyFlyCam()
{
	_position = glm::vec3( 4.0f, 10.0f, 50.0f );
	_rotation = glm::vec3( -20.0f, 20.0f, 0.0f );
}

MyFlyCam::~MyFlyCam()
{
}

glm::mat4x4 MyFlyCam::GetViewMatrix()
{
	glm::mat4 rotationMatrix;
	rotationMatrix = glm::rotate( rotationMatrix, _rotation.x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
	rotationMatrix = glm::rotate( rotationMatrix, _rotation.y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
	rotationMatrix = glm::rotate( rotationMatrix, _rotation.z, glm::vec3( 0.0f, 0.0f, 1.0f ) );

	glm::vec4 forwardVector = rotationMatrix * glm::vec4( 0.0f, 0.0f, -1.0f, 0.0f );
	glm::vec3 forward = glm::vec3( forwardVector.x, forwardVector.y, forwardVector.z );

	glm::vec3 lookPos = _position + glm::normalize( forward );

	glm::mat4 View = glm::lookAt(
		_position,
		lookPos,
		glm::vec3( 0, 1, 0 ) 
		);

	return View;
}
