#include "BulletSphereCollider.h"
#include "Scene.h"
#include "RenderFactory.h"
#include "RigidBodyFactory.h"

BulletSphereCollider::BulletSphereCollider()
{
	_elasticity = 0.0f;
	_colliderType = SphereType;
}

BulletSphereCollider::~BulletSphereCollider()
{
}

void BulletSphereCollider::Draw( glm::mat4 mCameraMatrix )
{
	//sphere
	if ( _pRigidBody != NULL )
	{
		glm::mat4 matrix = _pRigidBody->GetModelMatrix();
		_icoSphere.SetModelMatrix( matrix );
	}
	else
	{
		if ( _pGameObject != NULL )
		{
			_icoSphere.GetTransform()->pPosition->x = _pGameObject->GetTransform()->pPosition->x;
			_icoSphere.GetTransform()->pPosition->y = _pGameObject->GetTransform()->pPosition->y;
			_icoSphere.GetTransform()->pPosition->z = _pGameObject->GetTransform()->pPosition->z;

			_icoSphere.GetTransform()->pOrientation->x = _pGameObject->GetTransform()->pOrientation->x;
			_icoSphere.GetTransform()->pOrientation->y = _pGameObject->GetTransform()->pOrientation->y;
			_icoSphere.GetTransform()->pOrientation->z = _pGameObject->GetTransform()->pOrientation->z;
		}
	}

	_icoSphere.Draw( mCameraMatrix );
}

boost::shared_ptr<RigidBody> BulletSphereCollider::Create( float mass, float elasticity, float radious, bool noBody )
{
	_mass = mass;
	_elasticity = elasticity;
	_radius = radious;

	//create drawable bounding sphere
	_icoSphere.Create( 2 );
	_icoSphere.ConnectRenderer( RenderFactory::CreateRenderer() );
	_icoSphere.GetTransform()->pScale = new glm::vec3( radious, radious, radious );

	if ( !noBody )
	{
		_pRigidBody = RigidBodyFactory::CreateSphereBody( mass, elasticity, radious );

		return _pRigidBody;
	}

	return NULL;
}

void BulletSphereCollider::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType /*= true */ )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "CType", "BulletSphereCollider" );
	}

	Component::SerializeToXLM( pInTree, false );

	pInTree->put( "Radius", _radius );
	pInTree->put( "Mass", _mass );
	pInTree->put( "Elasticity", _elasticity );
}

void BulletSphereCollider::DeSerializeFromXML( ptree::value_type const& v )
{
	Component::DeSerializeFromXML( v );

	Create( v.second.get<float>( "Mass" ), v.second.get<float>( "Elasticity" ), v.second.get<float>( "Radius" ) );
	_elasticity = v.second.get<float>( "Elasticity" );
}

float BulletSphereCollider::GetRadious()
{
	return _radius;
}

void BulletSphereCollider::SetRadious( float radius )
{
	_radius = radius;

	_icoSphere.GetTransform()->pScale = new glm::vec3( radius, radius, radius );
}
