#pragma once

#include "Common.h"
#include "OpenGLCommon.h"
#include <glm/glm.hpp>

namespace Framework
{
	float DegToRad( float fAngDeg );

	GLuint CreateShader( GLenum eShaderType, const std::string &strShaderFile, const std::string &strShaderName );
	GLuint LoadShader( GLenum eShaderType, const std::string &strShaderFilename );

	GLuint CreateProgram( const std::vector<GLuint> &shaderList );

	std::string FindFileOrThrow( const std::string &strFilename );

	glm::mat4 CreateStandardViewMatrix( float fFrustumScale, float fzFar, float fzNear );
	float CalcFrustumScale( float fFovDeg );

	GLuint FCreateShader( GLenum eShaderType, const std::string &strShaderFile );
	GLuint FCreateProgram( const std::vector<GLuint> &shaderList );

	unsigned int defaults( unsigned int displayMode, int &width, int &height );
	void APIENTRY DebugFunc( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam );

	float RandomFloat( float a, float b );
	float RandomFloatToDP( float a, float b, int dp );
};

