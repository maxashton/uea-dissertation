#ifdef _WIN32
#pragma once
#include "Common.h"
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include "Structs.h"

struct ObjectBuffer
{
	glm::vec4 diffuseColor;
	glm::vec4 specularColor;
	glm::mat4 modelToCameraMatrix;
	glm::mat4 normalModelToCameraMatrix;
	float specularShininess;
};

struct LightBuffer
{
	glm::vec4 ambientIntensity;

	LightInfo lights [3];

	float lightAttenuation;
	float maxIntensity;
	float gamma;
};

struct ProjectionBuffer
{
	D3DXMATRIX cameraToClipMatrix;
};

class Direct3DShader
{

public:

	Direct3DShader();
	~Direct3DShader();

	static void SetCameraToClipMatrix( int w, int h );
	static void SetupConstantBuffers();
	static void SetRasterState( D3D11_FILL_MODE fillMode = D3D11_FILL_SOLID );
	static void Release();
	static void UpdateLightingBuffers( LightBlock* pLightBlock );
	void CreateVertexAndPixelShader( std::string fileLocation );
	void CreateVertexAndPixelShaderGemetryOnly( std::string fileLocation );

	//const Direct3DShaderPropoties GetShaderPropoties();
	const glm::mat4 GetCameraToClipMatrix();

	static ID3D11RasterizerState* RasterState; 
	static IDXGISwapChain *SwapChain;
	static ID3D11Device *Device;
	static ID3D11DeviceContext *DeviceContext;
	static ID3D11RenderTargetView *BackBuffer;
	static ID3D11DepthStencilView *ZBuffer;
	
	static std::vector<ID3D11Buffer*> ConstBuffers;
	
	ID3D11InputLayout* GetLayout() const;
	ID3D11VertexShader* GetVertexShader() const;
	ID3D11PixelShader* GetPixelShader() const;

	void SetDeviceContextWithShader();
 
protected:

	std::vector<unsigned int> _shaderList;
	glm::mat4 _cameraToClipMatrix;
	//Direct3DShaderPropoties _strctShaderPropoties;

	ID3D11InputLayout * _pLayout;
	ID3D11VertexShader* _pVShader;
	ID3D11PixelShader* _pPShader;

	ObjectBuffer _objectBuffer;
	LightBuffer _lightBuffer;
};
#endif
