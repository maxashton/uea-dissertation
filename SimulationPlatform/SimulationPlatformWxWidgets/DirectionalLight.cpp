#include "DirectionalLight.h"
#include <glm/gtc/quaternion.hpp>

DirectionalLight::DirectionalLight()
{
}


DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::SetLightDirection( float fDir )
{
	_fLightDirection = fDir;
}

glm::vec4 DirectionalLight::GetLightDirection()
{
	float angle = 2.0f * 3.14159f * _fLightDirection;
	glm::vec4 sunDirection( 0.0f );
	sunDirection[ 0 ] = sinf( angle );
	sunDirection[ 1 ] = cosf( angle );

	//Keep the sun from being perfectly centered overhead.
	sunDirection = glm::rotate( glm::quat( glm::mat4( 1.0f ) ), 5.0f, glm::vec3( 0.0f, 1.0f, 0.0f ) ) * sunDirection;

	return sunDirection;
}

float DirectionalLight::GetLightDirectionFloat()
{
	return _fLightDirection;
}

void DirectionalLight::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType /*= true */ )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "Type", "DirectionalLight" );
	}

	Light::SerializeToXLM( pInTree, false );

	ptree & position = pInTree->add( "Direction", _fLightDirection );
}

void DirectionalLight::DeSerializeFromXML( ptree::value_type const& v )
{
	Light::DeSerializeFromXML( v );
	
	_fLightDirection = v.second.get<float>( "Direction" );
}
