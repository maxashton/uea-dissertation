#include "PhysicsEngine.h"

PhysicsEngine::PhysicsEngine()
{
	_physicsStep = 0.0f;
	_physicsInitialSet = true;
	_runSimulation = false;
	_physicsRunning = true;

	_pRigidBodies = boost::shared_ptr<std::vector<boost::shared_ptr<RigidBody> >>( new std::vector<boost::shared_ptr<RigidBody> >() );
}


PhysicsEngine::~PhysicsEngine()
{
}

void PhysicsEngine::Setup()
{

}

void PhysicsEngine::Destroy()
{
	if ( _pPhysicsThread )
	{
		_pPhysicsThread->interrupt();

		//wait until physics thread exits
		while ( _physicsRunning )
		{
			if ( !_physicsRunning )
			{
				break;
			}
		}

		delete _pPhysicsThread;
	}
}

void PhysicsEngine::StartAsync()
{
	_pPhysicsThread = new boost::thread( boost::bind( &PhysicsEngine::UpdatePhysics, this ) );
}

void PhysicsEngine::Pause()
{
	_runSimulation = false;
}

void PhysicsEngine::Run()
{
	_runSimulation = true;
}

float PhysicsEngine::GetLastTimeStep()
{
	return _physicsStep;
}

void PhysicsEngine::SetInitialStep( bool initialStep )
{
	_physicsInitialSet = initialStep;
}

void PhysicsEngine::AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody )
{
	_pRigidBodies->push_back( pRigidBody );
}

void PhysicsEngine::UpdatePhysics()
{

}
