#pragma once
#include "Common.h"
#include "GameObject.h"
#include <bullet/btBulletDynamicsCommon.h>
#include "PhysicsEngine.h"
#include "Structs.h"

class SceneIO
{

public:

	SceneIO();
	~SceneIO();

	static void Write( std::string fileLocation, std::vector<GameObject*>* pScene, std::vector<boost::shared_ptr<PointLight> >* pvPointLights,
					   std::vector<DirectionalLight*>* pvDirectionalLights, glm::vec4 ambientIntensity, std::string renderer,
					   std::string physicsEngine );
	static void Read( std::string fileLocation, LightManager* pLightManager, std::vector<GameObject*>* pObjects, boost::shared_ptr<PhysicsEngine> pPhysicsEngine );

	static LoadingOutputData ReadSettings( std::string fileLocation );

};

