#include "LightManager.h"

LightManager::LightManager()
{
	_pvPointLights = new std::vector<boost::shared_ptr<PointLight> >();
	_pvDirectionalLights = new std::vector<DirectionalLight*>();
}

LightManager::~LightManager()
{
	if ( _pvPointLights )
	{
		delete _pvPointLights;
	}

	if ( _pvDirectionalLights )
	{
		for ( std::vector<DirectionalLight*>::iterator it = _pvDirectionalLights->begin(); it != _pvDirectionalLights->end(); ++it )
		{
			delete *it;
		}

		delete _pvDirectionalLights;
	}
}

void LightManager::AddPointLight( boost::shared_ptr<PointLight> pLight )
{
	_pvPointLights->push_back( pLight );
}

void LightManager::RemoveLight( boost::shared_ptr<Light> pLight )
{
	//check if point light
	boost::shared_ptr<PointLight> point = boost::dynamic_pointer_cast<PointLight>( pLight );

	if ( point != NULL )
	{
		std::vector<boost::shared_ptr<PointLight> >::iterator i = std::find( _pvPointLights->begin(), _pvPointLights->end(), point );
		_pvPointLights->erase( i );
	}
}

void LightManager::AddDirectionalLight( DirectionalLight* pLight )
{
	_pvDirectionalLights->push_back( pLight );
}

LightBlock LightManager::GetLightInfo( const glm::mat4 &worldToCameraMat ) const
{
	LightBlock lightData;
	lightData.gamma = _fGamma;

	lightData.ambientIntensity = _v4AmbientIntensity;
	lightData.lightAttenuation = _fLightAttenuation;
	lightData.maxIntensity = _fMaxItensity;

	int i = 0;
	for ( std::vector<DirectionalLight*>::iterator it = _pvDirectionalLights->begin(); it != _pvDirectionalLights->end(); ++it, i++ )
	{
		lightData.lights[ i ].cameraSpaceLightPos = worldToCameraMat * ( *it )->GetLightDirection();
		lightData.lights[ i ].lightIntensity = ( *it )->GetLightIntensity();
	}

	for ( std::vector<boost::shared_ptr<PointLight> >::iterator it = _pvPointLights->begin(); it != _pvPointLights->end(); ++it, i++ )
	{
		glm::vec4 worldLightPos = glm::vec4( *( *it )->GetLightPossition(), 1.0f );
		glm::vec4 lightPosCameraSpace = worldToCameraMat * worldLightPos;

		lightData.lights[ i ].cameraSpaceLightPos = lightPosCameraSpace;
		lightData.lights[ i ].lightIntensity = ( * it )->GetLightIntensity();
	}

	return lightData;
}

void LightManager::SetGamma( float fGamma )
{
	_fGamma = fGamma;
}

float LightManager::GetGamma()
{
	return _fGamma;
}

std::vector<boost::shared_ptr<PointLight> >* LightManager::GetPointLights()
{
	return _pvPointLights;
}

std::vector<DirectionalLight*>* LightManager::GetDirectionLights()
{
	return _pvDirectionalLights;
}

void LightManager::SetAmbiantIntensity( glm::vec4 v4AmbiantIntensity )
{
	_v4AmbientIntensity = v4AmbiantIntensity;
}

glm::vec4 LightManager::GetAmbiantIntensity()
{
	return _v4AmbientIntensity;
}

void LightManager::SetLightAttenuation( float fLightAttenuation )
{
	_fLightAttenuation = fLightAttenuation;
}

void LightManager::SetMaxIntensity( float fMaxIntensity )
{
	_fMaxItensity = fMaxIntensity;
}

