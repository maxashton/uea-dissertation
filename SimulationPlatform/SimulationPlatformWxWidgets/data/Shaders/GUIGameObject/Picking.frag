#version 330

in vec3 colorID;

out vec4 color;

void main()
{
	color = vec4( colorID, 1.0f );
}