///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NONAME_H__
#define __NONAME_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/statbox.h>
#include <wx/listbox.h>
#include <wx/clrpicker.h>
#include <wx/choice.h>
#include <wx/slider.h>
#include <wx/notebook.h>
#include <wx/splitter.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class BuilderFrame
///////////////////////////////////////////////////////////////////////////////
class BuilderFrame : public wxFrame
{
private:

protected:
	wxMenuBar* m_menubar1;
	wxMenu* Files;
	wxSplitterWindow* m_splitter3;
	wxPanel* m_panel1;
	wxBoxSizer* bSizer19;
	wxPanel* m_panel2;
	wxNotebook* m_notebook1;
	wxPanel* m_panel3;
	wxStaticText* m_staticText8;
	wxTextCtrl* m_filePickBox;
	wxButton* m_filePickButton;
	wxButton* m_button6;
	wxListBox* m_listBox1;
	wxButton* m_button3;
	wxButton* m_button4;
	wxPanel* m_panel4;
	wxStaticText* m_staticText29;
	wxTextCtrl* m_textCtrl1;
	wxStaticText* m_staticText81;
	wxTextCtrl* m_textCtrl2;
	wxStaticText* m_staticText9;
	wxTextCtrl* m_textCtrl3;
	wxStaticText* m_staticText10;
	wxTextCtrl* m_textCtrl4;
	wxStaticText* m_staticText11;
	wxTextCtrl* m_textCtrl5;
	wxStaticText* m_staticText12;
	wxTextCtrl* m_textCtrl6;
	wxStaticText* m_staticText45;
	wxColourPickerCtrl* m_colourPicker1;
	wxStaticText* m_staticText15;
	wxChoice* m_choice1;
	wxStaticText* m_staticText16;
	wxTextCtrl* m_textCtrl7;
	wxStaticText* m_staticText17;
	wxTextCtrl* m_textCtrl8;
	wxStaticText* m_staticText18;
	wxTextCtrl* m_textCtrl9;
	wxStaticText* m_staticText19;
	wxTextCtrl* m_textCtrl10;
	wxStaticText* m_staticText181;
	wxTextCtrl* m_textCtrl12;
	wxStaticText* m_staticText191;
	wxTextCtrl* m_textCtrl13;
	wxPanel* m_panel11;
	wxStaticText* m_staticText46;
	wxColourPickerCtrl* m_colourPicker11;
	wxSlider* m_slider2;
	wxPanel* m_panel5;
	wxStaticText* m_staticText20;
	wxChoice* m_choice2;
	wxStaticText* m_staticText21;
	wxChoice* m_choice3;
	wxStaticText* m_staticText28;
	wxColourPickerCtrl* m_colourPicker3;
	wxPanel* Scene;
	wxListBox* m_listBox2;
	wxButton* m_button31;

	// Virtual event handlers, overide them in your derived class
	virtual void Load_Click( wxCommandEvent& event ) { event.Skip(); }
	virtual void Save( wxCommandEvent& event ) { event.Skip(); }
	virtual void Exit( wxCommandEvent& event ) { event.Skip(); }
	virtual void RightClick( wxMouseEvent& event ) { event.Skip(); }
	virtual void MyFilePicker1( wxCommandEvent& event ) { event.Skip(); }
	virtual void Create_Add_Click( wxCommandEvent& event ) { event.Skip(); }
	virtual void Create_Create_Obj( wxCommandEvent& event ) { event.Skip(); }
	virtual void Create_Add_PointLight( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Pos_X( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Pos_Y( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Pos_Z( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Ori_X( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Ori_Y( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Ori_Z( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Tint_Color( wxColourPickerEvent& event ) { event.Skip(); }
	virtual void Prop_Collider_Type( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Col_Width( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Col_Height( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Col_Depth( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Col_Radius( wxCommandEvent& event ) { event.Skip(); }
	virtual void Mass_Changed( wxCommandEvent& event ) { event.Skip(); }
	virtual void Elasticity_Changed( wxCommandEvent& event ) { event.Skip(); }
	virtual void Prop_Light_Color( wxColourPickerEvent& event ) { event.Skip(); }
	virtual void Ambient_Color_Change( wxColourPickerEvent& event ) { event.Skip(); }
	virtual void Scene_List_Change( wxCommandEvent& event ) { event.Skip(); }
	virtual void Scene_Delete_Click( wxCommandEvent& event ) { event.Skip(); }


public:

	BuilderFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1123, 622 ), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );

	~BuilderFrame();

	void m_splitter3OnIdle( wxIdleEvent& )
	{
		m_splitter3->SetSashPosition( 764 );
		m_splitter3->Disconnect( wxEVT_IDLE, wxIdleEventHandler( BuilderFrame::m_splitter3OnIdle ), NULL, this );
	}

};

#endif //__NONAME_H__
