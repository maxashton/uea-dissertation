#pragma once

#include "Common.h"
#include "Component.h"
#include "RigidBody.h"

enum ColliderType
{
	None,
	BoxType,
	SphereType,
	PlaneType
};

class GameObject;

class Collider : public Component
{

public:

	Collider();
	virtual ~Collider();

	virtual void Draw();
	virtual boost::shared_ptr<RigidBody> Create();
	virtual void Update();
	virtual void AttachGameObject( GameObject* pGameObject );
	boost::shared_ptr<RigidBody> GetRigidBody();
	void RemoveRigidBody();

	ColliderType GetColliderType();
	virtual IdentifyType Identify();

	float GetElasticity();
	void SetElasticity( float val );

	float GetMass();
	void SetMass( float val );

protected: 

	boost::shared_ptr<RigidBody> _pRigidBody;
	float _mass;
	float _elasticity;

	ColliderType _colliderType;
};