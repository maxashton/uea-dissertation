#pragma once
#include "Common.h"

class Texture
{

public:

	Texture();
	virtual ~Texture();

	virtual void CreateEmptyTexture( int iWidth, int iHeight, unsigned int format );
	virtual void CreateFromData( BYTE* bData, int iWidth, int iHeight, int iBPP, unsigned int format, bool bGenerateMipMaps = false );
	virtual void SetSamplerParameter( unsigned int parameter, unsigned int value );
	virtual void DeleteTexture();
	virtual void BindTexture();

	virtual bool ReloadTexture();
	virtual bool LoadTexture2D( std::string sPath );

	int GetWidth();
	int GetHeight();
	int GetBPP();

	std::string GetPath();

	virtual	Texture* Clone();

protected:

	int _iWidth;
	int _iHeight;
	int _iBPP;
	bool _bMipMapsGenerated;

	int _tfMinification;
	int _tfMagnification;

	std::string _sPath;

};

