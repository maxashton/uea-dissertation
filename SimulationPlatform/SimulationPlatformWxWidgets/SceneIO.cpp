#include "SceneIO.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "AssimpGameObject.h"
#include "RenderFactory.h"
#include "BulletPlaneCollider.h"
#include "BulletSphereCollider.h"
#include "BulletBoxCollider.h"

SceneIO::SceneIO()
{
}


SceneIO::~SceneIO()
{
}

void SceneIO::Write( std::string fileLocation, std::vector<GameObject*>* pScene, std::vector<boost::shared_ptr<PointLight> >* pvPointLights, std::vector<DirectionalLight*>* pvDirectionalLights,
					 glm::vec4 ambientIntensity, std::string renderer, std::string physicsEngine )
{
	ptree pt;

	pt.add( "Scene.version", 3 );

	//settings
	ptree &settingList = pt.add( "Scene.Settings", "" );

	ptree & rendererNode = settingList.add( "Renderer", renderer );
	ptree & physics = settingList.add( "PhysicsEngine", physicsEngine );

	//ambient intensity
	ptree & ambientIntensityNode = settingList.add( "AmbientIntensity", "" );
	ambientIntensityNode.put( "R", ambientIntensity.r );
	ambientIntensityNode.put( "G", ambientIntensity.r );
	ambientIntensityNode.put( "B", ambientIntensity.r );
	ambientIntensityNode.put( "A", ambientIntensity.r );

	//lights
	ptree &lightList = pt.add( "Scene.Lights", "" );

	for ( std::vector<boost::shared_ptr<PointLight> >::iterator i = pvPointLights->begin(); i != pvPointLights->end(); i++ )
	{
		ptree & lightObject = lightList.add( "Light", "" );

		( *i )->SerializeToXLM( &lightObject );
	}


	//game objects
	ptree & objectList = pt.add( "Scene.ObjectList", "" );

	for ( std::vector<GameObject*>::iterator i = pScene->begin(); i != pScene->end(); i++ )
	{
		ptree & gameObject = objectList.add( "GameObject", "" );
		ptree & pickingNode = gameObject.add( "PickingColour", "" );

		glm::vec4 color = ( *i )->GetPickingColor();
		pickingNode.add( "r", color.r );
		pickingNode.add( "g", color.g );
		pickingNode.add( "b", color.b );
		pickingNode.add( "a", color.a );

		( *i )->SerializeToXLM( &gameObject );
	}

	write_xml( fileLocation, pt );
}

void SceneIO::Read( std::string fileLocation, LightManager* pLightManager, std::vector<GameObject*>* pObjects, boost::shared_ptr<PhysicsEngine> pPhysicsEngine )
{
	ptree pt;
	read_xml( fileLocation, pt );

	BOOST_FOREACH( ptree::value_type &v, pt.get_child( "Scene.Lights" ) )
	{
		if ( v.first == "Light" )
		{
			if ( v.second.get<std::string>( "Type" ) == "PointLight" )
			{
				boost::shared_ptr<PointLight> pLight = boost::shared_ptr<PointLight>( new PointLight() );
				pLight->DeSerializeFromXML( v );
				pLightManager->AddPointLight( pLight );
			}
			else if ( v.first == "DirectionalLight" )
			{
				DirectionalLight* pLight = new DirectionalLight();
				pLight->DeSerializeFromXML( v );
				pLightManager->AddDirectionalLight( pLight );
			}
		}
	}

	BOOST_FOREACH( ptree::value_type const& v, pt.get_child( "Scene.ObjectList" ) )
	{
		if ( v.first == "GameObject" ) 
		{
			AssimpGameObject* pGameObject;
			if ( v.second.get<std::string>( "Type" ) == "AssimpGameObject" )
			{
				pGameObject = new AssimpGameObject();
				pGameObject->ConnectRenderer( RenderFactory::CreateRenderer() );
				pGameObject->DeSerializeFromXML( v );

				BOOST_FOREACH( ptree::value_type const& chomV, v.second.get_child( "Components" ) )
				{
					if ( chomV.first == "Component" )
					{
						try
						{
							if ( chomV.second.get<std::string>( "CType" ) == "BulletSphereCollider" )
							{
								BulletSphereCollider* pComponent = new BulletSphereCollider();
								pComponent->DeSerializeFromXML( chomV );
								boost::shared_ptr<RigidBody> body = pComponent->GetRigidBody();
								pPhysicsEngine->AddRigidBody( body );
								pGameObject->AddComponenet( pComponent );
							}
							else if ( chomV.second.get<std::string>( "CType" ) == "BulletPlaneCollider" )
							{
								BulletPlaneCollider* pComponent = new BulletPlaneCollider();
								pComponent->DeSerializeFromXML( chomV );
								boost::shared_ptr<RigidBody> body = pComponent->GetRigidBody();
								pPhysicsEngine->AddRigidBody( body );
								pGameObject->AddComponenet( pComponent );
							}
							else if ( chomV.second.get<std::string>( "CType" ) == "BulletBoxCollider" )
							{
								BulletBoxCollider* pComponent = new BulletBoxCollider();
								pComponent->DeSerializeFromXML( chomV );
								boost::shared_ptr<RigidBody> body = pComponent->GetRigidBody();
								pPhysicsEngine->AddRigidBody( body );
								pGameObject->AddComponenet( pComponent );
							}

						}
						catch ( boost::exception const&  ex )
						{

						}
					}
				}

				ptree colorNode = v.second.get_child( "PickingColour" );
				glm::vec4 color = glm::vec4( colorNode.get<float>( "r" ), colorNode.get<float>( "g" ), colorNode.get<float>( "b" ), colorNode.get<float>( "a" ) );
				
				pGameObject->SetPickingColor( color );

				pObjects->push_back( pGameObject );
			}
		}
	}
}

LoadingOutputData SceneIO::ReadSettings( std::string fileLocation )
{
	LoadingOutputData output;

	std::ifstream file( fileLocation );

	ptree pt;
	read_xml( file, pt );

	BOOST_FOREACH( ptree::value_type const& v, pt.get_child( "Scene.Settings" ) )
	{
		if ( v.first == "Renderer" )
		{
			output.renderer = v.second.get_value<std::string>();
		}
		else if ( v.first == "PhysicsEngine" )
		{
			output.physicsEngine = v.second.get_value<std::string>();
		}
		else if ( v.first == "AmbientIntensity" )
		{
			glm::vec4 vec( v.second.get<float>( "R" ), v.second.get<float>( "G" ), v.second.get<float>( "B" ), v.second.get<float>( "A" ) );
			output.ambientLight = vec;
		}
	}

	return output;
}
