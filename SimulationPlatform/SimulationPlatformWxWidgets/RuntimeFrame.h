///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __NONAME_H__1
#define __NONAME_H__1

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/sizer.h>
#include <wx/gdicmn.h>
#include <wx/panel.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/splitter.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class RuntimeFrame
///////////////////////////////////////////////////////////////////////////////
class RuntimeFrame : public wxFrame
{
private:

protected:
	wxSplitterWindow* m_splitter1;
	wxPanel* m_panel1;
	wxBoxSizer* bSizer2;
	wxPanel* m_panel2;
	wxStaticText* m_staticText1;
	wxStaticText* m_staticText2;
	wxStaticText* m_staticText3;
	wxStaticText* m_staticText4;

public:

	RuntimeFrame( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 982, 300 ), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );

	~RuntimeFrame();

	void m_splitter1OnIdle( wxIdleEvent& )
	{
		m_splitter1->SetSashPosition( 1700 );
		m_splitter1->Disconnect( wxEVT_IDLE, wxIdleEventHandler( RuntimeFrame::m_splitter1OnIdle ), NULL, this );
	}

};

#endif //__NONAME_H__
