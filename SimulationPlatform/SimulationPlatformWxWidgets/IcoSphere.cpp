#include "IcoSphere.h"

IcoSphere::IcoSphere()
{
}


IcoSphere::~IcoSphere()
{
}

TriangleIndices::TriangleIndices( int v1, int v2, int v3 )
{
	this->v1 = v1;
	this->v2 = v2;
	this->v3 = v3;
}

// add vertex to mesh, fix position to be on unit sphere, return index
int IcoSphere::AddVertex( glm::vec3 p )
{
	double length = glm::sqrt( p.x * p.x + p.y * p.y + p.z * p.z );
	_geometry.push_back( glm::vec3( p.x / length, p.y / length, p.z / length ) );
	return _index++;
}

// return index of point in the middle of p1 and p2
int IcoSphere::GetMiddlePoint( int p1, int p2 )
{
	//// first check if we have it already
	//bool firstIsSmaller = p1 < p2;
	//long smallerIndex = firstIsSmaller ? p1 : p2;
	//long greaterIndex = firstIsSmaller ? p2 : p1;
	//long key = ( smallerIndex << 32 ) + greaterIndex;
	long key = 0;


	//std::map<long, int>::iterator it = _middlePointIndexCache.find( key );
	//int findVal;
	//if ( it != _middlePointIndexCache.end() )
	//{
	//	findVal = it->second;
	//	//return findVal;
	//}

	// not in cache, calculate it
	glm::vec3 point1 = _geometry[ p1 ];
	glm::vec3 point2 = _geometry[ p2 ];
	glm::vec3 middle = glm::vec3( ( point1.x + point2.x ) / 2.0, ( point1.y + point2.y ) / 2.0, ( point1.z + point2.z ) / 2.0 );

	// add vertex makes sure point is on unit sphere
	int i = AddVertex( middle );

	// store it, return index
	_middlePointIndexCache.insert( std::pair<long, int>( key, i ) );

	return i;
}

void IcoSphere::Create( int recursionLevel )
{
	_geometry = std::vector<glm::vec3>();
	_middlePointIndexCache = std::map<long, int>();
	_index = 0;

	// create 12 vertices of a icosahedron
	float t = ( 1.0f + glm::sqrt( 5.0f ) ) / 2.0f;

	AddVertex( glm::vec3( -1.0f, t, 0.0f ) );
	AddVertex( glm::vec3( 1.0f, t, 0.0f ) );
	AddVertex( glm::vec3( -1.0f, -t, 0.0f ) );
	AddVertex( glm::vec3( 1.0f, -t, 0.0f ) );

	AddVertex( glm::vec3( 0.0f, -1.0f, t ) );
	AddVertex( glm::vec3( 0.0f, 1.0f, t ) );
	AddVertex( glm::vec3( 0.0f, -1.0f, -t ) );
	AddVertex( glm::vec3( 0.0f, 1.0f, -t ) );

	AddVertex( glm::vec3( t, 0.0f, -1.0f ) );
	AddVertex( glm::vec3( t, 0.0f, 1.0f ) );
	AddVertex( glm::vec3( -t, 0.0f, -1.0f ) );
	AddVertex( glm::vec3( -t, 0.0f, 1.0f ) );


	// create 20 triangles of the icosahedron
	std::vector<TriangleIndices> faces = std::vector<TriangleIndices>();

	// 5 faces around point 0
	faces.push_back( TriangleIndices( 0, 11, 5 ) );
	faces.push_back( TriangleIndices( 0, 5, 1 ) );
	faces.push_back( TriangleIndices( 0, 1, 7 ) );
	faces.push_back( TriangleIndices( 0, 7, 10 ) );
	faces.push_back( TriangleIndices( 0, 10, 11 ) );

	// 5 adjacent faces 
	faces.push_back( TriangleIndices( 1, 5, 9 ) );
	faces.push_back( TriangleIndices( 5, 11, 4 ) );
	faces.push_back( TriangleIndices( 11, 10, 2 ) );
	faces.push_back( TriangleIndices( 10, 7, 6 ) );
	faces.push_back( TriangleIndices( 7, 1, 8 ) );

	// 5 faces around point 3
	faces.push_back( TriangleIndices( 3, 9, 4 ) );
	faces.push_back( TriangleIndices( 3, 4, 2 ) );
	faces.push_back( TriangleIndices( 3, 2, 6 ) );
	faces.push_back( TriangleIndices( 3, 6, 8 ) );
	faces.push_back( TriangleIndices( 3, 8, 9 ) );

	// 5 adjacent faces 
	faces.push_back( TriangleIndices( 4, 9, 5 ) );
	faces.push_back( TriangleIndices( 2, 4, 11 ) );
	faces.push_back( TriangleIndices( 6, 2, 10 ) );
	faces.push_back( TriangleIndices( 8, 6, 7 ) );
	faces.push_back( TriangleIndices( 9, 8, 1 ) );


	// refine triangles
	for ( int i = 0; i < recursionLevel; i++ )
	{
		std::vector<TriangleIndices>  faces2 = std::vector<TriangleIndices>();

		for ( auto &tri : faces )
		{
			// replace triangle by 4 triangles
			int a = GetMiddlePoint( tri.v1, tri.v2 );
			int b = GetMiddlePoint( tri.v2, tri.v3 );
			int c = GetMiddlePoint( tri.v3, tri.v1 );

			faces2.push_back( TriangleIndices( tri.v1, a, c ) );
			faces2.push_back( TriangleIndices( tri.v2, b, a ) );
			faces2.push_back( TriangleIndices( tri.v3, c, b ) );
			faces2.push_back( TriangleIndices( a, b, c ) );
		}

		faces = faces2;
	}

	// done, now add triangles to mesh
	for ( auto &tri : faces )
	{
		_indicies.push_back( tri.v1 );
		_indicies.push_back( tri.v2 );
		_indicies.push_back( tri.v3 );
	}
}

std::vector<GLshort> IcoSphere::GetIndicies()
{
	return _indicies;
}

std::vector<glm::vec3> IcoSphere::GetGeometry()
{
	return _geometry;
}

void IcoSphere::Draw( glm::mat4 mCameraMatrix )
{
	_pRenderer->DrawIcoSphere( mCameraMatrix, _modelMatrixOverride, _modelMatrix, _pTransform, GetNumberOfInicies() );
}

int IcoSphere::GetNumberOfInicies()
{
	return ( int ) _indicies.size();
}

void IcoSphere::ConnectRenderer( Renderer* pRenderer )
{
	GameObject::ConnectRenderer( pRenderer );

	pRenderer->BuildGeometricDiffuseShader();
	pRenderer->InitializeGeometricVertexBuffer( _indicies, _geometry );
}
