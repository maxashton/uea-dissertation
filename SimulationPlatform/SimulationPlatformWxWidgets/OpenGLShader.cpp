#include "OpenGLShader.h"
#include "Material.h"
#include "LightManager.h"
#include "Framework.h"

int OpenGLShader::PROJECTION_BLOCK_INDEX = 0;
int OpenGLShader::MATERIAL_BLOCK_INDEX = 1;
int OpenGLShader::LIGHT_BLOCK_INDEX = 2;

GLuint OpenGLShader::LIGHT_UNIFORM_BUFFER = NULL;
GLuint OpenGLShader::PROJECTION_UNIFORM_BUFFER = NULL;
GLuint OpenGLShader::MATERIAL_UNIFORM_BUFFER = NULL;

glm::mat4 OpenGLShader::_cameraToClipMatrix = glm::mat4();

OpenGLShader::OpenGLShader()
{
	_cameraToClipMatrix = glm::mat4( 0.0f );
}

OpenGLShader::~OpenGLShader()
{
}

void OpenGLShader::SetupStaticBuffers()
{
	//generate buffers
	glGenBuffers( 1, &PROJECTION_UNIFORM_BUFFER );
	glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::PROJECTION_UNIFORM_BUFFER );
	glBufferData( GL_UNIFORM_BUFFER, sizeof( ProjectionBlock ), NULL, GL_DYNAMIC_DRAW );

	glGenBuffers( 1, &MATERIAL_UNIFORM_BUFFER );
	glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::MATERIAL_UNIFORM_BUFFER );
	glBufferData( GL_UNIFORM_BUFFER, sizeof( Material ), NULL, GL_DYNAMIC_DRAW );

	glGenBuffers( 1, &LIGHT_UNIFORM_BUFFER );
	glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::LIGHT_UNIFORM_BUFFER );
	glBufferData( GL_UNIFORM_BUFFER, sizeof( LightBlock ), NULL, GL_DYNAMIC_DRAW );

	//Bind the static buffers.
	glBindBufferRange( GL_UNIFORM_BUFFER, OpenGLShader::PROJECTION_BLOCK_INDEX, OpenGLShader::PROJECTION_UNIFORM_BUFFER, 0, sizeof( ProjectionBlock ) );
	glBindBufferRange( GL_UNIFORM_BUFFER, OpenGLShader::MATERIAL_BLOCK_INDEX, OpenGLShader::MATERIAL_UNIFORM_BUFFER, 0, sizeof( Material ) );
	glBindBufferRange( GL_UNIFORM_BUFFER, OpenGLShader::LIGHT_BLOCK_INDEX, OpenGLShader::LIGHT_UNIFORM_BUFFER, 0, sizeof( LightBlock ) );

	glBindBuffer( GL_UNIFORM_BUFFER, 0 );
}

void OpenGLShader::SetCameraToClipMatrix( int w, int h )
{
	float fzNear = 1.0f; float fzFar = 300.0f;

	glutil::MatrixStack persMatrix;
	persMatrix.Perspective( 45.0f, ( w / ( float ) h ), fzNear, fzFar );

	_cameraToClipMatrix = persMatrix.Top();

	glutil::MatrixStack orthoMatrix;
	orthoMatrix.SetMatrix( glm::ortho( 0.0f, ( float )w, ( float )h, 0.0f, 0.0f, 100.0f ) );

	ProjectionBlock projData;
	projData.cameraToClipMatrix = persMatrix.Top();
	projData.orthoMatrix = orthoMatrix.Top();

	glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::PROJECTION_UNIFORM_BUFFER );
	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( ProjectionBlock ), &projData );
	glBindBuffer( GL_UNIFORM_BUFFER, 0 );

	glViewport( 0, 0, ( GLsizei ) w, ( GLsizei ) h );
}

void OpenGLShader::AddShader( GLenum shaderType, std::string strFileName )
{
	_shaderList.push_back( Framework::LoadShader( shaderType, strFileName ) );
}

void OpenGLShader::CreatePrograme()
{
	_strctShaderPropoties.ProgramID = Framework::CreateProgram( _shaderList );

	_strctShaderPropoties.ModelToCameraMatrixUnif = glGetUniformLocation( _strctShaderPropoties.ProgramID, "modelToCameraMatrix" );
	_strctShaderPropoties.NormalModelToCameraMatrixUnif = glGetUniformLocation( _strctShaderPropoties.ProgramID, "normalModelToCameraMatrix" );
	_strctShaderPropoties.TextureSamplerUnif = glGetUniformLocation( _strctShaderPropoties.ProgramID, "textureSampler" );
	_strctShaderPropoties.ColorID = glGetUniformLocation( _strctShaderPropoties.ProgramID, "colorID" );

	GLuint projectionBlock = glGetUniformBlockIndex( _strctShaderPropoties.ProgramID, "Projection" );
	glUniformBlockBinding( _strctShaderPropoties.ProgramID, projectionBlock, OpenGLShader::PROJECTION_BLOCK_INDEX );

	GLuint materialBlock = glGetUniformBlockIndex( _strctShaderPropoties.ProgramID, "Material" );
	glUniformBlockBinding( _strctShaderPropoties.ProgramID, materialBlock, OpenGLShader::MATERIAL_BLOCK_INDEX );

	GLuint lightBlock = glGetUniformBlockIndex( _strctShaderPropoties.ProgramID, "Light" );
	glUniformBlockBinding( _strctShaderPropoties.ProgramID, lightBlock, OpenGLShader::LIGHT_BLOCK_INDEX );
}

const glm::mat4 OpenGLShader::GetCameraToClipMatrix()
{
	return _cameraToClipMatrix;
}

const OpenGLShaderPropoties OpenGLShader::GetShaderPropoties()
{
	return _strctShaderPropoties;
}