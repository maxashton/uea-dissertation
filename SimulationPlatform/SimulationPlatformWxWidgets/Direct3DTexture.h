#ifdef _WIN32
#pragma once

#include "Texture.h"
#include <d3dx11.h>

class Direct3DTexture :	public Texture
{

public:

	Direct3DTexture();
	virtual ~Direct3DTexture();

	virtual void BindTexture();
	virtual bool LoadTexture2D( std::string sPath );
	virtual	Texture* Clone();

protected:

	ID3D11ShaderResourceView* _pTexture;

};
#endif
