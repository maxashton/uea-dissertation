#pragma once

#include "Common.h"
#include "Transform.h"
#include "Material.h"
#include "Component.h"
#include "Renderer.h"
#include "RenderFactory.h"

class Component;

class GameObject
{

public:

	GameObject();
	GameObject( RenderFactory factory );
	virtual ~GameObject();

	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual void DrawPicking( glm::mat4 mCameraMatrix );

	Material* GetMaterial();
	boost::shared_ptr<Transform> GetTransform();

	glm::mat4 GetModelMatrix();
	bool GetModelMatrixOverride();

	void SetModelMatrix( glm::mat4 modelMatrix );
	void AddComponenet( Component* pComponenet );
	void RemoveComponenet( Component* pComponenet );
	void Update();

	virtual void ConnectRenderer( Renderer* pRenderer );

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );

	virtual int GetNumberOfVertesies();

	void SetPickingColor( glm::vec4 color );
	glm::vec4 GetPickingColor();

	void SetDrawComponenets( bool toDraw );

	Component* FindCollider();

	void SetName( std::string name );
	std::string GetName();
	std::vector<Component*>* GetComponenets();

	void SetModelViewMatrixOverride( bool val );
		
protected:

	RenderFactory _renderfactory;

	std::vector<Component*> _vComponenetList;
	boost::shared_ptr<Transform> _pTransform;
	Material* _pMaterial;

	glm::mat4 _modelMatrix;
	glm::vec4 _pickingColor;

	bool _modelMatrixOverride;
	bool _drawComponents;

	Renderer* _pRenderer;

	std::string _name;

};

