#version 330

layout(location = 0) in vec4 position;

smooth out vec4 theColor;

uniform mat4 modelToCameraMatrix;

uniform Projection
{
	mat4 cameraToClipMatrix;
};

void main()
{
	vec4 tempCamPosition = (modelToCameraMatrix * position);
	gl_Position = cameraToClipMatrix * tempCamPosition;
	
	theColor = vec4( 1.0, 1.0, 1.0, 1.0 );
}
