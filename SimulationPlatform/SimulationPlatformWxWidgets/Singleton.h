#pragma once

#include "Common.h"

template<class T> class Singleton
{
	Singleton( const Singleton& );
	Singleton& operator=( const Singleton& );

protected:

	Singleton() {}
	virtual ~Singleton() {}

public:

	static T& Instance()
	{
		static T instance;
		return instance;
	}

}; 