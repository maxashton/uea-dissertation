#pragma once

#include "Common.h"
#include "Camera.h"
#include "OpenGLCommon.h"

class FlyCam : public Camera
{

public:

	FlyCam( glutil::ViewData initialViewData, glutil::ViewScale viewScale );
	virtual ~FlyCam();
	
	void HandleKeyPress( unsigned char key );
	void HandleMouseMotion( int x, int y );
	void HandleMouseButton( int button, int state, int x, int y );
	void HandelMouseWheel( int wheel, int direction, int x, int y );

	virtual glm::mat4 GetViewMatrix();
	virtual glm::vec3 GetForwardVector();
	virtual glm::vec3 GetCurrentPosition();

protected:

	int CalcGlutModifiers();

	glutil::ViewData _initialViewData;
	glutil::ViewScale _viewScale;

	glutil::ViewPole* _pViewPole;
};

