#pragma once

#include "Common.h"
#include "RigidBody.h"
#include <boost/thread.hpp>

class PhysicsEngine
{

public:

	PhysicsEngine();
	~PhysicsEngine();

	virtual void Setup();
	virtual void Destroy();
	virtual void StartAsync();
	virtual void Pause();
	virtual void Run();
	virtual void AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody );

	float GetLastTimeStep();

	void SetInitialStep( bool initialStep );

protected:

	bool _runSimulation;
	bool _physicsInitialSet;
	bool _physicsRunning = false;

	float _physicsStep;

	boost::shared_ptr<std::vector<boost::shared_ptr<RigidBody> >> _pRigidBodies;

	boost::posix_time::ptime _lastPhysicsTime;
	boost::thread* _pPhysicsThread;

	virtual void UpdatePhysics();

};

