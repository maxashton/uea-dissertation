#include "OpenGLRenderer.h"
#include "OpenGLTexture.h"

#include <assimp/postprocess.h>

OpenGLRenderer::OpenGLRenderer()
{
	_pShader = new OpenGLShader();
	_pPickingShader = new OpenGLShader();
}

OpenGLRenderer::~OpenGLRenderer()
{
	if ( _pShader ) delete _pShader;
	if ( _pPickingShader ) delete _pPickingShader;
}

void OpenGLRenderer::DrawIcoSphere( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );
	OpenGLShaderPropoties strctShaderProps = _pShader->GetShaderPropoties();

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	glUseProgram( strctShaderProps.ProgramID );
	{
		if ( modelMatrixOverride )
		{
			msModelSpaceMatrix.ApplyMatrix( modelMatrix );
			msModelSpaceMatrix.Scale( pTransform->pScale->x + 0.01f );
		}
		else
		{
			msModelSpaceMatrix.Translate( *pTransform->pPosition );
			msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
			msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
		}

		glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( msModelSpaceMatrix.Top() ) );

		glBindVertexArray( _vao );

		glDrawElements( GL_TRIANGLES, sizeof( int ) * numOfIndicies, GL_UNSIGNED_SHORT, 0 );

		glBindVertexArray( 0 );

	}
	glUseProgram( 0 );

	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void OpenGLRenderer::DrawGeoPlane( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies )
{
	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );
	OpenGLShaderPropoties strctShaderProps = _pShader->GetShaderPropoties();

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

	glUseProgram( strctShaderProps.ProgramID );
	{
		if ( modelMatrixOverride )
		{
			msModelSpaceMatrix.ApplyMatrix( modelMatrix );
			msModelSpaceMatrix.RotateX( -90.0f );
			msModelSpaceMatrix.Scale( pTransform->pScale->x + 0.01f, pTransform->pScale->y + 0.01f, pTransform->pScale->z + 0.01f );

		}
		else
		{
			msModelSpaceMatrix.Translate( *pTransform->pPosition );
			msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
			msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
		}

		glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( msModelSpaceMatrix.Top() ) );

		glBindVertexArray( _vao );

		glDrawElements( GL_TRIANGLES, sizeof( int ) * numOfIndicies, GL_UNSIGNED_SHORT, 0 );

		glBindVertexArray( 0 );
	}
	glUseProgram( 0 );

	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void OpenGLRenderer::DrawAssimpGameObject( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform,
											Material* pMatrial, int iNumMeshes, std::vector<int> materialIndices, std::vector<Texture*>* textures, 
											std::vector<int> meshStartIndices, std::vector<int> meshSizes )
{
	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );
	OpenGLShaderPropoties strctShaderProps = _pShader->GetShaderPropoties();

	glUseProgram( strctShaderProps.ProgramID );
	{
		//set uniform material data
		glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::MATERIAL_UNIFORM_BUFFER );
		glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( Material ), &pMatrial->GetData() );
		glBindBuffer( GL_UNIFORM_BUFFER, 0 );

		glUniform1i( strctShaderProps.TextureSamplerUnif, 0 );
		{
			if ( modelMatrixOverride )
			{
				msModelSpaceMatrix.ApplyMatrix( modelMatrix );
				msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
			}
			else
			{
				msModelSpaceMatrix.Translate( *pTransform->pPosition );
				msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
				msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
			}

			glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( msModelSpaceMatrix.Top() ) );

			glm::mat3 normMatrix( msModelSpaceMatrix.Top() );
			normMatrix = glm::transpose( glm::inverse( normMatrix ) );
			glUniformMatrix3fv( _pShader->GetShaderPropoties().NormalModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( normMatrix ) );

			//_pModel->BindModelsVAO();
			glBindVertexArray( _vao );
			glBindBuffer( GL_ARRAY_BUFFER, _vertexBufferObject );

			// Vertex positions
			glEnableVertexAttribArray( 0 );
			glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), 0 );

			// Texture coordinates
			glEnableVertexAttribArray( 1 );
			glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )sizeof( aiVector3D ) );

			// Normal vectors
			glEnableVertexAttribArray( 2 );
			glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )( sizeof( aiVector3D ) + sizeof( aiVector2D ) ) );

			for ( int i = 0; i < iNumMeshes; i++ )
			{
				int iMatIndex = materialIndices[ i ];
				if ( ( *textures ).size() > iMatIndex )
				{
					( *textures ) [iMatIndex]->BindTexture();
				}
				else
				{
					pMatrial->SetDiffuseColor(  0.9f, 0.0f, 0.5f, 1.0f );
					glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( Material ), &pMatrial->GetData() );
				}

				glDrawArrays( GL_TRIANGLES, meshStartIndices[ i ], meshSizes[ i ] );
			}

			glBindTexture( GL_TEXTURE_2D, 0 );

			glDisableVertexAttribArray( 0 );
			glDisableVertexAttribArray( 1 );
			glDisableVertexAttribArray( 2 );
		}
	}

	glUseProgram( 0 );
}

void OpenGLRenderer::DrawAssimpGameObjectPicking( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int iNumMeshes, 
													std::vector<int> materialIndices, std::vector<int> meshStartIndices, std::vector<int> meshSizes, glm::vec4 color )
{
	glutil::MatrixStack msModelSpaceMatrix = glutil::MatrixStack( mCameraMatrix );
	OpenGLShaderPropoties strctShaderProps = _pPickingShader->GetShaderPropoties();

	glUseProgram( strctShaderProps.ProgramID );
	{
		//set uniform material data
		glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::MATERIAL_UNIFORM_BUFFER );

		//create picking material
		Material pickMat;
		pickMat.SetDiffuseColor( color.r, color.g, color.b, color.a );

		glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( Material ), &pickMat.GetData() );
		glBindBuffer( GL_UNIFORM_BUFFER, 0 );

			if ( modelMatrixOverride )
			{
				msModelSpaceMatrix.ApplyMatrix( modelMatrix );
				msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
			}
			else
			{
				msModelSpaceMatrix.Translate( *pTransform->pPosition );
				msModelSpaceMatrix.ApplyMatrix( glm::mat4_cast( *pTransform->pOrientation ) );
				msModelSpaceMatrix.Scale( pTransform->pScale->x, pTransform->pScale->y, pTransform->pScale->z );
			}

			glUniformMatrix4fv( _pShader->GetShaderPropoties().ModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( msModelSpaceMatrix.Top() ) );

			glm::mat3 normMatrix( msModelSpaceMatrix.Top() );
			normMatrix = glm::transpose( glm::inverse( normMatrix ) );
			glUniformMatrix3fv( _pShader->GetShaderPropoties().NormalModelToCameraMatrixUnif, 1, GL_FALSE, glm::value_ptr( normMatrix ) );

			//_pModel->BindModelsVAO();
			glBindVertexArray( _vao );
			glBindBuffer( GL_ARRAY_BUFFER, _vertexBufferObject );

			// Vertex positions
			glEnableVertexAttribArray( 0 );
			glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), 0 );

			// Texture coordinates
			glEnableVertexAttribArray( 1 );
			glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )sizeof( aiVector3D ) );

			// Normal vectors
			glEnableVertexAttribArray( 2 );
			glVertexAttribPointer( 2, 3, GL_FLOAT, GL_FALSE, 2 * sizeof( aiVector3D ) + sizeof( aiVector2D ), ( void* )( sizeof( aiVector3D ) + sizeof( aiVector2D ) ) );

			for ( int i = 0; i < iNumMeshes; i++ )
			{
				int iMatIndex = materialIndices [i];
				glDrawArrays( GL_TRIANGLES, meshStartIndices [i], meshSizes [i] );
			}

			glDisableVertexAttribArray( 0 );
			glDisableVertexAttribArray( 1 );
			glDisableVertexAttribArray( 2 );
	}

	glUseProgram( 0 );
}

void OpenGLRenderer::BuildAssimpGameObjectDiffuseShader()
{
	_pShader->AddShader( GL_VERTEX_SHADER, "Shaders\\AssimpGameObject\\default.vert" );
	_pShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\AssimpGameObject\\DiffusedAmbientGammaTexture.frag" );

	_pShader->CreatePrograme();

	_pPickingShader->AddShader( GL_VERTEX_SHADER, "Shaders\\AssimpGameObject\\default.vert" );
	_pPickingShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\AssimpGameObject\\picking.frag" );

	_pPickingShader->CreatePrograme();

}

void OpenGLRenderer::BuildAssimGameObjectSpecularShader()
{
	_pShader->AddShader( GL_VERTEX_SHADER, "Shaders\\AssimpGameObject\\default.vert" );
	_pShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\AssimpGameObject\\SpecularDiffusedAmbientGammaTexture.frag" );

	_pShader->CreatePrograme();
}

void OpenGLRenderer::InitializeAssimpModelVertexBuffer( std::vector<BYTE> modelData )
{
	glGenBuffers( 1, &_vertexBufferObject );

	glGenVertexArrays( 1, &_vao );
	glBindVertexArray( _vao );
	glBindBuffer( GL_ARRAY_BUFFER, _vertexBufferObject );

	glBufferData( GL_ARRAY_BUFFER, modelData.size(), &modelData[0], GL_STATIC_DRAW );

	glBindVertexArray( 0 );
}

void OpenGLRenderer::BuildGeometricSpecularShader()
{
	_pShader->AddShader( GL_VERTEX_SHADER, "Shaders\\Geometric\\MatrixPerspective.vert" );
	_pShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\Geometric\\StandardColors.frag" );

	_pShader->CreatePrograme();
}

void OpenGLRenderer::BuildGeometricDiffuseShader()
{
	_pShader->AddShader( GL_VERTEX_SHADER, "Shaders\\Geometric\\MatrixPerspective.vert" );
	_pShader->AddShader( GL_FRAGMENT_SHADER, "Shaders\\Geometric\\StandardColors.frag" );

	_pShader->CreatePrograme();
}

void OpenGLRenderer::InitializeGeometricVertexBuffer( std::vector<short> indicies, std::vector<glm::vec3> geomtery )
{
	glGenBuffers( 1, &_vertexBufferObject );

	glBindBuffer( GL_ARRAY_BUFFER, _vertexBufferObject );
	glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec3 ) * geomtery.size(), &geomtery [0], GL_STATIC_DRAW );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	glGenBuffers( 1, &_indexBufferObject );

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, _indexBufferObject );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( GLshort ) * indicies.size(), &indicies [0], GL_STATIC_DRAW );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	glGenVertexArrays( 1, &_vao );
	glBindVertexArray( _vao );

	glBindBuffer( GL_ARRAY_BUFFER, _vertexBufferObject );
	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, _indexBufferObject );

	glBindVertexArray( 0 );
}

void OpenGLRenderer::UpdateLightingBuffers( LightBlock* lightData )
{
	glBindBuffer( GL_UNIFORM_BUFFER, OpenGLShader::LIGHT_UNIFORM_BUFFER );
	glBufferSubData( GL_UNIFORM_BUFFER, 0, sizeof( LightBlock ), &*lightData );
	glBindBuffer( GL_UNIFORM_BUFFER, 0 );
}

void OpenGLRenderer::Clear()
{
	glClearColor( 0.5f, 0.74f, 1.0f, 0.9f );
	glClearDepth( 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

Texture* OpenGLRenderer::LoadTexture( std::string path )
{
	OpenGLTexture* pNew = new OpenGLTexture();
	pNew->LoadTexture2D( path );

	return pNew;
}