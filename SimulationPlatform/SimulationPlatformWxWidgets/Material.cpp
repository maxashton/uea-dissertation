#include "Material.h"

Material::Material()
{
	this->_materialBlock._vDiffuseColor.r = 1.0f;
	this->_materialBlock._vDiffuseColor.g = 1.0f;
	this->_materialBlock._vDiffuseColor.b = 1.0f;
	this->_materialBlock._vDiffuseColor.a = 1.0f;

	this->_materialBlock._vSpecularColor.r = 1.0f;
	this->_materialBlock._vSpecularColor.g = 1.0f;
	this->_materialBlock._vSpecularColor.b = 1.0f;
	this->_materialBlock._vSpecularColor.a = 1.0f;

	this->_materialBlock._fSpecularShininess = 1.0f;
}

Material::~Material()
{

}

void Material::SetDiffuseColor( float r, float g, float b, float a )
{
	this->_materialBlock._vDiffuseColor.r = r;
	this->_materialBlock._vDiffuseColor.g = g;
	this->_materialBlock._vDiffuseColor.b = b;
	this->_materialBlock._vDiffuseColor.a = a;;
}

void Material::SetSpecularColor( float r, float g, float b, float a )
{
	this->_materialBlock._vSpecularColor.r = r;
	this->_materialBlock._vSpecularColor.g = g;
	this->_materialBlock._vSpecularColor.b = b;
	this->_materialBlock._vSpecularColor.a = a;
}

void Material::SetSpecularShininess( float val )
{
	this->_materialBlock._fSpecularShininess = val;
}

glm::vec4 Material::GetDiffuseColor()
{
	return _materialBlock._vDiffuseColor;
}

glm::vec4 Material::GetSpecularColor()
{
	return _materialBlock._vSpecularColor;
}

float Material::GetSpecularShininess()
{
	return _materialBlock._fSpecularShininess;
}

MaterialBlock Material::GetData()
{
	return _materialBlock;
}