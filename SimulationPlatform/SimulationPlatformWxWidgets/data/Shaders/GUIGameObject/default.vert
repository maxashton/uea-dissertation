#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 inColorID;

out vec2 UV;
out vec3 colorID;

uniform mat4 modelToCameraMatrix;
uniform mat3 normalModelToCameraMatrix;

uniform Projection
{
	mat4 cameraToClipMatrix;
	mat4 orthoMatrix;
};

void main()
{
	vec4 tempCamPosition = (modelToCameraMatrix * vec4(position, 1.0));
	gl_Position = orthoMatrix * tempCamPosition;

	UV = vertexUV;
	colorID = inColorID;
}
