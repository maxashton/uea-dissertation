#include "PhysxPhysicsEngine.h"
#include "NvidiaPhysxRigidBody.h"

PxPhysics* PhysxPhysicsEngine::gPhysicsSDK = NULL;			//Instance of PhysX SDK
PxFoundation*			PhysxPhysicsEngine::gFoundation = NULL;			//Instance of singleton foundation SDK class
PxDefaultAllocator		PhysxPhysicsEngine::gDefaultAllocatorCallback;	//Instance of default implementation of the allocator interface required by the SDK

static PxDefaultErrorCallback	gDefaultErrorCallback;		//Instance of default implementation of the error callback


PhysxPhysicsEngine::PhysxPhysicsEngine()
{
}


PhysxPhysicsEngine::~PhysxPhysicsEngine()
{
}

void PhysxPhysicsEngine::Setup()
{
	//Creating foundation for PhysX
	gFoundation = PxCreateFoundation( PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback );

	//Creating instance of PhysX SDK
	gPhysicsSDK = PxCreatePhysics( PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale() );

	if ( gPhysicsSDK == NULL )
	{
		exit( 1 );
	}


	//Creating scene
	PxSceneDesc sceneDesc( gPhysicsSDK->getTolerancesScale() );	//Descriptor class for scenes 

	sceneDesc.gravity = PxVec3( 0.0f, -9.8f, 0.0f );		//Setting gravity
	sceneDesc.cpuDispatcher = PxDefaultCpuDispatcherCreate( 1 );	//Creates default CPU dispatcher for the scene
	sceneDesc.filterShader = PxDefaultSimulationFilterShader;	//Creates default collision filter shader for the scene

	gScene = gPhysicsSDK->createScene( sceneDesc );				//Creates a scene 
}

void PhysxPhysicsEngine::Destroy()
{
	_physicsRunning = false;

	PhysicsEngine::Destroy();

	gPhysicsSDK->release();			//Removes any actors,  particle systems, and constraint shaders from this scene
	gFoundation->release();			//Destroys the instance of foundation SDK
}

void PhysxPhysicsEngine::StartAsync()
{
	_pPhysicsThread = new boost::thread( boost::bind( &PhysxPhysicsEngine::UpdatePhysics, this ) );
}

void PhysxPhysicsEngine::AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody )
{

	PhysicsEngine::AddRigidBody( pRigidBody );

	boost::shared_ptr<NvidiaPhysxRigidBody> castedBody = boost::static_pointer_cast< NvidiaPhysxRigidBody >( pRigidBody );

	PxActor* pBody = castedBody->GetRigidBody();

	gScene->addActor( *pBody );																		//Adding plane actor to PhysX scene
}

void PhysxPhysicsEngine::UpdatePhysics()
{
	while ( _physicsRunning )
	{
		try
		{
			if ( _physicsInitialSet )
			{
				_physicsInitialSet = false;
				if ( gScene != NULL )
				{
					gScene->simulate( 1.0f );	//Advances the simulation by 'gTimeStep' time
					gScene->fetchResults( true );		//Block until the simulation run is completed
				}
			}
			else
			{
				boost::posix_time::ptime timeNow = boost::posix_time::microsec_clock::local_time();
				boost::posix_time::time_duration diff = timeNow - _lastPhysicsTime;

				if ( gScene != NULL )
				{
					//if simulation not yet started
					if ( _runSimulation )
					{
						_physicsStep = ( float )( 0.000001f * diff.total_microseconds() );
						gScene->simulate( _physicsStep );	//Advances the simulation by 'gTimeStep' time
						gScene->fetchResults( true );		//Block until the simulation run is completed
					}
				}

				_lastPhysicsTime = timeNow;
			}

			boost::this_thread::sleep_for( boost::chrono::milliseconds( 1 ) );
		}
		catch ( boost::thread_interrupted& )
		{
			_physicsRunning = false;
		}
	}
}
