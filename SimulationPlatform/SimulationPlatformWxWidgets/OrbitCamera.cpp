#include "OrbitCamera.h"
#include "Framework.h"

OrbitCamera::OrbitCamera( GameObject* pOrbitObject ) : Camera()
{
	_fOrbitDistance = 1.0f;
	_pObjectToOrbit = pOrbitObject;
}


OrbitCamera::~OrbitCamera()
{
}

void OrbitCamera::ChangeOrientation( float x, float y, float z )
{
	_v3Rotation.x = _v3Rotation.x + x;
	_v3Rotation.y = _v3Rotation.y + y;
	_v3Rotation.z = _v3Rotation.z + z;

	if ( _v3Rotation.x > 0 )
	{
		_v3Rotation.x = 0;
	}

	if ( _v3Rotation.y > 360 )
	{
		_v3Rotation.y = _v3Rotation.y - 360.0f;
	}

	if ( _v3Rotation.z > 360 )
	{
		_v3Rotation.z = _v3Rotation.z - 360.0f;
	}

	if ( _v3Rotation.x < -55 )
	{
		_v3Rotation.x = -55;
	}

	if ( _v3Rotation.y < 0 )
	{
		_v3Rotation.y = 360.0f + _v3Rotation.y;
	}

	if ( _v3Rotation.z < 0 )
	{
		_v3Rotation.z = 360.0f + _v3Rotation.z;
	}

	_qOrbitOrientation = glm::quat( glm::vec3( Framework::DegToRad( _v3Rotation.x ), Framework::DegToRad( _v3Rotation.y ), Framework::DegToRad( _v3Rotation.z ) ) );
}

void OrbitCamera::SetOrbitDistance( float fDistance )
{
	_fOrbitDistance = fDistance;
}

glm::vec3 OrbitCamera::GetDirectionVector()
{
	glm::vec3 vForward = glm::vec3( 0, 0, 0 );

	vForward = _qOrbitOrientation * glm::vec3( 0, 0, -1.0 );

	return glm::normalize( vForward );
}

glm::mat4x4 OrbitCamera::GetViewMatrix()
{
	glm::vec3 vForward = GetDirectionVector();
	vForward = vForward * _fOrbitDistance;

	glm::vec3 v3ObjPos = *_pObjectToOrbit->GetTransform()->pPosition;
	glm::vec3 v3Centre =  v3ObjPos + vForward;

	return glm::lookAt( v3Centre, v3ObjPos, glm::vec3( 0.0f, 1.0f, 0.0f ) );
}

void OrbitCamera::ChangeOrbitDistance( float fDistance )
{
	_fOrbitDistance += fDistance;

	if ( _fOrbitDistance > -60 )
	{
		_fOrbitDistance = -60;
	}

	if ( _fOrbitDistance < -300 )
	{
		_fOrbitDistance = -300;
	}
}
