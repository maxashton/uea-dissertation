#pragma once
#include "Common.h"
#include "Renderer.h"

class RenderFactory
{

public:

	RenderFactory();
	virtual ~RenderFactory();

	static void SetRenderType( int renderType );
	
	static Renderer* CreateRenderer();

protected: 

	static int _renderType;
};

