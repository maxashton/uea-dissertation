#pragma once

#include <wx/wx.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>

#include "LightManager.h"
#include "Direct3DShader.h"
#include "DirectXRenderer.h"
#include "Structs.h"
#include "Texture.h"
#include "Engine.h"

class D3D11Canvas : public wxWindow
{

public:
	D3D11Canvas( wxWindow *parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0,
					const wxString& name = wxT( "TestGLCanvas" ) );

	~D3D11Canvas();

	int GetWidth();
	int GetHeight();
	void Initialize( int physicsType );

	Engine* GetEngine();

protected:

	void OnPaint( wxPaintEvent& event );
	void OnSize( wxSizeEvent& event );
	void OnEraseBackground( wxEraseEvent& event );
	void OnIdle( wxIdleEvent& event );

	void InitD3D();
	void Render();
	// events
	void mouseMoved( wxMouseEvent& event );
	void mouseDown( wxMouseEvent& event );
	void mouseWheelMoved( wxMouseEvent& event );
	void mouseReleased( wxMouseEvent& event );
	void rightClick( wxMouseEvent& event );
	void mouseLeftWindow( wxMouseEvent& event );
	void keyPressed( wxKeyEvent& event );
	void keyReleased( wxKeyEvent& event );
	void InitPipeline( void );

	bool m_init;

	HWND _hWnd;

	DirectXRenderer _renderer;
	std::vector<Texture*> _textures;

	bool _initialized;
	Engine _engine;

	DECLARE_EVENT_TABLE()
};

