#include "Framework.h"
#include <fstream>
#include <sstream>
#include <boost/math/special_functions/round.hpp>

namespace Framework
{
	GLuint LoadShader( GLenum eShaderType, const std::string &strShaderFilename )
	{
		std::string strFilename = FindFileOrThrow( strShaderFilename );
		std::ifstream shaderFile( strFilename.c_str() );
		std::stringstream shaderData;
		shaderData << shaderFile.rdbuf();
		shaderFile.close();

		return FCreateShader( eShaderType, shaderData.str() );
	}

	GLuint CreateProgram( const std::vector<GLuint> &shaderList )
	{
		return FCreateProgram( shaderList );
	}

	float RandomFloat( float a, float b )
	{
		return ( ( b - a )*( ( float )rand() / RAND_MAX ) ) + a;
	}

	float RandomFloatToDP( float a, float b, int dp )
	{
		float random =  ( ( b - a )*( ( float )rand() / RAND_MAX ) ) + a;

		float randomDP = floorf( random * 100 ) / 100;

		//random *= 100;
		//random += 0.5;
		//random = ( float )( ( int )random );
		//random /= 100;

		return randomDP;
	}

	float DegToRad( float fAngDeg )
	{
		const float fDegToRad = 3.14159f * 2.0f / 360.0f;
		return fAngDeg * fDegToRad;
	}

	std::string FindFileOrThrow( const std::string &strFilename )
	{
		std::string data = "data\\" + strFilename;
		std::ifstream testFile( data.c_str() );
		if ( testFile.is_open() )
			return data;

		throw std::runtime_error( "Could not find the file " + strFilename );
	}

	glm::mat4 CreateStandardViewMatrix( float fFrustumScale, float fzFar, float fzNear )
	{
		glm::mat4 cameraToClipMatrix;
		cameraToClipMatrix[ 0 ].x = fFrustumScale;
		cameraToClipMatrix[ 1 ].y = fFrustumScale;
		cameraToClipMatrix[ 2 ].z = ( fzFar + fzNear ) / ( fzNear - fzFar );
		cameraToClipMatrix[ 2 ].w = -1.0f;
		cameraToClipMatrix[ 3 ].z = ( 2 * fzFar * fzNear ) / ( fzNear - fzFar );

		return cameraToClipMatrix;
	}
	
	float CalcFrustumScale( float fFovDeg )
	{
		const float degToRad = 3.14159f * 2.0f / 360.0f;
		float fFovRad = fFovDeg * degToRad;
		return 1.0f / tan( fFovRad / 2.0f );
	}

	GLuint FCreateShader( GLenum eShaderType, const std::string &strShaderFile )
	{
		GLuint shader = glCreateShader( eShaderType );
		const char *strFileData = strShaderFile.c_str();
		glShaderSource( shader, 1, &strFileData, NULL );

		glCompileShader( shader );

		GLint status;
		glGetShaderiv( shader, GL_COMPILE_STATUS, &status );
		if ( status == GL_FALSE )
		{
			GLint infoLogLength;
			glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &infoLogLength );

			GLchar *strInfoLog = new GLchar [infoLogLength + 1];
			glGetShaderInfoLog( shader, infoLogLength, NULL, strInfoLog );

			const char *strShaderType = NULL;
			switch ( eShaderType )
			{
			case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
			case GL_GEOMETRY_SHADER: strShaderType = "geometry"; break;
			case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
			}

			fprintf( stderr, "Compile failure in %s shader:\n%s\n", strShaderType, strInfoLog );
			delete [] strInfoLog;
		}

		return shader;
	}

	GLuint FCreateProgram( const std::vector<GLuint> &shaderList )
	{
		GLuint program = glCreateProgram();

		for ( size_t iLoop = 0; iLoop < shaderList.size(); iLoop++ )
			glAttachShader( program, shaderList [iLoop] );

		glLinkProgram( program );

		GLint status;
		glGetProgramiv( program, GL_LINK_STATUS, &status );
		if ( status == GL_FALSE )
		{
			GLint infoLogLength;
			glGetProgramiv( program, GL_INFO_LOG_LENGTH, &infoLogLength );

			GLchar *strInfoLog = new GLchar [infoLogLength + 1];
			glGetProgramInfoLog( program, infoLogLength, NULL, strInfoLog );
			fprintf( stderr, "Linker failure: %s\n", strInfoLog );
			delete [] strInfoLog;
		}

		for ( size_t iLoop = 0; iLoop < shaderList.size(); iLoop++ )
			glDetachShader( program, shaderList [iLoop] );

		return program;
	}

	unsigned int defaults( unsigned int displayMode, int &width, int &height ) { return displayMode; }

	void APIENTRY DebugFunc( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, GLvoid* userParam )
	{
		std::string srcName;
		switch ( source )
		{
		case GL_DEBUG_SOURCE_API_ARB: srcName = "API"; break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: srcName = "Window System"; break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: srcName = "Shader Compiler"; break;
		case GL_DEBUG_SOURCE_THIRD_PARTY_ARB: srcName = "Third Party"; break;
		case GL_DEBUG_SOURCE_APPLICATION_ARB: srcName = "Application"; break;
		case GL_DEBUG_SOURCE_OTHER_ARB: srcName = "Other"; break;
		}

		std::string errorType;
		switch ( type )
		{
		case GL_DEBUG_TYPE_ERROR_ARB: errorType = "Error"; break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: errorType = "Deprecated Functionality"; break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB: errorType = "Undefined Behavior"; break;
		case GL_DEBUG_TYPE_PORTABILITY_ARB: errorType = "Portability"; break;
		case GL_DEBUG_TYPE_PERFORMANCE_ARB: errorType = "Performance"; break;
		case GL_DEBUG_TYPE_OTHER_ARB: errorType = "Other"; break;
		}

		std::string typeSeverity;
		switch ( severity )
		{
		case GL_DEBUG_SEVERITY_HIGH_ARB: typeSeverity = "High"; break;
		case GL_DEBUG_SEVERITY_MEDIUM_ARB: typeSeverity = "Medium"; break;
		case GL_DEBUG_SEVERITY_LOW_ARB: typeSeverity = "Low"; break;
		}

		printf( "%s from %s,\t%s priority\nMessage: %s\n",
			errorType.c_str(), srcName.c_str(), typeSeverity.c_str(), message );
	}
}

