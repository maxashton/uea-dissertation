#include "BulletPhysicsEngine.h"
#include <boost/thread.hpp>
#include "BulletRigidBody.h"


BulletPhysicsEngine::BulletPhysicsEngine()
{
}


BulletPhysicsEngine::~BulletPhysicsEngine()
{
}

void BulletPhysicsEngine::Setup()
{
	_pCollisionConfig = new btDefaultCollisionConfiguration();
	_pDispatcher = new btCollisionDispatcher( _pCollisionConfig );
	_pBroadphase = new btDbvtBroadphase();
	_pSolver = new btSequentialImpulseConstraintSolver();
	_pWorld = new btDiscreteDynamicsWorld( _pDispatcher, _pBroadphase, _pSolver, _pCollisionConfig );
	_pWorld->setGravity( btVector3( 0, -10, 0 ) );	//gravity on Earth
}

void BulletPhysicsEngine::Destroy()
{
	_physicsRunning = false;

	BOOST_FOREACH( btRigidBody* body, _rigidBodies )
	{
		_pWorld->removeCollisionObject( body );

		btMotionState* motionState = body->getMotionState();
		btCollisionShape* shape = body->getCollisionShape();

		//delete body;
		delete shape;
		delete motionState;
	}


	if ( _pDispatcher ) delete _pDispatcher;
	if ( _pCollisionConfig ) delete _pCollisionConfig;
	if ( _pBroadphase ) delete _pBroadphase;
	if ( _pSolver ) delete _pSolver;

	PhysicsEngine::Destroy();


	//if ( _pWorld ) delete _pWorld;

}

void BulletPhysicsEngine::StartAsync()
{
	_pPhysicsThread = new boost::thread( boost::bind( &BulletPhysicsEngine::UpdatePhysics, this ) );
}

void BulletPhysicsEngine::AddRigidBody( boost::shared_ptr<RigidBody> pRigidBody )
{
	PhysicsEngine::AddRigidBody( pRigidBody );

	boost::shared_ptr<BulletRigidBody> castedBody = boost::static_pointer_cast<BulletRigidBody>( pRigidBody );

	boost::shared_ptr<btRigidBody> pBody = castedBody->GetRigidBody();
	_pWorld->addRigidBody( &*pBody );
}

void BulletPhysicsEngine::UpdatePhysics()
{
	while ( _physicsRunning )
	{
		try
		{
			if ( _physicsInitialSet )
			{
				_physicsInitialSet = false;
				if ( _pWorld != NULL )
				{
					_pWorld->stepSimulation( 1.0f );
				}
			}
			else
			{
				boost::posix_time::ptime timeNow = boost::posix_time::microsec_clock::local_time();
				boost::posix_time::time_duration diff = timeNow - _lastPhysicsTime;

				if ( _pWorld != NULL )
				{
					//if simulation not yet started
					if ( _runSimulation )
					{
						_physicsStep = ( float )( 0.000001f * diff.total_microseconds() );
						_pWorld->stepSimulation( _physicsStep );
					}
				}


				_lastPhysicsTime = timeNow;
			}

			boost::this_thread::sleep_for( boost::chrono::milliseconds( 10 ) );
		}
		catch ( boost::thread_interrupted& )
		{
			_physicsRunning = false;
		}
	}

}