#include "AssimpGameObject.h"
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h> 
#include <assimp/postprocess.h>

AssimpGameObject::AssimpGameObject()
{
	_modelMatrixOverride = false;
	_bLoaded = false;
	_pTextures = new std::vector<Texture*>();
}

AssimpGameObject::AssimpGameObject( AssimpGameObject* otherObject )
{
	this->_modelMatrixOverride = otherObject->_modelMatrixOverride;
	this->_bLoaded = otherObject->_bLoaded;

	//performance
	_pTextures = new std::vector<Texture*>();
	BOOST_FOREACH( Texture* pTexture, *otherObject->_pTextures )
	{
		_pTextures->push_back( pTexture->Clone() );
	}

	this->_iMeshStartIndices = otherObject->_iMeshStartIndices;
	this->_iMeshSizes = otherObject->_iMeshSizes;
	this->_iMaterialIndices = otherObject->_iMaterialIndices;
	this->_modelData = otherObject->_modelData;

	this->_pRenderer = otherObject->_pRenderer;
	this->_filePath = otherObject->_filePath;
	_pRenderer->InitializeAssimpModelVertexBuffer( _modelData );
}

AssimpGameObject::AssimpGameObject( RenderFactory factory )
{
	_renderfactory = factory;
}

AssimpGameObject::~AssimpGameObject()
{
	BOOST_FOREACH( Texture* pTexture, ( *_pTextures ) )
	{
		if ( pTexture )	delete pTexture;
	}

	delete _pTextures;
}

void AssimpGameObject::Draw( glm::mat4 mCameraMatrix )
{
	GameObject::Draw( mCameraMatrix );

	_pRenderer->DrawAssimpGameObject( mCameraMatrix, _modelMatrixOverride, _modelMatrix, _pTransform, _pMaterial, ( int ) _iMeshSizes.size(), _iMaterialIndices,
										_pTextures, _iMeshStartIndices, _iMeshSizes );
}

void AssimpGameObject::DrawPicking( glm::mat4 mCameraMatrix )
{
	_pRenderer->DrawAssimpGameObjectPicking( mCameraMatrix, _modelMatrixOverride, _modelMatrix, _pTransform, ( int )_iMeshSizes.size(), _iMaterialIndices,
												_iMeshStartIndices, _iMeshSizes, this->_pickingColor );
}

void AssimpGameObject::ConnectRenderer( Renderer* pRenderer )
{
	GameObject::ConnectRenderer( pRenderer );
	_pRenderer->BuildAssimpGameObjectDiffuseShader();
}

void AssimpGameObject::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "Type", "AssimpGameObject" );
	}

	GameObject::SerializeToXLM( pInTree, false );

	ptree & transformNode = pInTree->add( "FilePath", _filePath );
}

void AssimpGameObject::DeSerializeFromXML( ptree::value_type const& v )
{
	GameObject::DeSerializeFromXML( v );

	LoadModelFromFile( v.second.get<std::string>( "FilePath" ) );
}

int AssimpGameObject::GetNumberOfVertesies()
{
	int total = 0;

	BOOST_FOREACH( int num, _iMeshSizes )
	{
		total += num;
	}

	return total;
}

bool AssimpGameObject::LoadModelFromFile( std::string filePath )
{
	bool sccess = LoadModelFromFileLogic( filePath.c_str() );

	_pRenderer->InitializeAssimpModelVertexBuffer( _modelData );
	return sccess;
}

std::string GetDirectoryPath( std::string sFilePath )
{
	// Get directory path
	std::string sDirectory = "";

	for ( int i = ( int ) sFilePath.length(); i >= 0; i-- )
	{
		if ( sFilePath [i] == '\\' || sFilePath [i] == '/' )
		{
			sDirectory = sFilePath.substr( 0, i + 1 );
			break;
		}
	}

	return sDirectory;
}


bool AssimpGameObject::LoadModelFromFileLogic( const char* sFilePath )
{
	_filePath = sFilePath;

	_pTextures->reserve( 50 );

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile( sFilePath,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType );

	if ( !scene )
	{
		std::cout << "Failed to load model \n";
		//MessageBox( appMain.hWnd, "Couldn't load model ", "Error Importing Asset", MB_ICONERROR );
		return false;
	}

	const int iVertexTotalSize = sizeof( aiVector3D ) * 2 + sizeof( aiVector2D );

	int iTotalVertices = 0;

	for ( unsigned int i = 0; i < scene->mNumMeshes; i++ )
	{
		aiMesh* mesh = scene->mMeshes [i];
		int iMeshFaces = mesh->mNumFaces;
		_iMaterialIndices.push_back( mesh->mMaterialIndex );
		int iSizeBefore = _dataSize;
		_iMeshStartIndices.push_back( iSizeBefore / iVertexTotalSize );

		for ( int j = 0; j < iMeshFaces; j++ )
		{
			const aiFace& face = mesh->mFaces [j];

			for ( int k = 0; k < 3; k++ )
			{
				aiVector3D pos = mesh->mVertices [face.mIndices [k]];
				aiVector3D uv = mesh->mTextureCoords [0] [face.mIndices [k]];
				aiVector3D normal = mesh->HasNormals() ? mesh->mNormals [face.mIndices [k]] : aiVector3D( 1.0f, 1.0f, 1.0f );

				AddModelData( &pos, sizeof( aiVector3D ) );
				AddModelData( &uv, sizeof( aiVector2D ) );
				AddModelData( &normal, sizeof( aiVector3D ) );
			}
		}

		int iMeshVertices = mesh->mNumVertices;
		iTotalVertices += iMeshVertices;
		_iMeshSizes.push_back( ( _dataSize - iSizeBefore ) / iVertexTotalSize );
	}
	_iNumMaterials = scene->mNumMaterials;

	std::vector<int> materialRemap( _iNumMaterials );

	for ( int i = 0; i < _iNumMaterials; i++ )
	{
		const aiMaterial* material = scene->mMaterials [i];
		int a = 5;
		int texIndex = 0;
		aiString path;  // filename

		if ( material->GetTexture( aiTextureType_DIFFUSE, texIndex, &path ) == AI_SUCCESS )
		{
			std::string sDir = GetDirectoryPath( sFilePath );
			std::string sTextureName = path.data;
			std::string sFullPath = sDir + sTextureName;
			int iTexFound = -1;

			for ( int j = 0; j < ( int )_pTextures->size(); j++ )
			{
				if ( sFullPath == ( *_pTextures ) [j]->GetPath() )
				{
					iTexFound = j;
					break;
				}
			}

			if ( iTexFound != -1 )materialRemap [i] = iTexFound;
			else
			{
				Texture* pNew = _pRenderer->LoadTexture( sFullPath );
				materialRemap [i] = ( int ) _pTextures->size();
				_pTextures->push_back( pNew );
			}
		}
	}

	for ( int i = 0; i < ( int )_iMeshSizes.size(); i++ )
	{
		int iOldIndex = _iMaterialIndices [i];
		_iMaterialIndices [i] = materialRemap [iOldIndex];
	}

	return _bLoaded = true;
}

void AssimpGameObject::AddModelData( void* ptrData, UINT uiDataSize )
{
	_modelData.insert( _modelData.end(), ( BYTE* )ptrData, ( BYTE* )ptrData + uiDataSize );
	_dataSize += uiDataSize;
}
