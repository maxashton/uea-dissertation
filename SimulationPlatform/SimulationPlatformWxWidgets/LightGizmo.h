#pragma once
#include "AssimpGameObject.h"
#include "Light.h"

class LightGizmo :	public AssimpGameObject
{

public:

	LightGizmo( boost::shared_ptr<Light> pLight );
	virtual ~LightGizmo();

	boost::shared_ptr<Light> GetLight();

protected:

	boost::shared_ptr<Light> _pLight;
};

