#pragma once

#include "Common.h"
#include "GameObject.h"
#include "OpenGLCommon.h"

class GameObject;

class Component
{

public:

	Component();
	virtual ~Component();

	virtual void Update();
	virtual void Draw( glm::mat4 mCameraMatrix );
	virtual void AttachGameObject( GameObject* pGameObject );
	void SetToDraw( bool draw );

	virtual void SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType = true );
	virtual void DeSerializeFromXML( ptree::value_type const& v );
	virtual IdentifyType Identify();

protected: 

	GameObject* _pGameObject;
	
	bool _draw;
};

