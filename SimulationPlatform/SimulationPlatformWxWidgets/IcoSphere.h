#pragma once

#include "Common.h"
#include "GameObject.h"
#include "Renderer.h"
#include <assimp/scene.h> 
#include <map>

class IcoSphere : public GameObject
{

public:

	IcoSphere();
	virtual ~IcoSphere();

	void Create( int recursionLevel );
	std::vector<short> GetIndicies();
	std::vector<glm::vec3> GetGeometry();
	int GetNumberOfInicies();

	virtual void ConnectRenderer( Renderer* pRenderer );
	virtual void Draw( glm::mat4 mCameraMatrix );

protected:

	std::vector<glm::vec3> _geometry;
	std::vector<short> _indicies;

	int _index;
	std::map<long, int> _middlePointIndexCache;

	int AddVertex( glm::vec3 p );
	int GetMiddlePoint( int p1, int p2 );
};

class TriangleIndices
{
public: 

	int v1;
	int v2;
	int v3;

	TriangleIndices( int v1, int v2, int v3 );
};