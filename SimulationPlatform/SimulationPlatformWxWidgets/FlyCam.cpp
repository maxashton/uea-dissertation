#include "FlyCam.h"

FlyCam::FlyCam( glutil::ViewData initialViewData, glutil::ViewScale viewScale ) : Camera()
{
	_initialViewData = initialViewData;
	_viewScale = viewScale;

	_pViewPole = new glutil::ViewPole( initialViewData, viewScale, glutil::MB_LEFT_BTN );
}

FlyCam::~FlyCam()
{
	if ( _pViewPole ) delete _pViewPole;
}

void FlyCam::HandleKeyPress( unsigned char key )
{
	_pViewPole->CharPress( key );
}

void FlyCam::HandleMouseButton( int button, int state, int x, int y )
{
	int modifiers = CalcGlutModifiers();

	glm::ivec2 mouseLoc = glm::ivec2( x, y );

	glutil::MouseButtons eButton;

	switch ( button )
	{
	case 0:
		eButton = glutil::MB_LEFT_BTN;
		break;
	case 1:
		eButton = glutil::MB_MIDDLE_BTN;
		break;
	case 2:
		eButton = glutil::MB_RIGHT_BTN;
		break;
#ifdef LOAD_X11
		//Linux Mouse wheel support
	case 3:
	{
		forward.MouseWheel( 1, modifiers, mouseLoc );
		return;
	}
	case 4:
	{
		forward.MouseWheel( -1, modifiers, mouseLoc );
		return;
	}
#endif
	default:
		return;
	}

	_pViewPole->MouseClick( eButton, ( state == 1 ) ? true : false , modifiers, glm::ivec2( x, y ) );
}

void FlyCam::HandelMouseWheel( int wheel, int direction, int x, int y )
{
	_pViewPole->MouseWheel( direction, CalcGlutModifiers(), glm::ivec2( x, y ) );
}

void FlyCam::HandleMouseMotion( int x, int y )
{
	_pViewPole->MouseMove( glm::ivec2( x, y ) );
}

int FlyCam::CalcGlutModifiers()
{
	int ret = 0;

	//int modifiers = glutGetModifiers();
	//if ( modifiers & GLUT_ACTIVE_SHIFT )
	//	ret |= glutil::MM_KEY_SHIFT;
	//if ( modifiers & GLUT_ACTIVE_CTRL )
	//	ret |= glutil::MM_KEY_CTRL;
	//if ( modifiers & GLUT_ACTIVE_ALT )
	//	ret |= glutil::MM_KEY_ALT;

	return ret;
}

glm::mat4 FlyCam::GetViewMatrix()
{
	return _pViewPole->CalcMatrix();
}

glm::vec3 FlyCam::GetForwardVector()
{
	glm::vec4 forward = glm::inverse( _pViewPole->CalcMatrix() ) * glm::vec4( 0, 0, -1.0, 0.0 );

	forward = glm::normalize( forward );

	glm::vec3 re = glm::vec3( forward.x, forward.y, forward.z );

	return re;
}

glm::vec3 FlyCam::GetCurrentPosition()
{
	return _pViewPole->GetView().targetPos;
}
