#pragma once

#include "Common.h"
#include "RigidBody.h"

#define BulletPx 1
#define NvPhysX 2

#define PhysicsType int

class RigidBodyFactory
{

public:

	RigidBodyFactory();
	~RigidBodyFactory();

	static void SetType( PhysicsType etype );
	static boost::shared_ptr<RigidBody> CreatePlaneBody();
	static boost::shared_ptr<RigidBody> CreateSphereBody( float mass, float elasticity, float radious );
	static boost::shared_ptr<RigidBody> CreateBoxBody( float mass, float elasticity, float width, float height, float depth );


protected: 

	static PhysicsType _type;
};

