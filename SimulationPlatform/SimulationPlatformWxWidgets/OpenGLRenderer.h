#pragma once

#include "Renderer.h"
#include "OpenGLShader.h"

class OpenGLRenderer :	public Renderer
{

public:

	OpenGLRenderer();
	virtual ~OpenGLRenderer();

	virtual void DrawIcoSphere( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> transform, int numOfIndicies );
	virtual void DrawGeoPlane( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform, int numOfIndicies );
	virtual void DrawAssimpGameObject( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform,
										Material* pMatrial, int iNumMeshes, std::vector<int> materialIndices, std::vector<Texture*>* textures,
										std::vector<int> meshStartIndices, std::vector<int> meshSizes );
	virtual void DrawAssimpGameObjectPicking( glm::mat4 mCameraMatrix, bool modelMatrixOverride, glm::mat4 modelMatrix, boost::shared_ptr<Transform> pTransform,
											  int iNumMeshes, std::vector<int> materialIndices, std::vector<int> meshStartIndices,
											  std::vector<int> meshSizes, glm::vec4 color );

	virtual void BuildAssimpGameObjectDiffuseShader();
	virtual void BuildAssimGameObjectSpecularShader();
	virtual void InitializeAssimpModelVertexBuffer( std::vector<BYTE> modelData );

	virtual void BuildGeometricSpecularShader();
	virtual void BuildGeometricDiffuseShader();
	virtual void InitializeGeometricVertexBuffer( std::vector<short> indicies, std::vector<glm::vec3> geomtery );

	static void UpdateLightingBuffers( LightBlock* lightData );

	static void Clear();

	Texture* LoadTexture( std::string path );

protected:

	OpenGLShader* _pShader;
	OpenGLShader* _pPickingShader;

	GLuint _vertexBufferObject;
	GLuint _indexBufferObject;
	GLuint _vao;
};

