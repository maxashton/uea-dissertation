#pragma once

#include "Common.h"
#include "BuilderFrame.h"
#include "Engine.h"

class Engine;

class BuilderFrameEx : public BuilderFrame
{

public:

	BuilderFrameEx( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1123, 622 ), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );
	virtual ~BuilderFrameEx();

protected: 

	Engine* _pEngine;
	std::map<wxString, wxString> _filePaths;

	wxWindow* _renderPanel;

	virtual void FormPaint( wxPaintEvent& event );
	virtual void Load_Click( wxCommandEvent& event );
	virtual void Save( wxCommandEvent& event );
	virtual void Exit( wxCommandEvent& event );
	virtual void Create_Create_Obj( wxCommandEvent& event );
	virtual void Create_Add_Click( wxCommandEvent& event );
	virtual void Create_Add_PointLight( wxCommandEvent& event );
	virtual void Prop_Pos_X( wxCommandEvent& event );
	virtual void Prop_Pos_Y( wxCommandEvent& event );
	virtual void Prop_Pos_Z( wxCommandEvent& event );
	virtual void Prop_Ori_X( wxCommandEvent& event );
	virtual void Prop_Ori_Y( wxCommandEvent& event );
	virtual void Prop_Ori_Z( wxCommandEvent& event );
	virtual void Prop_Tint_Color( wxColourPickerEvent& event );
	virtual void Prop_Collider_Type( wxCommandEvent& event );
	virtual void Prop_Col_Width( wxCommandEvent& event );
	virtual void Prop_Col_Height( wxCommandEvent& event );
	virtual void Prop_Col_Depth( wxCommandEvent& event );
	virtual void Prop_Col_Radius( wxCommandEvent& event );
	virtual void Ambient_Color_Change( wxColourPickerEvent& event );
	virtual void MyFilePicker1( wxCommandEvent& event );
	virtual void Ambient_Color_Change( wxCommandEvent& event );
	virtual void Prop_Light_Color( wxColourPickerEvent& event );
	virtual void Scene_List_Change( wxCommandEvent& event );
	virtual void Scene_Delete_Click( wxCommandEvent& event );
	virtual void Mass_Changed( wxCommandEvent& event );
	virtual void Elasticity_Changed( wxCommandEvent& event );

	void SelectionChanged( GameObject* pSelectedObject );

	GameObject* _pSelectedObject;

	void UpdateUIBasedOnSelection();
	void UpdateUITransformInfo();
	void UpdateUIColliderInfo();
	void UpdateLightInfo();
	void SetPlaneColliderInfo( Component* pObjectColider );
	void SetBoxColliderInfo( Component* pObjectColider );
	void SetSphereColliderInfo( Component* pObjectColider );

	void UpdateGameObjectCollider();

	bool _settingUI;
	bool _canvasCreated;

	int _listBoxCounter;
};

