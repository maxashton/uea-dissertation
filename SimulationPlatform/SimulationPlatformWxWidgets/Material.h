#pragma once

#include "Common.h"
#include <glm/glm.hpp>

struct MaterialBlock
{
	glm::vec4 _vDiffuseColor;
	glm::vec4 _vSpecularColor;
	float _fSpecularShininess;
	float padding[ 3 ];
};

class Material
{
public:

	Material();
	virtual ~Material();

	void SetDiffuseColor( float r, float g, float b, float a );
	void SetSpecularColor( float r, float g, float b, float a );
	void SetSpecularShininess( float val );

	glm::vec4 GetDiffuseColor();
	glm::vec4 GetSpecularColor();
	float GetSpecularShininess();

	MaterialBlock GetData();

protected:

	MaterialBlock _materialBlock;

};

