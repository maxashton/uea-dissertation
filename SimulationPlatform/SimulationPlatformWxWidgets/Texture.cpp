#include "Texture.h"

Texture::Texture()
{
}


Texture::~Texture()
{
}

void Texture::CreateEmptyTexture( int iWidth, int iHeight, unsigned int format )
{

}

void Texture::CreateFromData( BYTE* bData, int iWidth, int iHeight, int iBPP, unsigned int format, bool bGenerateMipMaps /*= false */ )
{

}

bool Texture::ReloadTexture()
{
	return false;
}

bool Texture::LoadTexture2D( std::string sPath )
{
	return false;
}

void Texture::BindTexture()
{

}

void Texture::SetSamplerParameter( unsigned int parameter, unsigned int value )
{

}

void Texture::DeleteTexture()
{

}

int Texture::GetWidth()
{
	return _iWidth;
}

int Texture::GetHeight()
{
	return _iHeight;
}

int Texture::GetBPP()
{
	return _iBPP;
}

std::string Texture::GetPath()
{
	return _sPath;
}

Texture* Texture::Clone()
{
	return NULL;
}
