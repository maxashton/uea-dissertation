#include "BulletRigidBody.h"
#include "OpenGLCommon.h"

BulletRigidBody::BulletRigidBody()
{
}


BulletRigidBody::~BulletRigidBody()
{
}

glm::mat4 BulletRigidBody::GetModelMatrix()
{
	btTransform t;
	_pRigidBody->getMotionState()->getWorldTransform( t );
	float mat [16];
	t.getOpenGLMatrix( mat );

	glm::mat4 matrix = glm::make_mat4( mat );

	return matrix;
}

void BulletRigidBody::SetModelMatrix( boost::shared_ptr<glm::vec3> position )
{
	btTransform t;
	t.setIdentity();
	t.setOrigin( btVector3( position->x, position->y, position->z ) );

	if ( _pRigidBody != NULL )
	{
		_pRigidBody->setWorldTransform( t );
	}
}

void BulletRigidBody::SetRigidBody( boost::shared_ptr<btRigidBody> pRigidBody )
{
	_pRigidBody = pRigidBody;
}

boost::shared_ptr<btRigidBody> BulletRigidBody::GetRigidBody()
{
	return _pRigidBody;
}

void BulletRigidBody::SetMass( float val )
{

}

void BulletRigidBody::SetElasticity( float val )
{

}
