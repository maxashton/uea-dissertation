#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>

#include <string>
#include <vector>
#include <map>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <bullet/btBulletDynamicsCommon.h>

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>

using boost::property_tree::ptree;

#define DirectX 0
#define OpenGL 1

//////////////////////////////////////////////////////////////////////////
//	This file includes operating specific paths for OpenGL (GL and GLU)
//	headers, which depending on operating system may have different paths
//////////////////////////////////////////////////////////////////////////
#if defined (_WIN32) || defined(__WIN32__)

////Visual C++ version of gl.h using WINGDIAPI and APIENTRY but doesn't define them
////so window.h must be included
//#ifdef _MSC_VER
//#define _CRT_SECURE_NO_WARNINGS //disable warnings for safe function versions
//#endif
//

#include "Structs.h"
#endif