///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "BuilderFrame.h"

///////////////////////////////////////////////////////////////////////////

BuilderFrame::BuilderFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	m_menubar1 = new wxMenuBar( 0 );
	Files = new wxMenu();
	wxMenuItem* Load;
	Load = new wxMenuItem( Files, wxID_ANY, wxString( wxT( "Load" ) ), wxEmptyString, wxITEM_NORMAL );
	Files->Append( Load );

	wxMenuItem* Save;
	Save = new wxMenuItem( Files, wxID_ANY, wxString( wxT( "Save" ) ), wxEmptyString, wxITEM_NORMAL );
	Files->Append( Save );

	wxMenuItem* Exit;
	Exit = new wxMenuItem( Files, wxID_ANY, wxString( wxT( "Exit" ) ), wxEmptyString, wxITEM_NORMAL );
	Files->Append( Exit );

	m_menubar1->Append( Files, wxT( "File" ) );

	this->SetMenuBar( m_menubar1 );

	wxFlexGridSizer* fgSizer2;
	fgSizer2 = new wxFlexGridSizer( 0, 1, 0, 0 );
	fgSizer2->AddGrowableCol( 0 );
	fgSizer2->AddGrowableRow( 0 );
	fgSizer2->SetFlexibleDirection( wxBOTH );
	fgSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_splitter3 = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter3->Connect( wxEVT_IDLE, wxIdleEventHandler( BuilderFrame::m_splitter3OnIdle ), NULL, this );

	m_panel1 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxSize( -1, -1 ), wxTAB_TRAVERSAL );
	bSizer19 = new wxBoxSizer( wxVERTICAL );


	m_panel1->SetSizer( bSizer19 );
	m_panel1->Layout();
	bSizer19->Fit( m_panel1 );
	m_panel2 = new wxPanel( m_splitter3, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxFlexGridSizer* fgSizer21;
	fgSizer21 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer21->AddGrowableCol( 0 );
	fgSizer21->AddGrowableRow( 0 );
	fgSizer21->SetFlexibleDirection( wxBOTH );
	fgSizer21->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_notebook1 = new wxNotebook( m_panel2, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_panel3 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* fgSizer3;
	fgSizer3 = new wxFlexGridSizer( 0, 2, 0, 0 );
	fgSizer3->AddGrowableCol( 0 );
	fgSizer3->AddGrowableRow( 0 );
	fgSizer3->SetFlexibleDirection( wxBOTH );
	fgSizer3->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxStaticBoxSizer* sbSizer10;
	sbSizer10 = new wxStaticBoxSizer( new wxStaticBox( m_panel3, wxID_ANY, wxT( "Load Game Object" ) ), wxVERTICAL );

	m_staticText8 = new wxStaticText( sbSizer10->GetStaticBox(), wxID_ANY, wxT( "File:" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText8->Wrap( -1 );
	sbSizer10->Add( m_staticText8, 0, wxALL, 5 );

	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer( wxHORIZONTAL );

	m_filePickBox = new wxTextCtrl( sbSizer10->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200, -1 ), 0 );
	bSizer30->Add( m_filePickBox, 0, wxALL, 5 );

	m_filePickButton = new wxButton( sbSizer10->GetStaticBox(), wxID_ANY, wxT( "Find..." ), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer30->Add( m_filePickButton, 0, wxALL, 5 );


	sbSizer10->Add( bSizer30, 1, wxEXPAND, 5 );

	m_button6 = new wxButton( sbSizer10->GetStaticBox(), wxID_ANY, wxT( "Add" ), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer10->Add( m_button6, 0, wxALL, 5 );


	fgSizer3->Add( sbSizer10, 1, wxEXPAND, 5 );


	bSizer2->Add( fgSizer3, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer111;
	sbSizer111 = new wxStaticBoxSizer( new wxStaticBox( m_panel3, wxID_ANY, wxT( "Loaded Objects" ) ), wxVERTICAL );

	m_listBox1 = new wxListBox( sbSizer111->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( -1, 200 ), 0, NULL, 0 );
	sbSizer111->Add( m_listBox1, 0, wxALL | wxEXPAND, 5 );

	m_button3 = new wxButton( sbSizer111->GetStaticBox(), wxID_ANY, wxT( "Create" ), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizer111->Add( m_button3, 0, wxALL, 5 );


	bSizer2->Add( sbSizer111, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer7;
	sbSizer7 = new wxStaticBoxSizer( new wxStaticBox( m_panel3, wxID_ANY, wxT( "Lights" ) ), wxVERTICAL );

	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxHORIZONTAL );

	m_button4 = new wxButton( sbSizer7->GetStaticBox(), wxID_ANY, wxT( "Add Point Light" ), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer17->Add( m_button4, 0, wxALL, 5 );


	bSizer16->Add( bSizer17, 1, wxEXPAND, 5 );


	sbSizer7->Add( bSizer16, 0, wxEXPAND, 5 );


	bSizer2->Add( sbSizer7, 0, wxEXPAND, 5 );


	m_panel3->SetSizer( bSizer2 );
	m_panel3->Layout();
	bSizer2->Fit( m_panel3 );
	m_notebook1->AddPage( m_panel3, wxT( "Create" ), false );
	m_panel4 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );

	wxStaticBoxSizer* sbSizer1;
	sbSizer1 = new wxStaticBoxSizer( new wxStaticBox( m_panel4, wxID_ANY, wxT( "Position" ) ), wxVERTICAL );

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText29 = new wxStaticText( sbSizer1->GetStaticBox(), wxID_ANY, wxT( "X" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText29->Wrap( -1 );
	bSizer6->Add( m_staticText29, 0, wxALL, 5 );

	m_textCtrl1 = new wxTextCtrl( sbSizer1->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer6->Add( m_textCtrl1, 0, wxALL, 5 );

	m_staticText81 = new wxStaticText( sbSizer1->GetStaticBox(), wxID_ANY, wxT( "Y" ), wxDefaultPosition, wxSize( -1, -1 ), 0 );
	m_staticText81->Wrap( -1 );
	bSizer6->Add( m_staticText81, 0, wxALL, 5 );

	m_textCtrl2 = new wxTextCtrl( sbSizer1->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer6->Add( m_textCtrl2, 0, wxALL, 5 );

	m_staticText9 = new wxStaticText( sbSizer1->GetStaticBox(), wxID_ANY, wxT( "Z" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText9->Wrap( -1 );
	bSizer6->Add( m_staticText9, 0, wxALL, 5 );

	m_textCtrl3 = new wxTextCtrl( sbSizer1->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer6->Add( m_textCtrl3, 0, wxALL, 5 );


	sbSizer1->Add( bSizer6, 0, wxEXPAND, 5 );


	bSizer5->Add( sbSizer1, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer2;
	sbSizer2 = new wxStaticBoxSizer( new wxStaticBox( m_panel4, wxID_ANY, wxT( "Orientation" ) ), wxVERTICAL );

	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText10 = new wxStaticText( sbSizer2->GetStaticBox(), wxID_ANY, wxT( "X" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText10->Wrap( -1 );
	bSizer71->Add( m_staticText10, 0, wxALL, 5 );

	m_textCtrl4 = new wxTextCtrl( sbSizer2->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer71->Add( m_textCtrl4, 0, wxALL, 5 );

	m_staticText11 = new wxStaticText( sbSizer2->GetStaticBox(), wxID_ANY, wxT( "Y" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText11->Wrap( -1 );
	bSizer71->Add( m_staticText11, 0, wxALL, 5 );

	m_textCtrl5 = new wxTextCtrl( sbSizer2->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer71->Add( m_textCtrl5, 0, wxALL, 5 );

	m_staticText12 = new wxStaticText( sbSizer2->GetStaticBox(), wxID_ANY, wxT( "Z" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText12->Wrap( -1 );
	bSizer71->Add( m_staticText12, 0, wxALL, 5 );

	m_textCtrl6 = new wxTextCtrl( sbSizer2->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer71->Add( m_textCtrl6, 0, wxALL, 5 );


	sbSizer2->Add( bSizer71, 0, wxEXPAND, 5 );


	bSizer5->Add( sbSizer2, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer4;
	sbSizer4 = new wxStaticBoxSizer( new wxStaticBox( m_panel4, wxID_ANY, wxT( "Tint" ) ), wxVERTICAL );

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText45 = new wxStaticText( sbSizer4->GetStaticBox(), wxID_ANY, wxT( "Colour" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText45->Wrap( -1 );
	bSizer8->Add( m_staticText45, 0, wxALL, 5 );

	m_colourPicker1 = new wxColourPickerCtrl( sbSizer4->GetStaticBox(), wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer8->Add( m_colourPicker1, 0, wxALL, 5 );


	sbSizer4->Add( bSizer8, 0, wxEXPAND, 5 );


	bSizer5->Add( sbSizer4, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer5;
	sbSizer5 = new wxStaticBoxSizer( new wxStaticBox( m_panel4, wxID_ANY, wxT( "Collider Options" ) ), wxVERTICAL );

	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer10;
	bSizer10 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText15 = new wxStaticText( sbSizer5->GetStaticBox(), wxID_ANY, wxT( "Type" ), wxDefaultPosition, wxSize( 40, -1 ), 0 );
	m_staticText15->Wrap( -1 );
	bSizer10->Add( m_staticText15, 0, wxALL, 5 );

	wxString m_choice1Choices [] = { wxT( "Box" ), wxT( "Sphere" ), wxT( "Plane" ) };
	int m_choice1NChoices = sizeof( m_choice1Choices ) / sizeof( wxString );
	m_choice1 = new wxChoice( sbSizer5->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxSize( 200, -1 ), m_choice1NChoices, m_choice1Choices, 0 );
	m_choice1->SetSelection( 0 );
	bSizer10->Add( m_choice1, 0, wxALL, 5 );


	bSizer9->Add( bSizer10, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer11;
	bSizer11 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText16 = new wxStaticText( sbSizer5->GetStaticBox(), wxID_ANY, wxT( "Width" ), wxDefaultPosition, wxSize( 40, -1 ), 0 );
	m_staticText16->Wrap( -1 );
	bSizer11->Add( m_staticText16, 0, wxALL, 5 );

	m_textCtrl7 = new wxTextCtrl( sbSizer5->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer11->Add( m_textCtrl7, 0, wxALL, 5 );

	m_staticText17 = new wxStaticText( sbSizer5->GetStaticBox(), wxID_ANY, wxT( "Height" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText17->Wrap( -1 );
	bSizer11->Add( m_staticText17, 0, wxALL, 5 );

	m_textCtrl8 = new wxTextCtrl( sbSizer5->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer11->Add( m_textCtrl8, 0, wxALL, 5 );

	m_staticText18 = new wxStaticText( sbSizer5->GetStaticBox(), wxID_ANY, wxT( "Depth" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText18->Wrap( -1 );
	bSizer11->Add( m_staticText18, 0, wxALL, 5 );

	m_textCtrl9 = new wxTextCtrl( sbSizer5->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer11->Add( m_textCtrl9, 0, wxALL, 5 );


	bSizer9->Add( bSizer11, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText19 = new wxStaticText( sbSizer5->GetStaticBox(), wxID_ANY, wxT( "Radius" ), wxDefaultPosition, wxSize( 40, -1 ), 0 );
	m_staticText19->Wrap( -1 );
	bSizer12->Add( m_staticText19, 0, wxALL, 5 );

	m_textCtrl10 = new wxTextCtrl( sbSizer5->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer12->Add( m_textCtrl10, 0, wxALL, 5 );


	bSizer9->Add( bSizer12, 1, wxEXPAND, 5 );


	sbSizer5->Add( bSizer9, 0, wxEXPAND, 5 );


	bSizer5->Add( sbSizer5, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer101;
	sbSizer101 = new wxStaticBoxSizer( new wxStaticBox( m_panel4, wxID_ANY, wxT( "Physics Options" ) ), wxVERTICAL );

	wxBoxSizer* bSizer20;
	bSizer20 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText181 = new wxStaticText( sbSizer101->GetStaticBox(), wxID_ANY, wxT( "Mass" ), wxDefaultPosition, wxSize( 50, -1 ), 0 );
	m_staticText181->Wrap( -1 );
	bSizer20->Add( m_staticText181, 0, wxALL, 5 );

	m_textCtrl12 = new wxTextCtrl( sbSizer101->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer20->Add( m_textCtrl12, 0, wxALL, 5 );


	sbSizer101->Add( bSizer20, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizer21;
	bSizer21 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText191 = new wxStaticText( sbSizer101->GetStaticBox(), wxID_ANY, wxT( "Elasticity" ), wxDefaultPosition, wxSize( 50, -1 ), 0 );
	m_staticText191->Wrap( -1 );
	bSizer21->Add( m_staticText191, 0, wxALL, 5 );

	m_textCtrl13 = new wxTextCtrl( sbSizer101->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50, -1 ), 0 );
	bSizer21->Add( m_textCtrl13, 0, wxALL, 5 );


	sbSizer101->Add( bSizer21, 1, wxEXPAND, 5 );


	bSizer5->Add( sbSizer101, 0, wxEXPAND, 5 );


	m_panel4->SetSizer( bSizer5 );
	m_panel4->Layout();
	bSizer5->Fit( m_panel4 );
	m_notebook1->AddPage( m_panel4, wxT( "Game Object" ), true );
	m_panel11 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer24;
	bSizer24 = new wxBoxSizer( wxVERTICAL );

	wxStaticBoxSizer* sbSizer41;
	sbSizer41 = new wxStaticBoxSizer( new wxStaticBox( m_panel11, wxID_ANY, wxT( "Intensity" ) ), wxVERTICAL );

	wxBoxSizer* bSizer81;
	bSizer81 = new wxBoxSizer( wxHORIZONTAL );

	m_staticText46 = new wxStaticText( sbSizer41->GetStaticBox(), wxID_ANY, wxT( "Colour" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText46->Wrap( -1 );
	bSizer81->Add( m_staticText46, 0, wxALL, 5 );

	m_colourPicker11 = new wxColourPickerCtrl( sbSizer41->GetStaticBox(), wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer81->Add( m_colourPicker11, 0, wxALL, 5 );


	sbSizer41->Add( bSizer81, 0, wxEXPAND, 5 );


	bSizer24->Add( sbSizer41, 0, wxEXPAND, 5 );

	wxStaticBoxSizer* sbSizer411;
	sbSizer411 = new wxStaticBoxSizer( new wxStaticBox( m_panel11, wxID_ANY, wxT( "Direction" ) ), wxVERTICAL );

	wxBoxSizer* bSizer811;
	bSizer811 = new wxBoxSizer( wxVERTICAL );

	m_slider2 = new wxSlider( sbSizer411->GetStaticBox(), wxID_ANY, 50, 0, 360, wxDefaultPosition, wxSize( 300, -1 ), wxSL_HORIZONTAL );
	bSizer811->Add( m_slider2, 0, wxALL, 5 );


	sbSizer411->Add( bSizer811, 0, wxEXPAND, 5 );


	bSizer24->Add( sbSizer411, 0, wxEXPAND, 5 );


	m_panel11->SetSizer( bSizer24 );
	m_panel11->Layout();
	bSizer24->Fit( m_panel11 );
	m_notebook1->AddPage( m_panel11, wxT( "Light" ), false );
	m_panel5 = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxVERTICAL );

	m_staticText20 = new wxStaticText( m_panel5, wxID_ANY, wxT( "Renderer" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText20->Wrap( -1 );
	bSizer13->Add( m_staticText20, 0, wxALL, 5 );

	wxString m_choice2Choices [] = { wxT( "Direct3D 11" ), wxT( "OpenGL 3.3" ) };
	int m_choice2NChoices = sizeof( m_choice2Choices ) / sizeof( wxString );
	m_choice2 = new wxChoice( m_panel5, wxID_ANY, wxDefaultPosition, wxSize( 140, -1 ), m_choice2NChoices, m_choice2Choices, 0 );
	m_choice2->SetSelection( 0 );
	bSizer13->Add( m_choice2, 0, wxALL, 5 );

	m_staticText21 = new wxStaticText( m_panel5, wxID_ANY, wxT( "Physics Engine" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	bSizer13->Add( m_staticText21, 0, wxALL, 5 );

	wxString m_choice3Choices [] = { wxT( "Bullet Physics" ), wxT( "Nvidia PhysX" ) };
	int m_choice3NChoices = sizeof( m_choice3Choices ) / sizeof( wxString );
	m_choice3 = new wxChoice( m_panel5, wxID_ANY, wxDefaultPosition, wxSize( 140, -1 ), m_choice3NChoices, m_choice3Choices, 0 );
	m_choice3->SetSelection( 0 );
	bSizer13->Add( m_choice3, 0, wxALL, 5 );

	m_staticText28 = new wxStaticText( m_panel5, wxID_ANY, wxT( "Ambient Light Intensity" ), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText28->Wrap( -1 );
	bSizer13->Add( m_staticText28, 0, wxALL, 5 );

	m_colourPicker3 = new wxColourPickerCtrl( m_panel5, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	bSizer13->Add( m_colourPicker3, 0, wxALL, 5 );


	m_panel5->SetSizer( bSizer13 );
	m_panel5->Layout();
	bSizer13->Fit( m_panel5 );
	m_notebook1->AddPage( m_panel5, wxT( "Scene Options" ), false );
	Scene = new wxPanel( m_notebook1, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxVERTICAL );

	m_listBox2 = new wxListBox( Scene, wxID_ANY, wxDefaultPosition, wxSize( -1, 400 ), 0, NULL, 0 );
	bSizer14->Add( m_listBox2, 0, wxALL | wxEXPAND, 5 );

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxHORIZONTAL );

	m_button31 = new wxButton( Scene, wxID_ANY, wxT( "Delete" ), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer15->Add( m_button31, 0, wxALL, 5 );


	bSizer14->Add( bSizer15, 1, wxEXPAND, 5 );


	Scene->SetSizer( bSizer14 );
	Scene->Layout();
	bSizer14->Fit( Scene );
	m_notebook1->AddPage( Scene, wxT( "Scene" ), false );

	fgSizer21->Add( m_notebook1, 1, wxEXPAND | wxALL, 5 );


	m_panel2->SetSizer( fgSizer21 );
	m_panel2->Layout();
	fgSizer21->Fit( m_panel2 );
	m_splitter3->SplitVertically( m_panel1, m_panel2, 764 );
	fgSizer2->Add( m_splitter3, 1, wxEXPAND, 5 );


	this->SetSizer( fgSizer2 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( Load->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Load_Click ) );
	this->Connect( Save->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Save ) );
	this->Connect( Exit->GetId(), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Exit ) );
	m_panel1->Connect( wxEVT_RIGHT_UP, wxMouseEventHandler( BuilderFrame::RightClick ), NULL, this );
	m_filePickButton->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::MyFilePicker1 ), NULL, this );
	m_button6->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Add_Click ), NULL, this );
	m_button3->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Create_Obj ), NULL, this );
	m_button4->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Add_PointLight ), NULL, this );
	m_textCtrl1->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_X ), NULL, this );
	m_textCtrl2->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_Y ), NULL, this );
	m_textCtrl3->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_Z ), NULL, this );
	m_textCtrl4->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_X ), NULL, this );
	m_textCtrl5->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_Y ), NULL, this );
	m_textCtrl6->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_Z ), NULL, this );
	m_colourPicker1->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Prop_Tint_Color ), NULL, this );
	m_choice1->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( BuilderFrame::Prop_Collider_Type ), NULL, this );
	m_textCtrl7->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Width ), NULL, this );
	m_textCtrl8->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Height ), NULL, this );
	m_textCtrl9->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Depth ), NULL, this );
	m_textCtrl10->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Radius ), NULL, this );
	m_textCtrl12->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Mass_Changed ), NULL, this );
	m_textCtrl13->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Elasticity_Changed ), NULL, this );
	m_colourPicker11->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Prop_Light_Color ), NULL, this );
	m_colourPicker3->Connect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Ambient_Color_Change ), NULL, this );
	m_listBox2->Connect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( BuilderFrame::Scene_List_Change ), NULL, this );
	m_button31->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Scene_Delete_Click ), NULL, this );
}

BuilderFrame::~BuilderFrame()
{
	// Disconnect Events
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Load_Click ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Save ) );
	this->Disconnect( wxID_ANY, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( BuilderFrame::Exit ) );
	m_panel1->Disconnect( wxEVT_RIGHT_UP, wxMouseEventHandler( BuilderFrame::RightClick ), NULL, this );
	m_filePickButton->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::MyFilePicker1 ), NULL, this );
	m_button6->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Add_Click ), NULL, this );
	m_button3->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Create_Obj ), NULL, this );
	m_button4->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Create_Add_PointLight ), NULL, this );
	m_textCtrl1->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_X ), NULL, this );
	m_textCtrl2->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_Y ), NULL, this );
	m_textCtrl3->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Pos_Z ), NULL, this );
	m_textCtrl4->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_X ), NULL, this );
	m_textCtrl5->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_Y ), NULL, this );
	m_textCtrl6->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Ori_Z ), NULL, this );
	m_colourPicker1->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Prop_Tint_Color ), NULL, this );
	m_choice1->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( BuilderFrame::Prop_Collider_Type ), NULL, this );
	m_textCtrl7->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Width ), NULL, this );
	m_textCtrl8->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Height ), NULL, this );
	m_textCtrl9->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Depth ), NULL, this );
	m_textCtrl10->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Prop_Col_Radius ), NULL, this );
	m_textCtrl12->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Mass_Changed ), NULL, this );
	m_textCtrl13->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( BuilderFrame::Elasticity_Changed ), NULL, this );
	m_colourPicker11->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Prop_Light_Color ), NULL, this );
	m_colourPicker3->Disconnect( wxEVT_COMMAND_COLOURPICKER_CHANGED, wxColourPickerEventHandler( BuilderFrame::Ambient_Color_Change ), NULL, this );
	m_listBox2->Disconnect( wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler( BuilderFrame::Scene_List_Change ), NULL, this );
	m_button31->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( BuilderFrame::Scene_Delete_Click ), NULL, this );

}
