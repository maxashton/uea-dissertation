#include "Common.h"

#include "wx/wx.h"
#include "wx/sizer.h"
#include "BuilderFrameEx.h"
#include "RuntimeFrameEx.h"

class MyApp : public wxApp
{
	virtual bool OnInit();

	wxFrame *frame;

public:

};

IMPLEMENT_APP( MyApp )


bool MyApp::OnInit()
{
	int mode = 1;

	if ( mode == 0 )
	{
		frame = new BuilderFrameEx( ( wxFrame * )NULL, -1, wxT( "Simulation Application" ), wxPoint( 50, 50 ), wxSize( 1200, 800 ) );
	}
	else
	{
		frame = new RuntimeFrameEx( ( wxFrame * )NULL, -1, wxT( "Simulation Application" ), wxPoint( 50, 50 ), wxSize( 1200, 800 ) );
	}

	frame->Maximize( true );
	frame->SetAutoLayout( true );

	frame->Show();
	return true;
}