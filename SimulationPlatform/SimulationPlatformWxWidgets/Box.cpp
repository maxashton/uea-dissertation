#include "Box.h"


Box::Box()
{
}


Box::~Box()
{
}

void Box::Create()
{
	_geometry.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );

	_geometry.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );

	_geometry.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );

	_geometry.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );

	_geometry.push_back( glm::vec3( 1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, -1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( 1.0f, 1.0f, 1.0f ) );

	_geometry.push_back( glm::vec3( -1.0f, -1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, -1.0f, 1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, -1.0f ) );
	_geometry.push_back( glm::vec3( -1.0f, 1.0f, 1.0f ) );

	//face 1
	_indicies.push_back( 0 );
	_indicies.push_back( 1 );
	_indicies.push_back( 2 );

	_indicies.push_back( 2 );
	_indicies.push_back( 1 );
	_indicies.push_back( 3 );

	_indicies.push_back( 4 );
	_indicies.push_back( 5 );
	_indicies.push_back( 6 );

	_indicies.push_back( 6 );
	_indicies.push_back( 5 );
	_indicies.push_back( 7 );

	_indicies.push_back( 8 );
	_indicies.push_back( 9 );
	_indicies.push_back( 10 );

	_indicies.push_back( 10 );
	_indicies.push_back( 9 );
	_indicies.push_back( 11 );

	_indicies.push_back( 12 );
	_indicies.push_back( 13 );
	_indicies.push_back( 14 );

	_indicies.push_back( 14 );
	_indicies.push_back( 13 );
	_indicies.push_back( 15 );

	_indicies.push_back( 16 );
	_indicies.push_back( 17 );
	_indicies.push_back( 18 );

	_indicies.push_back( 18 );
	_indicies.push_back( 17 );
	_indicies.push_back( 19 );

	_indicies.push_back( 20 );
	_indicies.push_back( 21 );
	_indicies.push_back( 22 );

	_indicies.push_back( 22 );
	_indicies.push_back( 21 );
	_indicies.push_back( 23 );

}

int Box::GetNumberOfInicies()
{
	return ( int )_indicies.size();
}

void Box::ConnectRenderer( Renderer* pRenderer )
{
	GameObject::ConnectRenderer( pRenderer );

	pRenderer->BuildGeometricDiffuseShader();
	pRenderer->InitializeGeometricVertexBuffer( _indicies, _geometry );
}

void Box::Draw( glm::mat4 mCameraMatrix )
{
	_pRenderer->DrawGeoPlane( mCameraMatrix, _modelMatrixOverride, _modelMatrix, _pTransform, GetNumberOfInicies() );
}
