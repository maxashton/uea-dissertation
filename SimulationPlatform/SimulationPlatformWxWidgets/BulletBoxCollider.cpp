#include "BulletBoxCollider.h"
#include "RenderFactory.h"
#include "RigidBodyFactory.h"

BulletBoxCollider::BulletBoxCollider()
{
	_colliderType = BoxType;
}


BulletBoxCollider::~BulletBoxCollider()
{
}

void BulletBoxCollider::Draw( glm::mat4 mCameraMatrix )
{
	if ( _pRigidBody != NULL )
	{
		glm::mat4 matrix = _pRigidBody->GetModelMatrix();
		_box.SetModelMatrix( matrix );
	}
	else
	{
		if ( _pGameObject != NULL )
		{
			_box.GetTransform()->pPosition->x = _pGameObject->GetTransform()->pPosition->x;
			_box.GetTransform()->pPosition->y = _pGameObject->GetTransform()->pPosition->y;
			_box.GetTransform()->pPosition->z = _pGameObject->GetTransform()->pPosition->z;

			_box.GetTransform()->pOrientation->x = _pGameObject->GetTransform()->pOrientation->x;
			_box.GetTransform()->pOrientation->y = _pGameObject->GetTransform()->pOrientation->y;
			_box.GetTransform()->pOrientation->z = _pGameObject->GetTransform()->pOrientation->z;
		}
	}

	_box.Draw( mCameraMatrix );
}

boost::shared_ptr<RigidBody> BulletBoxCollider::Create( float mass, float elasticity, float width, float height, float depth, bool noBody )
{
	_mass = mass;
	_elasticity = elasticity;
	_width = width;
	_height = height;
	_depth = depth;

	_box.Create();
	_box.ConnectRenderer( RenderFactory::CreateRenderer() );
	_box.GetTransform()->pScale = new glm::vec3( width / 2.0f, height / 2.0f, depth / 2.0f );

	if ( !noBody )
	{
		_pRigidBody = RigidBodyFactory::CreateBoxBody( mass, elasticity, width, height, depth );

		return _pRigidBody;
	}

	return NULL;
}

void BulletBoxCollider::SerializeToXLM( boost::property_tree::ptree* pInTree, bool writeType /*= true */ )
{
	if ( writeType )
	{
		ptree & transformNode = pInTree->add( "CType", "BulletBoxCollider" );
	}

	Component::SerializeToXLM( pInTree, false );

	pInTree->put( "Mass", _mass );
	pInTree->put( "Elasticity", _elasticity );
	pInTree->put( "Width", _width );
	pInTree->put( "Height", _height ); 
	pInTree->put( "Depth", _depth );
}

void BulletBoxCollider::DeSerializeFromXML( ptree::value_type const& v )
{
	Component::DeSerializeFromXML( v );

	Create( v.second.get<float>( "Mass" ), v.second.get<float>( "Elasticity" ), v.second.get<float>( "Width" ), v.second.get<float>( "Height" ), v.second.get<float>( "Depth" ) );
}

glm::vec3 BulletBoxCollider::GetDimentions()
{
	return glm::vec3( _width, _height, _depth );
}

void BulletBoxCollider::SetDimentions( glm::vec3 dims )
{
	_width = dims.x;
	_height = dims.y;
	_depth = dims.z;

	_box.GetTransform()->pScale = new glm::vec3( dims.x / 2.0f, dims.y / 2.0f, dims.z / 2.0f );
}
